import KeycloakProvider from 'next-auth/providers/keycloak'
import {RedisAdapter} from './adapter'
import Redis from 'ioredis'
import {NextAuthOptions} from 'next-auth'
import axios from 'axios'
import {MEMBER_JOIN_ENDPOINT} from 'util/endpoints'

export function nextAuthConfig(
	nextAuthUrl: string | undefined,
	redisPort: number,
	redistHost: string,
	redisPassword: string,
	keycloakId: string,
	keycloakSecret: string,
	keycloakIssuer: string,
	topLevelDomain: string
): NextAuthOptions {
	// Client connection to Redis session store
	const redis = new Redis({
		port: redisPort,
		host: redistHost,
		password: redisPassword,
	})

	const useSecureCookies = (nextAuthUrl as string).startsWith('https://')

	// NEXT AUTH CONFIGURATION OBJECT HERE
	return {
		adapter: RedisAdapter(redis),
		providers: [
			KeycloakProvider({
				name: "Fractal Auth",
				clientId: keycloakId,
				clientSecret: keycloakSecret,
				issuer: keycloakIssuer,
			}),
		],
		// TODO -  REGENERATE AND ADD SECRET TO ENV VARIABLE
		secret: '+xDI330DgJv8xfi2VwirPX4Wt6zDEI6miu4UX9qd98c=',
		session: {
			strategy: 'database',
		},
		callbacks: {
			async signIn({user, account, profile, email, credentials}) {
				// console.log('************SIGN IN*************')
				// console.log('user', user)
				// console.log('account', account)
				// console.log('profile', profile)
				// console.log('email', email)
				// console.log('credentials', credentials)

				// Pass providerAccountId to user object to reduce Redis queries in session callback
				try {
					const response = await axios.post(
						`${MEMBER_JOIN_ENDPOINT}`,
						{},
						{
							headers: {Authorization: `Bearer ${account.access_token}`},
						}
					)
					console.log(account.access_token)
					console.log('RESPONSE FROM SIGN IN CALLBACK', response)
				} catch (error) {
					console.log('ERROR FROM SIGN IN: ', error)
				}
				user.providerAccountId = account.providerAccountId

				return true
			},
			async session({session, user}) {
				// console.log('************SESSION*************')
				// console.log('user', user)
				// console.log('session', session)

				// Hit member endpoint to trigger get or create in django db
				console.log(session)
				console.log(user)
				session.uuid = user.providerAccountId as string
				session.name = user.name
				let redisSession = await redis.get(`user:account:keycloak:${user.providerAccountId}`)
				if (redisSession) {
					const redisSessionObj = JSON.parse(redisSession)
					session.accessToken = redisSessionObj?.access_token
				}
				return session
			},
		},
		cookies: {
			sessionToken: {
				name: `next-auth.session-token`,
				options: {
					httpOnly: true,
					sameSite: 'lax',
					path: '/',
					secure: useSecureCookies,
					domain: topLevelDomain,
				},
			},
		},
	}
}
