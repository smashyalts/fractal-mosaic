import React from 'react'
import axios from 'axios'

// Next
import {useRouter} from 'next/router'

// Next Auth
import {useSession, signIn} from 'next-auth/react'

const withAuth = (Component: React.FC) => {
	return () => {
		const router = useRouter()

		const {data: session} = useSession({
			required: true,
			onUnauthenticated() {
				signIn('keycloak')
			},
		})

		// User is logged in
		if (session) {
			// axios
			// 	.get(`${process.env.NEXT_PUBLIC_MOSAIC_API}device/`, {
			// 		headers: {
			// 			Authorization: `Bearer ${session?.accessToken}`,
			// 		},
			// 	})
			// 	.then((response) => {
			// 		if (response.data.length < 1) {
			// 			router.push('/adddevice')
			// 		}
			// 	})
			// 	.catch((error) => console.log(error))
			return <Component />
		}

		return null
	}
}

export default withAuth
