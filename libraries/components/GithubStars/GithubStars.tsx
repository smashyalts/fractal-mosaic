import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import css from './githubStars.module.css'

interface GithubStarsProps {
	stars: number
	url: string
}

const GithubStars = ({stars, url}: GithubStarsProps) => {
	return (
		<a href={url} className={css.githubStars} target='_blank'>
			<FontAwesomeIcon icon={['fab', 'github']} fixedWidth />
			<span className={css.githubStars__number}>{stars}</span>
			<FontAwesomeIcon icon={['fas', 'star']} size='xs' fixedWidth />
		</a>
	)
}

export default GithubStars
