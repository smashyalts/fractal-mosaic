// React
import {useState} from 'react'
import Link from 'next/link'

// NextAuth
import {signIn, signOut, useSession} from 'next-auth/react'

// Bootstrap
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import Button from 'react-bootstrap/Button'
import Dropdown from 'react-bootstrap/Dropdown'

// Icons
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

// Library Components
import Member from 'data/members/Member'
import PictureMd from '../Picture/PictureMd'
import FractalAvatar from '../Picture/FractalAvatar'

// Style
import css from './MonoNav.module.css'

// Types
import {CustomComponentProps, ImgType, MonoNavProps, UuidType} from 'types'
import useSwrAuth from 'hooks/useSwrAuth'
import ModalPersonalSettings from '../Modals/ModalPersonalSettings'

const MonoNav = ({children, uuid, currentDomain, useSession}: MonoNavProps) => {
	// Sidebar that can be opened from Mobile
	const [menu, setMenu] = useState(false)
	const toggleMenu = () => {
		setMenu(!!!menu)
	}

	return (
		<Navbar expand='xs' className={css.navbar}>
			<div className={css.navbar__nav}>
				{/* Mobile Hamburger Button. Only display if the page has a sub-menu*/}
				{children && (
					<Button className={css.navbar__menubtn} onClick={toggleMenu}>
						<FontAwesomeIcon icon={menu ? ['fas', 'xmark'] : ['fas', 'bars']} fixedWidth />
					</Button>
				)}

				{/* Logo + Name */}
				<Link href={`${process.env.NEXT_PUBLIC_HOMEPAGE_BASE_URL}`} passHref>
					<Navbar.Brand className={css.navbar__brand}>
						<div className={css.navbar__logo}>
							<svg version='1.1' x='0px' y='0px' viewBox='0 0 512 512'>
								<path
									d='M212,355.9l-24,13.9c-9.9,5.7-16,16.3-16,27.7v27.7c0,12.3,13.3,20,24,13.9l24-13.9c9.9-5.7,16-16.3,16-27.7
									v-27.7C236,357.5,222.7,349.8,212,355.9z'
								/>
								<path
									d='M276,220.4l-88,50.8c-9.9,5.7-16,16.3-16,27.7v27.7c0,12.3,13.3,20,24,13.9l88-50.8c9.9-5.7,16-16.3,16-27.7
									v-27.7C300,222,286.7,214.3,276,220.4z'
								/>
								<path
									d='M340,84.9l-152,87.8c-9.9,5.7-16,16.3-16,27.7v27.7c0,12.3,13.3,20,24,13.9l152-87.8c9.9-5.7,16-16.3,16-27.7
									V98.8C364,86.5,350.7,78.8,340,84.9z'
								/>
							</svg>
						</div>
						Fractal Networks
					</Navbar.Brand>
				</Link>

				{/* Nav Links, desktop only */}
				<Nav className={css.navbar__desktopLinks}>
					<Nav.Link
						href={process.env.NEXT_PUBLIC_STORE_BASE_URL}
						className={` ${currentDomain == 'Store' && css.navbar__link__active} ${css.navbar__link} `}>
						{/* <FontAwesomeIcon icon={["fas", "store"]} fixedWidth size="sm" /> */}
						Store
					</Nav.Link>
					<Nav.Link
						href='https://docs.fractalnetworks.co'
						target='_blank'
						className={` ${currentDomain == 'Docs' && css.navbar__link__active} ${css.navbar__link} `}>
						{/* <FontAwesomeIcon icon={["fas", "book"]} fixedWidth size="sm" /> */}
						Docs
					</Nav.Link>
					<Nav.Link
						href={process.env.NEXT_PUBLIC_CONSOLE_BASE_URL}
						className={` ${currentDomain == 'Console' && css.navbar__link__active} ${css.navbar__link} `}>
						{/* <FontAwesomeIcon icon={["fas", "gauge"]} fixedWidth size="sm" /> */}
						Console
					</Nav.Link>
				</Nav>
			</div>

			{/* @ts-ignore */}
			<FractalAccount uuid={uuid} appSession={useSession} />

			{/*Mobile Menu, triggered by button, different on every page */}
			{children && (
				<Navbar.Collapse
					style={menu ? {display: 'flex'} : {display: 'none'}}
					className={css.navbar__collapse}
					id='subNav'>
					{children}
				</Navbar.Collapse>
			)}
		</Navbar>
	)
}

export default MonoNav

interface FractalAccountProps {
	appSession: any
	uuid: UuidType
}
const FractalAccount = ({uuid, appSession}: FractalAccountProps) => {
	const {data: session, status} = appSession()

	if (status === 'unauthenticated') {
		return (
			<Button onClick={() => signIn('keycloak')} className={css.navbar__signbtn}>
				Sign In
			</Button>
		)
	} else if (status === 'loading') {
		return <div className={css.navbar__skeleton}></div>
	}

	return <MonoProfileDropdown uuid={uuid} />
}

const MonoProfileDropdown = ({uuid}: CustomComponentProps) => {
	async function logOut() {
		await signOut()
		window.location.href = `${process.env.NEXT_PUBLIC_KEYCLOAK_ISSUER}/protocol/openid-connect/logout?redirect_uri=${process.env.NEXT_PUBLIC_BASE_URL}`
	}

	// Account settings
	const [show, setShow] = useState(false)

	function handleGatewayClick() {
		window.open('https://github.com/fractalnetworksco/selfhosted-gateway', '_blank')
		return
	}

	const {data: session} = useSession()

	const {data, error} = useSwrAuth(`${process.env.NEXT_PUBLIC_MOSAIC_API}member/${session?.uuid}/`)

	const img: string | null = data && data.profile_image ? data.profile_image : null

	const isConsoleUI: boolean = window.location.href.includes('console')

	return (
		<>
			<Dropdown align='end' className={css.navbar__dropdown}>
				<Dropdown.Toggle className={css.navbar__droptoggle} id='dropdown-basic'>
					{img ? <PictureMd img={img} /> : <FractalAvatar />}
					<FontAwesomeIcon icon={['fas', 'caret-down']} size='sm' fixedWidth />
				</Dropdown.Toggle>
				<Dropdown.Menu className={css.navbar__dropmenu}>
					{/* <Dropdown.Item className={css.navbar__dropitem} href='#/action-1'>
					<FontAwesomeIcon icon={['fas', 'user-gear']} className={css.navbar__dropicon} size='sm' fixedWidth />
					Settings
				</Dropdown.Item>
				<Dropdown.Item className={css.navbar__dropitem} href='#/action-2'>
					<FontAwesomeIcon icon={['fas', 'credit-card']} className={css.navbar__dropicon} size='sm' fixedWidth />
					Billing
				</Dropdown.Item> */}
					{isConsoleUI && (
						<Dropdown.Item onClick={() => handleGatewayClick()} className={`${css.navbar__dropitem}`}>
							<FontAwesomeIcon
								icon={['fas', 'satellite-dish']}
								size='sm'
								className={`${css.navbar__dropicon} text-muted`}
								fixedWidth
							/>
							Use Custom Gateway
						</Dropdown.Item>
					)}
					{isConsoleUI && (
						<Dropdown.Item className={`${css.navbar__dropitem}`} onClick={() => setShow(!show)}>
							<FontAwesomeIcon
								icon={['fas', 'gear']}
								size='sm'
								className={`${css.navbar__dropicon} text-muted`}
								fixedWidth
							/>
							Account Settings
						</Dropdown.Item>
					)}
					<Dropdown.Item className={`${css.navbar__dropitem} ${css.text_danger}`} onClick={logOut}>
						<FontAwesomeIcon
							icon={['fas', 'right-to-bracket']}
							className={css.navbar__dropicon}
							size='sm'
							fixedWidth
							flip='horizontal'
						/>
						LogOut
					</Dropdown.Item>
				</Dropdown.Menu>
			</Dropdown>
			{isConsoleUI && <ModalPersonalSettings show={show} setShow={setShow} />}
		</>
	)
}
