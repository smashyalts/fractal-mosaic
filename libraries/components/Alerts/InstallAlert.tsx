// Bootstrap
import Alert from 'react-bootstrap/Alert'
import Button from 'react-bootstrap/Button'

// Icons
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

// Custom
import PictureMd from '../Picture/PictureMd'

// Types
import {LayoutProps} from 'types'

const InstallAlert = ({title}: LayoutProps) => {
	return (
		<Alert variant='secondary' className='bg-white text-body border-0'>
			<div className='mb-2 d-flex align-items-center'>
				<PictureMd img={'/Logo.svg'} circle={false} />
				<h3 className='m-0 me-auto'>Fractal Networks Mosaic</h3>
			</div>
			<p className='mb-3 text-muted'>
				Combine your personal computers to create a personal cloud. Install cloud services like {title} with a single
				click.
			</p>
			<div className='d-flex gap-2 align-items-center'>
				<Button className='me-1'>
					<span>Download Mosaic</span>
					<FontAwesomeIcon icon={['fas', 'download']} className='ms-2' fixedWidth />
				</Button>
				<Button variant='secondary' href={process.env.NEXT_PUBLIC_HOMEPAGE_BASE_URL}>
					Learn More
				</Button>
			</div>
		</Alert>
	)
}

export default InstallAlert
