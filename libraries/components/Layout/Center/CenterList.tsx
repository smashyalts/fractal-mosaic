// Fractal
import css from './layoutCenter.module.css'

// Util
import {LayoutProps} from 'types'

const CenterList = ({children}: LayoutProps) => {
	return <div className={css.centerList}>{children}</div>
}

export default CenterList
