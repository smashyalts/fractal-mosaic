// Fractal
import css from './layoutCenter.module.css'

// Util
import {LayoutProps} from 'types'

const CenterListHeader = ({children}: LayoutProps) => {
	return <header className={css.centerListHeader}>{children}</header>
}

export default CenterListHeader
