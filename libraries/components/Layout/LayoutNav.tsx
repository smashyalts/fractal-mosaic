// Util
import { LayoutProps } from 'types'


const LayoutNav = ({ children }: LayoutProps) => {
    return (
        <nav className="d-sm-flex d-none flex-column flex-shrink-0 h-100 overflow-auto bg-secondary gap-3 p-3">
            {children}
        </nav>
    )
}

export default LayoutNav