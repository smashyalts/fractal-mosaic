// Util
import { LayoutProps } from 'types'


const SidebarFooter = ({ children }: LayoutProps) => {
	return (
		<footer className="position-sticky bottom-0 p-3">
			{children}
		</footer>
	)
}

export default SidebarFooter