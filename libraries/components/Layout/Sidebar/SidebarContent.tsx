// Util
import { LayoutProps } from 'types'


const SidebarContent = ({ children }: LayoutProps) => {
  return (
    <ul className="m-0 p-0">
      {children}
    </ul>
  )
}

export default SidebarContent