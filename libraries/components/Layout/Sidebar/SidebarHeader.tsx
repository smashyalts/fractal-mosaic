// Util
import {LayoutProps} from 'types'

const DeviceHeader = ({children}: LayoutProps) => {
	return (
		<header className='position-sticky top-0 p-3 bg-secondary'>
			<h3 className='m-0'>{children}</h3>
		</header>
	)
}

export default DeviceHeader
