// Util
import { LayoutProps } from 'types'


const LayoutSidebar = ({ children }: LayoutProps) => {
    return (
        <section className="d-sm-flex d-none flex-column flex-shrink-0 h-100 overflow-auto bg-secondary">
            {children}
        </section>
    )
}

export default LayoutSidebar