// Util
import { LayoutProps } from 'types'


const Layout = ({ children }: LayoutProps) => {
    return (
        <main className="d-flex w-100 h-100 flex-column">
            {children}
        </main>
    )
}

export default Layout