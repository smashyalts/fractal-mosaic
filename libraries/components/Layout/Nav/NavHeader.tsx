// Util
import { LayoutProps } from 'types'


const NavHeader = ({ children }: LayoutProps) => {
	return (
		<header className="position-sticky top-0 bg-secondary">
			{children}
			{/* <hr className="m-0 mt-3 mx-auto" /> */}
		</header>
	)
}

export default NavHeader