import {useRouter} from 'next/router'

// Bootstrap
import ModalHeader from 'react-bootstrap/ModalHeader'
import ModalTitle from 'react-bootstrap/ModalTitle'

// Util
import {LayoutProps} from 'types'

const LayoutModalHeader = ({children}: LayoutProps) => {
	return (
		<ModalHeader className='bg-white' closeButton={useRouter().pathname !== '/adddevice' ? true : false}>
			<ModalTitle className='d-flex flex-wrap align-items-center'>{children}</ModalTitle>
		</ModalHeader>
	)
}

export default LayoutModalHeader
