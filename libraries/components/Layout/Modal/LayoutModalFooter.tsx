// Bootstrap
import ModalFooter from 'react-bootstrap/ModalFooter'

// Util
import { LayoutProps } from 'types'


const LayoutModalFooter = ({children}:LayoutProps) => {

	return (

		<ModalFooter className='justify-content-between'>

			{children}

		</ModalFooter>

	)

}

export default LayoutModalFooter