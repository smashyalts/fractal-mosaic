// Util
import {LayoutProps} from 'types'

const LayoutDashboard = ({children}: LayoutProps) => {
	return <main className='d-flex w-100 h-100 flex-grow-1 flex-column flex-sm-row overflow-hidden'>{children}</main>
}

export default LayoutDashboard
