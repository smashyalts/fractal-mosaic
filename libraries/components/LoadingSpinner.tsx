import Spinner from 'react-bootstrap/Spinner'

const LoadingSpinner = () => {
  return (
    <div className="d-flex align-items-center justify-content-center">
      <Spinner animation="border" variant="muted" size="sm" />
    </div>
  )
}

export default LoadingSpinner
