// Bootstrap
import FormControl from 'react-bootstrap/FormControl'
import InputGroup from 'react-bootstrap/InputGroup'
import Button from 'react-bootstrap/Button'

// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheck, faCopy } from '@fortawesome/free-solid-svg-icons'

// Util
import { TextInputProps } from 'types'
import useCopyToClipboard from 'hooks/useCopyToClipboard'

const TextCopy = ({ title, ...rest }: TextInputProps) => {
  const [isCopied, handleCopy] = useCopyToClipboard(1750)

  return (
    <InputGroup>
      <FormControl
        type="text"
        value={title}
        aria-label={title}
        placeholder={title}
        {...rest}
        readOnly
        disabled
      />
      <InputGroup.Text className="p-0">
        <Button
          onClick={() => handleCopy(title)}
          variant="link"
          className="bg-transparent"
        >
          <FontAwesomeIcon
            icon={isCopied ? faCheck : faCopy}
            className={isCopied ? 'text-success' : 'text-muted'}
            size="sm"
            fixedWidth
          />
        </Button>
      </InputGroup.Text>
    </InputGroup>
  )
}

export default TextCopy
