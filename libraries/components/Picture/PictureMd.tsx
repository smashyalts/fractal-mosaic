// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

// Fractal

import css from './picture.module.css'

// Util
import handleHealthStatus from 'util/handleHealthStatus'
import { PictureProps } from 'types'

const PictureMd = ({
  img,
  icn,
  health,
  isActive,
  circle = true,
  ...rest
}: PictureProps) => {
  return (
    <div
      className={`${css.picture__md} ${circle && css.picture__circle} ${
        isActive && css.picture__active
      }`}
    >
      {icn && <FontAwesomeIcon icon={icn} {...rest} fixedWidth />}
      {img && <img src={img} className={css.picture__img} />}
      {health && (
        <small>
          <FontAwesomeIcon
            icon={['fas', 'circle']}
            className={`${css.picture__status} ${
              css.picture__circle
            } ${handleHealthStatus(health)}`}
            size="sm"
          />
        </small>
      )}
    </div>
  )
}

export default PictureMd
