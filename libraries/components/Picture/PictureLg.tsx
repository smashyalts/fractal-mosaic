// Icons
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

// Fractal
import css from './picture.module.css'

// Util
import {PictureProps} from 'types'

const PictureLg = ({img, icn, isActive, circle = true, ...rest}: PictureProps) => {
	return (
		<div className={`${css.picture__lg} ${circle ? css.picture__circle : ''} ${isActive ? css.picture__active : ''}`}>
			{icn && <FontAwesomeIcon icon={icn} size='lg' fixedWidth {...rest} />}
			{img && (
				<img
					src={img}
					className={css.picture__img}
					style={
						img == `${process.env.NEXT_PUBLIC_API_BASE_URL}/media/app_catalog/logo_icon_ghost.png`
							? {objectFit: 'contain'}
							: undefined
					}
				/>
			)}
		</div>
	)
}
export default PictureLg
