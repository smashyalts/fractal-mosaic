// Fractal
import css from './picture.module.css'

interface IPictureAppDetailIcon {
	img: string
	width?: string
}

const PictureAppDetailIcon = ({img, width}: IPictureAppDetailIcon) => {
	return (
		<div className={`${css.picture__appDetailContainer}`}>
			<img src={img} className={`${css.picture__appDetailIcon} mt-3 mb-4`} style={{width: `${width}rem`}} />
		</div>
	)
}
export default PictureAppDetailIcon
