// SSR IMPORTS
import {GetServerSideProps} from 'next'
import axios from 'axios'
import {APP_CHECK_ENDPOINT} from 'util/endpoints'
import {unstable_getServerSession} from 'next-auth/next'
import {nextAuthConfig} from 'auth/auth-handler'

export const useIsAppInstalledSSR = async (context: any, APP_UUID: string) => {
	const session = await unstable_getServerSession(
		context.req,
		context.res,
		nextAuthConfig(
			process.env.NEXTAUTH_URL,
			process.env.REDIS_PORT as unknown as number,
			process.env.REDIS_HOST as string,
			process.env.REDIS_PASSWORD as string,
			process.env.KEYCLOAK_ID as string,
			process.env.KEYCLOAK_SECRET as string,
			process.env.KEYCLOAK_ISSUER as string,
			process.env.TOP_LEVEL_DOMAIN as string
		)
	)

	if (session) {
		const response = await axios.get(APP_CHECK_ENDPOINT(APP_UUID), {
			headers: {Authorization: `Bearer ${session?.accessToken}`},
		})
		const isInstalled = (): boolean => {
			if (response.data && response.data.state === 'installed') return true
			return false
		}
		return {
			props: {isInstalled: isInstalled()},
		}
	} else {
		return {
			props: {isInstalled: false},
		}
	}
}
