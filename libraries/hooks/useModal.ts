import { useState } from "react";

export type ModalProps = {
  isShowing: boolean;
  hide: () => void;
};

const useModal = () => {
  const [isShowing, setIsShowing] = useState(false);

  function toggle() {
    setIsShowing(!isShowing);
  }

  return {
    isShowing,
    toggle,
  };
};

export default useModal;
