// Next auth
import {useSession} from 'next-auth/react'

// SWR
import {useSWRConfig} from 'swr'

// Axios
import axios from 'axios'

// Endpoints
import {
	APP_LIST_ENDPOINT,
	APP_ENDPOINT,
	GENERATE_TOKEN_ENDPOINT,
	MEMBER_ENDPOINT,
	DEVICE_LIST_ENDPOINT,
	MEMBER_PASSPHRASE_ENDPOINT,
	APP_INSTANCE_CONFIG_ENDPOINT,
	ADD_ELEMENT_USER_ENDPOINT,
} from 'util/endpoints'

import useSwrAuth from 'hooks/useSwrAuth'
import {HealthType} from 'types'
import {saltPassphrase, verifyPassphrase} from 'util/passphrase'

const useMosaicApi = () => {
	const {data: session} = useSession()
	const {mutate} = useSWRConfig()

	const mosaicApi = {
		installApp: async (appUuid: string, deviceUuid: string) => {
			try {
				const res = await axios.post(
					`${APP_ENDPOINT}`,
					{app: appUuid, target_state: 'running', state: 'installed', device: deviceUuid},
					{
						headers: {Authorization: `Bearer ${session?.accessToken}`},
					}
				)
				console.log('INSTALL APP REQ: ', res)
				return res
			} catch (error) {
				console.log(error)
			}
		},
		uninstallApp: async (uuid: string) => {
			console.log('UNINSALL APP FUNC CALLED')
			try {
				const res = await axios.patch(
					`${APP_ENDPOINT}${uuid}/`,
					{state: 'not-installed'},
					{headers: {Authorization: `Bearer ${session?.accessToken}`}}
				)
				console.log('UNINSTALL APP RES: ', res)
				if (res.statusText === 'OK') {
					mutate([APP_LIST_ENDPOINT, session?.accessToken])
				}
				return res.data
			} catch (error) {
				console.log(error)
			}
		},
		rescheduleApp: async (appUuid: string, deviceUuid: string) => {
			try {
				const res = await axios.patch(
					`${APP_ENDPOINT}${appUuid}/`,
					{device: deviceUuid},
					{
						headers: {Authorization: `Bearer ${session?.accessToken}`},
					}
				)
				console.log('rescheduleApp REQ: ', res)
				return res
			} catch (error) {
				console.log(error)
			}
		},
		setAppInstanceTargetState: async (uuid: string, targetState: 'stopped' | 'running') => {
			console.log('AppInstanceTargetState FUNC CALLED')
			try {
				const res = await axios.patch(
					`${APP_ENDPOINT}${uuid}/`,
					{target_state: targetState},
					{headers: {Authorization: `Bearer ${session?.accessToken}`}}
				)
				console.log('AppInstanceTargetState RES: ', res)
				if (res.statusText === 'OK') {
					setTimeout(() => {
						mutate([APP_LIST_ENDPOINT, session?.accessToken])
					}, 5000)
				}
				return res.data
			} catch (error) {
				console.log(error)
			}
		},
		generateDeviceToken: async () => {
			try {
				const response = await axios.post(
					`${GENERATE_TOKEN_ENDPOINT}`,
					{},
					{
						headers: {
							Authorization: `Bearer ${session?.accessToken}`,
						},
					}
				)
				return response.data
			} catch (error) {
				console.log(error)
				return 'error'
			}
		},
		deleteDevice: async (uuid: string) => {
			try {
				const res = await axios.delete(`${DEVICE_LIST_ENDPOINT}${uuid}/`, {
					headers: {Authorization: `Bearer ${session?.accessToken}`},
				})
				console.log('DELETE DEVICE RESPONSE: ', res)
				if (res.status === 204) {
					mutate([DEVICE_LIST_ENDPOINT, session?.accessToken])
				}
				return res
			} catch (error) {
				console.log(error)
			}
		},
		updateDeviceName: async (deviceUuid: string, name: string) => {
			try {
				const res = await axios.patch(
					`${DEVICE_LIST_ENDPOINT}${deviceUuid}/`,
					{name},
					{headers: {Authorization: `Bearer ${session?.accessToken}`}}
				)
				console.log('updateDeviceName: ', res)
				if (res.statusText === 'OK') {
					mutate([`${DEVICE_LIST_ENDPOINT}${deviceUuid}/`, session?.accessToken])
				}
				return res
			} catch (error) {
				console.log(error)
			}
		},
		getAllDevicesStatusForMembersHealth: (): HealthType => {
			const {data, error} = useSwrAuth(DEVICE_LIST_ENDPOINT)
			if (data) {
				if (data.every((item: any) => item.health === 'green')) return 'green'
				if (data.every((item: any) => item.health === 'red')) return 'red'
				return 'yellow'
			} else {
				return 'red'
			}
		},
		appCatalogSearch: async (search?: string, categories?: string[], sort?: 'asc' | 'desc') => {
			const apiUrl = new URL(`${process.env.NEXT_PUBLIC_MOSAIC_API}catalog/search/`)
			search && apiUrl.searchParams.append('search', search)
			if (categories && categories.length > 0) {
				for (let category of categories) {
					apiUrl.searchParams.append('categories', category.toLowerCase())
				}
			}
			sort && apiUrl.searchParams.append('sort', sort)
			try {
				const response = await axios.get(apiUrl.toString())
				return response.data
			} catch (error) {
				console.log('ERROR FROM APP CATALOG SEARCH: ', error)
			}
		},
		uploadUserImageToDjango: async (image: File) => {
			const formData = new FormData()
			formData.append('profile_image', image, image.name)
			try {
				const response = await axios.patch(`${process.env.NEXT_PUBLIC_MOSAIC_API}member/${session?.uuid}/`, formData, {
					headers: {Authorization: `Bearer ${session?.accessToken}`},
				})
				console.log('RESPONSE FROM IMAGE UPLOAD: ', response)
				if (response.status === 200) {
					mutate([`${process.env.NEXT_PUBLIC_MOSAIC_API}member/${session?.uuid}/`, session?.accessToken])
				}
				return response
			} catch (error) {
				console.log('ERROR FROM IMAGE UPLOAD: ', error)
			}
		},
		updateUsersDisplayName: async (name: string, uuid: string) => {
			try {
				const res = await axios.patch(
					`${MEMBER_ENDPOINT}${uuid}/`,
					{display_name: name},
					{headers: {Authorization: `Bearer ${session?.accessToken}`}}
				)
				console.log('updateUsersDisplayName: ', res)
				return res
			} catch (error) {
				console.log(error)
			}
		},
		getPassphrase: async (userUuid: string) => {
			try {
				const response = await axios.get(`${MEMBER_PASSPHRASE_ENDPOINT(session?.uuid as string)}`, {
					// const response = await axios.get(`${MEMBER_PASSPHRASE_ENDPOINT(userUuid)}`, {
					headers: {Authorization: `Bearer ${session?.accessToken}`},
				})
				return response
			} catch (error) {
				console.log(error)
			}
		},
		setPassphrase: async (passphrase: string) => {
			const {base64String, salt} = await saltPassphrase(passphrase)
			try {
				const response = await axios.post(
					`${MEMBER_PASSPHRASE_ENDPOINT(session?.uuid as string)}`,
					{salted_passphrase: base64String, salt},
					{
						headers: {Authorization: `Bearer ${session?.accessToken}`},
					}
				)
			} catch (error) {
				console.log(error)
			}
		},
		appInstanceConfig: async (username: string, password: string, appInstanceUuid: string) => {
			try {
				const response = await axios.post(
					`${APP_INSTANCE_CONFIG_ENDPOINT}`,
					{data: {username, password}, appinstance: appInstanceUuid},
					{
						headers: {Authorization: `Bearer ${session?.accessToken}`},
					}
				)
				console.log(response)
				return response
			} catch (error) {
				console.log(error)
			}
		},
		addElementUser: async (username: string, password: string, appInstanceUuid: string, admin?: boolean) => {
			const postBody =
				admin !== undefined
					? {username, password, appinstance: appInstanceUuid, admin: admin.toString()}
					: {username, password, appinstance: appInstanceUuid}
			try {
				const response = await axios.post(`${ADD_ELEMENT_USER_ENDPOINT(appInstanceUuid)}`, postBody, {
					headers: {Authorization: `Bearer ${session?.accessToken}`},
				})
				console.log(response)
				return response
			} catch (error) {
				console.log(error)
			}
		},
	}

	return mosaicApi
}

export default useMosaicApi
