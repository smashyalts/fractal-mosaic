import { SetterOrUpdater } from "recoil";
import { ActionsType, ModalProps } from "../types";

export default function useActions(
  uuid: string | undefined,
  actions: ActionsType[],
  setState: SetterOrUpdater<ModalProps>[]
) {
  return actions.map((action: ActionsType, idx) => {
    return {
      ...action,
      func: () => {
        if (uuid) {
          setState[idx]((state) => ({
            uuid,
            show: !state.show,
          }));
        } else {
          throw new Error("trying to open modal without a uuid");
        }
      },
    };
  });
}
