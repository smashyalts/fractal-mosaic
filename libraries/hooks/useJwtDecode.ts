// NextAuth
import {useSession, getSession} from 'next-auth/react'

// JWT DECODE
import jwt_decode from 'jwt-decode'

const useJwtDecode = () => {
	let {data: session} = useSession()
	const decodeJwt: any = jwt_decode(session?.accessToken as string)
	const id = decodeJwt.sub
	return {id}
}

export default useJwtDecode
