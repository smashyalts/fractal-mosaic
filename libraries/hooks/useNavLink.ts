import { useRouter } from "next/router";
import { UuidType, LinkToType } from "types";

const useNavLink = (
  uuid?: UuidType,
  linkTo?: LinkToType,
  action?: () => void | undefined
) => {
  const router = useRouter();
  if (action) {
    if (router.pathname === "/") return "/";
    const currentGroup = router.query.uuid;
    return `/group/${currentGroup}`;
  }
  else if (linkTo == "home") {
    return "/";
  }
  return `/group/${uuid}`;
};

export default useNavLink;
