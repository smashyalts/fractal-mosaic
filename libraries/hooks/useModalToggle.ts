import { SetterOrUpdater } from "recoil";
import { ModalProps } from "../types";

export default function useModalToggle(setState: SetterOrUpdater<ModalProps>) {
  return () => {
    setState((state) => ({
      ...state,
      show: !state.show,
    }));
  };
}
