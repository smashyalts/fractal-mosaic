// Next auth
import { useSession } from "next-auth/react";

// Swr
import useSWR from "swr";

// Fetcher
import fetcher from "util/fetcher";

const useSwrAuth = (endpoint: string) => {
  const { data: session } = useSession();

  const { data, error } = useSWR([endpoint, session?.accessToken], fetcher);

  return {
    data,
    isLoading: !error && !data,
    error,
  };
};

export default useSwrAuth;
