import axios from 'axios'

const fetcher = (endpoint: string, token: string) =>
	axios
		.get(endpoint, {headers: {Authorization: `Bearer ${token}`}})
		.then((res) => res.data)
		.catch((e) => {
			console.log(e)
		})

export default fetcher
