const handleQuery = (queryVar: string): Promise<any> => {
	const query = `
      query {
        search(version:"6.0.0", query: "${queryVar}", first: 100) {
          id
          label
          membership {
            free
          }
        }
      }`
	return new Promise((resolve, reject) => {
		const headers = {
			'Content-Type': 'application/json',
		}
		return fetch('https://api.fontawesome.com', {
			method: 'POST',
			headers,
			// TODO: this is causing huge slow down. May be made faster with pagination.
			body: JSON.stringify({query}),
		})
			.then((response) => {
				if (response.ok) {
					response
						.json()
						.then((json) => resolve(json))
						.catch((e) => {
							reject(e)
						})
				} else {
					reject('bad query')
				}
			})
			.catch((e) => {
				reject(e)
			})
	})
}

export default handleQuery
