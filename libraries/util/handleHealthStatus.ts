import {HealthType} from 'types'

const handleHealthStatus = (health?: HealthType) => {
	if (health === 'green') {
		return 'text-success'
	} else if (health === 'yellow') {
		return 'text-warning'
	} else return 'text-danger'
}

export default handleHealthStatus
