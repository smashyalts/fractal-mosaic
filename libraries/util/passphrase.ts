import crypto from 'crypto'

async function hash(passphrase: string, salt?: string) {
	if (!salt) {
		salt = crypto.randomBytes(16).toString('base64')
	}
	const msgBuffer = new TextEncoder().encode(salt + passphrase)
	const salted_passphrase = await window.crypto.subtle.digest('SHA-256', msgBuffer)
	const base64String = window.btoa(String.fromCharCode(...new Uint8Array(salted_passphrase)))
	return {base64String, salt}
}

export async function saltPassphrase(passphrase: string) {
	return hash(passphrase)
}
export async function verifyPassphrase(passphrase: string, salt: string, saltedPassphrase: string) {
	const {base64String} = await hash(passphrase, salt)
	return base64String == saltedPassphrase
}
