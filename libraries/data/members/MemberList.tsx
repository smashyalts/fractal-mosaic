// React
import React from 'react'

// SWR
import useSWR from 'swr'
import fetcher from 'util/fetcher'

// Fractal
import Member from './Member'
import LoadingSpinner from 'components/Spinners/LoadingSpinner'

// Typescript
import { DataProps } from 'types'

const MemberList = ({ children, uuid }: DataProps) => {
  // Get list of Members from backend
  const { data, error } = useSWR(`/api/v1/member`, fetcher)
  // const data = false // for testing
  // const error = true // for testing

  const renderList = () => {
    // success
    if (data) {
      return data.map((uuid: string) => {
        return <Member uuid={uuid} children={children} key={uuid} />
      })

      // error
    } else if (error) {
      return console.error("Can't load Members List:", error)
    }

    // loading
    return <LoadingSpinner />
  }

  return <>{renderList()}</>
}

export default MemberList
