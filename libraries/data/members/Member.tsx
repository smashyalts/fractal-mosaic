// React
import React from 'react'

import {useSession} from 'next-auth/react'

// SWR
import useSWR from 'swr'
import fetcher from 'util/fetcher'
import useSwrAuth from 'hooks/useSwrAuth'

// Types
import {DataProps} from 'types'
import useMosaicApi from 'hooks/useMosaicApi'

// takes child component and adds data to them as a prop
const Member = ({children, uuid}: DataProps) => {
	const {getAllDevicesStatusForMembersHealth} = useMosaicApi()
	const {data: session} = useSession()

	const {data, error} = useSwrAuth(`${process.env.NEXT_PUBLIC_MOSAIC_API}member/${session?.uuid}/`)

	const img: string | undefined = data && data.profile_image ? data.profile_image : undefined

	const health = getAllDevicesStatusForMembersHealth()

	// success
	if (data) {
		const {display_name: title} = data
		return React.cloneElement(children, {title, health, img, uuid})

		// error
	} else if (error) {
		const title = 'error'
		const img = '' // TODO add hardcoded error image
		console.error("Can't load Members:", error)
		return React.cloneElement(children, {health, title, img, uuid})
	}

	// loading
	const loading = 'true'
	return React.cloneElement(children, {loading, health, img, uuid})
}

export default Member
