// Bootstrap
import Button from 'react-bootstrap/Button'

const AppAdd = () => {

	return <Button className="flex-fill flex-sm-grow-0 flex-sm-shrink-0" variant="primary" size="sm">App Store</Button>
	
}

export default AppAdd