// React
import React from 'react'

// Utils
import {DataProps} from 'types'
import useSwrAuth from 'hooks/useSwrAuth'

// Endpoints
import {APP_ENDPOINT} from 'util/endpoints'
import useMosaicApi from 'hooks/useMosaicApi'

//takes child component and adds data to them as a prop
const App = ({children, uuid}: DataProps) => {
	const {setAppInstanceTargetState: appPowerSwitch} = useMosaicApi()

	// Get App data from API
	const {data, error} = useSwrAuth(`${APP_ENDPOINT}${uuid}/`)
	// const data = false // for testing
	// const error = false // for testing

	// success
	if (data) {
		const {
			app,
			name: title,
			image: img,
			health,
			links: {
				default: {domain: url},
			},
			target_state,
			device_name,
			config,
		} = data
		return React.cloneElement(children, {
			app,
			title,
			img,
			health,
			url,
			uuid,
			target_state,
			device_name,
			config,
			appPowerSwitch,
		})

		// error
	} else if (error) {
		const title = 'error'
		const img = '' // TODO add hardcoded error image
		const health = 'error'
		console.error("Can't load App:", error)
		return React.cloneElement(children, {health, title, img, uuid})
	}

	// loading
	const loading = 'true'
	const img = true
	const health = true
	const url = true
	return React.cloneElement(children, {loading, img, health, url, uuid})
}

export default App
