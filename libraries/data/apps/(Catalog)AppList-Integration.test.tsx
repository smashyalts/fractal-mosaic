import React from 'react'
import { render, screen, waitFor } from '@testing-library/react'
import '@testing-library/jest-dom'
// TODO - FIX TESTING WRAPPERS
import { TestWrapper, SignedInWrapper } from '../../../util/testing/testing-wrapper'
import CatalogAppList from 'data/apps/CatalogAppList'
import AppList from 'data/apps/AppList'
import CardXl from 'components/CardXl'

// Endpoints
import { APP_ENDPOINT } from 'util/endpoints'

// Add icon library for Jest to resolve icons with [IconPrefix, IconName] format
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
library.add(fas, fab)

// MSW
import { setupServer } from 'msw/node'
import { rest } from 'msw'

const server = setupServer(
    // Mock AppList and CatalogApp data components
    rest.get(APP_ENDPOINT, (req, res, ctx) => {
        const installedQuery = req.url.searchParams.get('installed')
        if (installedQuery === "True") {
            return res(
                ctx.json([{ uuid: '1234567890' }])
            )
        }
        return res(
            ctx.json([{ uuid: '0987654321' }])
        )
    }),
    // Mock App data components - Installed Apps
    rest.get(`${APP_ENDPOINT}1234567890/`, (req, res, ctx) => {
        return res(
            ctx.json({
                name: 'Element',
                image: 'test/image',
                health: 'green',
                url: 'test-url.com'
            })
        )
    }),
    // Mock CatalogApp data components - Uninstalled Apps
    rest.get(`${APP_ENDPOINT}0987654321/`, (req, res, ctx) => {
        return res(
            ctx.json({
                name: 'BitWarden',
                image: 'another/test/image',
                health: 'red',
            })
        )
    })
)

beforeAll(() => server.listen());
afterAll(() => server.close());
afterEach(() => server.resetHandlers());

describe("Logged In", () => {
    it('Displays data from api', async () => {
        render(
            <>
                {/* @ts-ignore */}
                <CatalogAppList children={<CardXl />} />
                {/* @ts-ignore */}
                <AppList children={<CardXl />} />
            </>,
            { wrapper: SignedInWrapper }
        )
        // CatalogApp Title is rendered
        await waitFor(() => expect(screen.getByText(/BitWarden/gi)).toBeInTheDocument())

        // CatalogApp install button is rendered
        await waitFor(() => expect(screen.getByText(/Install App/gi)).toBeInTheDocument())

        // App Title is rendered
        await waitFor(() => expect(screen.getByText(/Element/gi)).toBeInTheDocument())

        // App url is rendered if one exists
        await waitFor(() => expect(screen.getByText(/test-url.com/gi)).toBeInTheDocument())

        // Health icons are rendered
        const healthIcons = document.querySelectorAll('svg[data-icon = circle]')
        expect(healthIcons.length).toEqual(2)

        // Uninstalled Health Icon is Red
        expect(healthIcons[0].classList.contains('text-danger')).toEqual(true)

        // Installed Health Icon is Green
        expect(healthIcons[1].classList.contains('text-success')).toEqual(true)
    })
})