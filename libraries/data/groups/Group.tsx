// React
import React from 'react'

// SWR
import useSWR from 'swr'
import fetcher from 'util/fetcher'

// Util
import { DataProps } from 'types'
import useIsActive from 'hooks/useIsActive'

// takes child component and adds data to them as a prop
const Group = ({ children, uuid }: DataProps) => {
  // Get Group data from API
  const { data, error } = useSWR(`/api/v1/group/${uuid}`, fetcher)
  // const data = false // for testing
  // const error = true // for testing

  const isActive = useIsActive(uuid)

  // success
  if (data) {
    const { displayName: title, icn, img, health } = data
    return React.cloneElement(children, {
      title,
      icn,
      img,
      health,
      uuid,
      isActive,
    })

    // error
  } else if (error) {
    const title = 'error'
    const img = '' // TODO add hardcoded error image
    const health = 'error'
    console.error("Can't load Group:", error)
    return React.cloneElement(children, { health, title, img, uuid })
  }

  // loading
  const loading = 'true'
  const img = true
  const icn = true
  const health = true
  return React.cloneElement(children, { loading, icn, img, health, uuid })
}

export default Group
