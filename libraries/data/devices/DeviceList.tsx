// React
import React from 'react'

// SWR
import useSwrAuth from 'hooks/useSwrAuth'

// Fractal
import Device from './Device'
import LoadingSpinner from 'components/Spinners/LoadingSpinner'

// Typescript
import {DataProps} from 'types'

// Next auth
import {useSession} from 'next-auth/react'

// Fractal Endpoints
import {DEVICE_LIST_ENDPOINT} from 'util/endpoints'

const DeviceList = ({children, uuid}: DataProps) => {
	const {data: session} = useSession()

	// Get list of Devices from backend
	const {data, error} = useSwrAuth(DEVICE_LIST_ENDPOINT)

	const renderList = () => {
		// success
		if (data) {
			return data.map((deviceObj: any) => {
				const {uuid} = deviceObj
				return <Device uuid={uuid} children={children} key={uuid} />
			})

			// error
		} else if (error) {
			return console.error("Can't load Device List:", error)
		}

		// loading
		return <LoadingSpinner />
	}

	return <>{renderList()}</>
}

export default DeviceList
