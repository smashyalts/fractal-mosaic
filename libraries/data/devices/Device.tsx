// React
import React from 'react'

// SWR
import useSwrAuth from 'hooks/useSwrAuth'

// Util
import {DataProps} from 'types'

// Next auth
import {useSession} from 'next-auth/react'

// Fractal Endpoints
import {DEVICE_LIST_ENDPOINT} from 'util/endpoints'

// takes child component and adds data to them as a prop
const Device = ({uuid, children}: DataProps) => {
	const {data: session} = useSession()

	// Get Member data from API
	const {data, error} = useSwrAuth(`${DEVICE_LIST_ENDPOINT}${uuid}/`)

	// success
	if (data) {
		const {name: title, health, maxStorage} = data
		return React.cloneElement(children, {title, health, uuid, maxStorage})

		// error
	} else if (error) {
		const title = 'error'
		const health = 'error'
		console.error("Can't load Device:", error)
		return React.cloneElement(children, {health, title, uuid})
	}

	// loading
	const loading = 'true'
	const health = true
	return React.cloneElement(children, {loading, health, uuid})
}

export default Device
