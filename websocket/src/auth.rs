//! Authentication helpers
//!
//! This module adds authentication helpers that are used to validate device tokens.
use crate::Options;
use anyhow::*;
use reqwest::Client;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

impl Options {
    /// Given a device token and a HTTP client, validate the device token by making a
    /// HTTP request.
    pub async fn check_device_token(
        &self,
        client: &Client,
        token: &str,
    ) -> Result<DeviceTokenResponse> {
        #[cfg(feature = "insecure-auth-stub")]
        if self.insecure_auth_stub {
            let mut token_parts = token.split(",");
            let user: Uuid = token_parts.next().unwrap().parse().unwrap();
            let device: Uuid = token_parts.next().unwrap().parse().unwrap();
            return Ok(DeviceTokenResponse { user, device });
        }

        let request = DeviceTokenRequest { token };

        let response: DeviceTokenResponse = client
            .post(&self.auth.to_string())
            .header("Authorization", format!("Bearer {}", self.jwt))
            .json(&request)
            .send()
            .await?
            .json()
            .await?;

        Ok(response)
    }
}

/// Request to validate device token.
#[derive(Clone, Debug, Serialize)]
pub struct DeviceTokenRequest<'a> {
    token: &'a str,
}

/// Response of validating device token.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct DeviceTokenResponse {
    user: Uuid,
    device: Uuid,
}

impl DeviceTokenResponse {
    /// Get the user UUID
    pub fn user(&self) -> Uuid {
        self.user
    }

    /// Get the device UUID.
    pub fn device(&self) -> Uuid {
        self.device
    }
}
