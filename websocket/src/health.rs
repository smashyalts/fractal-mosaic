use crate::Health;
use anyhow::*;
use async_tungstenite::tokio::connect_async;
use async_tungstenite::tungstenite::Error;

/// Perform health check by connecting to websocket.
pub async fn check(health: &Health) -> Result<()> {
    use Error::*;
    match connect_async(&health.address).await {
        Result::Ok(_) => Ok(()),
        Err(ConnectionClosed) => Ok(()),
        Err(err) => Err(err.into()),
    }
}
