import asyncio
import websockets
import aio_pika
import os
import pytest
import json
from aio_pika import Message, ExchangeType

ACCOUNT_UUID = "a675c452-235b-4abf-b4a5-b8ee23121158"
DEVICE_UUID = "6e6ca203-5343-499d-ba28-b26a333ea579"
#DEVICE_TOKEN = "f39ecc9056142f2dfb79f66a44e81c3450c7af84"
DEVICE_TOKEN = f"{ACCOUNT_UUID},{DEVICE_UUID}"

# this fixes some weird exception ignored errors related to async stuff.
# see: https://stackoverflow.com/questions/65740542
@pytest.fixture
def event_loop():
    loop = asyncio.get_event_loop()

    yield loop

    pending = asyncio.tasks.all_tasks(loop)
    loop.run_until_complete(asyncio.gather(*pending))
    loop.run_until_complete(asyncio.sleep(1))

    loop.close()

async def test_connect_rabbitmq():
    rabbit = await aio_pika.connect(os.environ.get("AMQP_URL"))
    channel = await rabbit.channel()
    await channel.close()
    await rabbit.close()

async def test_connect_websocket():
    headers = {"Authorization": f"Bearer {DEVICE_TOKEN}"}
    websocket_url = os.environ.get("HIVE_WEBSOCKET")
    websocket = await websockets.connect(websocket_url, extra_headers = headers)
    await websocket.close()

async def test_forward_event():
    # connect to websocket
    headers = {"Authorization": f"Bearer {DEVICE_TOKEN}"}
    websocket_url = os.environ.get("HIVE_WEBSOCKET")
    websocket = await websockets.connect(websocket_url, extra_headers = headers)

    # connect to rabbitmq
    rabbit = await aio_pika.connect(os.environ.get("AMQP_URL"))
    channel = await rabbit.channel()

    # declare exchange
    events_exchange = await channel.declare_exchange("events", type = ExchangeType.HEADERS)

    # publish event
    headers = {
        "account": ACCOUNT_UUID,
        "device": DEVICE_UUID,
        "type": "some_type",
        "service": "some_service",
        "some": "value"
    }
    message = Message(body = b"", headers = headers)
    await events_exchange.publish(message, routing_key="")

    # wait for event on websocket
    message = None

    # keep reading messages until we get the right one
    while True:
        message = await websocket.recv()
        message = json.loads(message)
        if message["type"] == "some_type":
            break

    # make sure data is same
    assert message["account"] == headers["account"]
    assert message["device"] == headers["device"]
    assert message["type"] == headers["type"]
    assert message["service"] == headers["service"]
    assert message["some"] == headers["some"]

    # close connections
    await websocket.close()
    await channel.close()
    await rabbit.close()
