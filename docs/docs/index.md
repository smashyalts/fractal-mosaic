title: Overview

# <img src="media/fractal-wordmark.svg" alt="Fractal Networks" style="height: 1.7ex;" /> Mosaic Overview

Here's an overview of the services and their dependencies that make up
Fractal Mosaic. All off-the-shelf components are indicated in pink.

```mermaid
graph BT
	Backend[Hive Backend]
	Frontend -->|RESP| Redis
	Backend -->|RESP| Redis
	Backend -->|SQL| Database
	Frontend -->|REST API| Backend
	Backend -->|AMQP| RabbitMQ
	DeviceService[Device Service] -->|AMQP| RabbitMQ
	WebSocket[WebSocket Servcice] -->|AMQP| RabbitMQ
	Device -->|WebSocket| WebSocket
	Backend <-->|REST API| DeviceService

	style RabbitMQ fill:#f9f
	style Database fill:#f9f
	style Redis fill:#f9f
```

## Services

| Service | Description |
| --- | --- |
| Hive Backend | Business logic, handles devices. Contains a worker for async tasks. |
| Frontend | Next JS user interface |
| Device Service | Delivers device events to Hive Backend, lets Hive Backend send events and requests to Devices via RabbitMQ |
| WebSocket Service | Maintains WebSocket connection to devices and relays messages to RabbitMQ |
| RabbitMQ | Message broker, used to relay messages between Hive Backend and Devices and used to coordinate tasks for the async worker. |
| Database | Database (SQLite) |
| Redis | Cache, used by frontend and backend. |

