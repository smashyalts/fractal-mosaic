# Device Communication

```mermaid
graph BT
	Backend[Hive Backend]
	DeviceService[Device Service] <-->|AMQP| RabbitMQ
	WebSocket[WebSocket Servcice] <-->|AMQP| RabbitMQ
	Device <-->|WebSocket| WebSocket
	Backend <-->|REST API| DeviceService

	style RabbitMQ fill:#f9f
```

Upon starting, the device establishes a WebSocket connection with the Fractal
Mosaic backend. This WebSocket connection signals to the backend that the device
is ready and reachable. Through this connection, the Device receives requests and
it sends status events. 

We are using the reliable RabbitMQ message broker to implement this messaging
configuration. There are two services involved in handling the messaging between 
the Devices and the Hive Backend:

- The *WebSocket Service* offers a WebSocket that Devices can connect to. Upon
  connecting, they can authenticate after which any messages they sent will be
  broadcast using RabbitMQ, and any messages destined to them will be relayed
  to them from RabbitMQ.

- The *Device Service* maintains a connection to RabbitMQ and delivers all messages
  from Devices to the Hive Backend using an HTTP POST request. The Device Service
  also offers two endpoints for the Hive Backend to send events or requests to
  Devices. The difference between events and requests is that requests need to be
  confirmed, whereas events do not.

From the point of view of the Device or the Hive Backend, events are JSON objects
that have some mandatory fields. RabbitMQ uses a custom header format, for this
reason the [amqp-value-json](https://docs.rs/amqp-value-json/0.1.0/amqp_value_json/)
crate is used to provide a mapping of JSON to AMQP headers, which is done transparently.

## Flows

### Device Connection

Upon connecting to the WebSocket, the device is asked to authenticate. If successful,
the WebSocket service will forward all events for that device (that have a matching
`device` UUID in the event) to the device. All events from the device are sent to
RabbitMQ (with some metadata injected).

```mermaid
flowchart LR
    Connect-->CheckAuth[Check Device Token]
    CheckAuth-->|Bad| Disconnect[Disconnect with error]
    CheckAuth-->|Good| Subscribe[Subscribe to<br />RabbitMQ events]
    Subscribe-->|Good| Loop[Wait for events<br />or messages]
    Subscribe-->|Bad| Disconnect
    Loop-->|WebSocket Message| GotMessage[Sent message<br /> to RabbitMQ]
    Loop-->|RabbitMQ Event| GotEvent[Send message<br />to Websocket]
    Loop-->|Error| Disconnect
    GotMessage-->Loop
    GotEvent-->Loop
```

### Hive Backend Event
### Hive Backend Request
### Hive Backend Delivery
