use crate::types::Event;
use crate::Global;
use amqp_value_json::ToJson;
use anyhow::{anyhow, Result};
use lapin::options::BasicConsumeOptions;
use lapin::types::{AMQPValue, FieldTable};
use log::*;
use rocket::futures::StreamExt;
use rocket::serde::json;
use std::time::Duration;

pub async fn run(global: Global) {
    loop {
        if let Err(error) = try_run(&global).await {
            error!("Error running callback: {error:?}");
            tokio::time::sleep(Duration::from_millis(100)).await;

            if !global.connection.status().connected() {
                global.shutdown.notify_one();
                break;
            }
        }
    }
}

pub async fn try_run(global: &Global) -> Result<()> {
    let channel = global.connection.create_channel().await?;

    let mut consumer = channel
        .basic_consume(
            &global.options.callback_queue,
            "hive-device-service-callback",
            BasicConsumeOptions::default(),
            FieldTable::default(),
        )
        .await?;
    let callback_url = global.options.callback_url.to_string();

    info!("Starting callback event loop");
    loop {
        let delivery = consumer.next().await.ok_or(anyhow!("stream closed"))??;
        debug!("Received event {delivery:#?}");
        let header = delivery
            .properties
            .headers()
            .as_ref()
            .ok_or(anyhow!("Missing header for event"))?;
        let header: json::Value = AMQPValue::FieldTable(header.clone()).to_json_value()?;
        let event: Event = json::from_value(header)?;
        debug!("Parsed event as {event:#?}");
        let response = global
            .client
            .post(&callback_url)
            .header("Authorization", &format!("Bearer {}", &global.options.jwt))
            .json(&event)
            .send()
            .await?;
        delivery.ack(Default::default()).await?;
        if !response.status().is_success() {
            return Err(anyhow!("Non-success status code: {}", response.status()));
        }
    }
}
