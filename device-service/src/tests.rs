use crate::types::{Event, Request, Response};
use crate::Options;
use amqp_value_json::{ToAmqp, ToJson};
use anyhow::Result;
use lapin::options::{BasicConsumeOptions, QueueDeclareOptions};
use lapin::types::{AMQPValue, FieldTable};
use lapin::{BasicProperties, Channel, Connection, ConnectionProperties, ExchangeKind, Queue};
use reqwest::Client;
use rocket::futures::StreamExt;
use std::collections::BTreeMap;
use std::net::{IpAddr, Ipv4Addr, SocketAddr};
use std::sync::{Arc, Mutex};
use std::time::Duration;
use tokio::sync::{broadcast, oneshot};
use url::Url;
use uuid::Uuid;
use warp::http::StatusCode;
use warp::Filter;

const AMQP_EXCHANGE: &str = "events";
const AMQP_CALLBACK_QUEUE: &str = "device-service-events";
const JWT: &str = "token";

/// Generate new options for the service with a unique port.
fn service_options(callback_url: Url, port: u16) -> Result<Options> {
    Ok(Options {
        amqp: Url::parse(&std::env::var("AMQP_URL")?)?,
        amqp_exchange: format!("{AMQP_EXCHANGE}-{port}"),
        jwks: None,
        jwt: JWT.into(),
        callback_url,
        callback_queue: format!("{AMQP_CALLBACK_QUEUE}-{port}"),
        auth_insecure_stub: true,
        timeout: Duration::from_secs(60),
        listen: SocketAddr::new(IpAddr::V4(Ipv4Addr::LOCALHOST), port),
    })
}

async fn amqp_connect(options: &Options) -> Result<Connection> {
    let connection = Connection::connect(
        &options.amqp.to_string(),
        ConnectionProperties::default()
            .with_executor(tokio_executor_trait::Tokio::current())
            .with_reactor(tokio_reactor_trait::Tokio),
    )
    .await?;

    Ok(connection)
}

async fn amqp_channel(options: &Options, connection: &Connection) -> Result<Channel> {
    // open up channel, this will be used to send all of the events.
    let channel = connection.create_channel().await?;

    // declare the exchange. in case this service starts first, the
    // exchange might not yet exist. if it does, this is ignored.
    channel
        .exchange_declare(
            &options.amqp_exchange,
            ExchangeKind::Headers,
            Default::default(),
            Default::default(),
        )
        .await?;

    Ok(channel)
}

async fn amqp_queue_subscription(
    options: &Options,
    channel: &Channel,
    filter: FieldTable,
) -> Result<Queue> {
    let mut queue_options = QueueDeclareOptions::default();
    queue_options.exclusive = true;
    let queue = channel
        .queue_declare("", queue_options, Default::default())
        .await
        .unwrap();

    // create subscription for messages for this device
    channel
        .queue_bind(
            queue.name().as_str(),
            &options.amqp_exchange,
            "#",
            Default::default(),
            filter,
        )
        .await
        .unwrap();

    Ok(queue)
}

/// Wait for the service to become reachable
async fn service_await(options: &Options) {
    let client = Client::new();
    let mut interval = tokio::time::interval(Duration::from_millis(10));
    loop {
        interval.tick().await;
        let response = client
            .post(&format!("http://{}/event", options.listen))
            .send()
            .await;
        match response {
            Err(e) if e.is_connect() => {}
            _ => break,
        }
    }
}

#[ignore]
#[tokio::test]
async fn send_event() {
    // create options
    let options = service_options(Url::parse("http://localhost:1234").unwrap(), 18000).unwrap();

    // connect to AMQP and create channel
    let connection = amqp_connect(&options).await.unwrap();
    let channel = amqp_channel(&options, &connection).await.unwrap();

    // start service and wait for to be reachable
    let options_clone = options.clone();
    let server = tokio::spawn(async move {
        options_clone.run().await.unwrap();
    });
    service_await(&options).await;

    // event to send
    let event = Event {
        account: Uuid::new_v4(),
        device: Uuid::new_v4(),
        service: "hive-backend".to_string(),
        r#type: "test-message".to_string(),
        other: Default::default(),
    };

    // create subscription for messages for this device
    let mut subscription = FieldTable::default();
    subscription.insert(
        "x-match".to_string().into(),
        AMQPValue::LongString("all".to_string().into()),
    );
    subscription.insert(
        "account".to_string().into(),
        AMQPValue::LongString(event.account.to_string().into()),
    );
    subscription.insert(
        "device".to_string().into(),
        AMQPValue::LongString(event.device.to_string().into()),
    );
    let queue = amqp_queue_subscription(&options, &channel, subscription)
        .await
        .unwrap();

    // send event via API
    let client = Client::new();
    let response = client
        .post(&format!("http://{}/api/v1/event", options.listen))
        .header("Authorization", &format!("Bearer {}", Uuid::new_v4()))
        .json(&event)
        .send()
        .await
        .unwrap();
    assert!(response.status().is_success());

    // create consumer to await messages
    let mut consumer = channel
        .basic_consume(
            queue.name().as_str(),
            "hive-device-service-test",
            BasicConsumeOptions::default(),
            FieldTable::default(),
        )
        .await
        .unwrap();

    let message = consumer.next().await.unwrap().unwrap();
    let headers = message.properties.headers().as_ref().unwrap();
    let headers = AMQPValue::FieldTable(headers.clone())
        .to_json_value()
        .unwrap();
    let actual_event: Event = serde_json::from_value(headers).unwrap();
    assert_eq!(actual_event, event);

    // stop server
    server.abort();
    let _ = server.await;
}

async fn send_response(
    options: Options,
    channel: Channel,
    setup: oneshot::Sender<()>,
) -> Result<()> {
    // create subscription for messages for this device
    let mut subscription = FieldTable::default();
    subscription.insert(
        "x-match".to_string().into(),
        AMQPValue::LongString("all".to_string().into()),
    );
    subscription.insert(
        "type".to_string().into(),
        AMQPValue::LongString("test-request".to_string().into()),
    );
    let queue = amqp_queue_subscription(&options, &channel, subscription).await?;

    let mut consumer = channel
        .basic_consume(
            queue.name().as_str(),
            "hive-device-service-test",
            BasicConsumeOptions::default(),
            FieldTable::default(),
        )
        .await
        .unwrap();

    // notify that it's set up
    setup.send(()).unwrap();

    let message = consumer.next().await.unwrap().unwrap();
    let headers = message.properties.headers().as_ref().unwrap();
    let headers = AMQPValue::FieldTable(headers.clone())
        .to_json_value()
        .unwrap();
    let request: Request = serde_json::from_value(headers).unwrap();
    let response = Response {
        account: request.account,
        device: request.device,
        service: "hive-device".to_string(),
        r#type: request.r#type,
        request: request.request.unwrap(),
        other: {
            let mut map = BTreeMap::new();
            let a = request.other.get("a").unwrap().as_f64().unwrap();
            let b = request.other.get("b").unwrap().as_f64().unwrap();
            map.insert("sum".into(), (a + b).into());
            map
        },
    };
    let response = serde_json::to_value(&response).unwrap();
    let response = match response.to_amqp_value().unwrap() {
        AMQPValue::FieldTable(table) => table,
        _ => unreachable!(),
    };

    let properties = BasicProperties::default().with_headers(response);
    let _result = channel
        .basic_publish(
            &options.amqp_exchange,
            "",
            Default::default(),
            &[],
            properties,
        )
        .await
        .unwrap();

    Ok(())
}

#[ignore]
#[tokio::test]
async fn send_request() {
    // create options
    let options = service_options(Url::parse("http://localhost:1234").unwrap(), 18001).unwrap();

    // connect to AMQP and create channel
    let connection = amqp_connect(&options).await.unwrap();
    let channel = amqp_channel(&options, &connection).await.unwrap();
    let (responder_send, responder_recv) = oneshot::channel();
    let responder = tokio::spawn(send_response(options.clone(), channel, responder_send));

    // start service and wait for to be reachable
    let options_clone = options.clone();
    let server = tokio::spawn(async move {
        options_clone.run().await.unwrap();
    });
    service_await(&options).await;
    responder_recv.await.unwrap();

    // request to send
    let request = Request {
        account: Uuid::new_v4(),
        device: Uuid::new_v4(),
        service: "hive-backend".to_string(),
        request: None,
        r#type: "test-request".to_string(),
        other: {
            let mut map = BTreeMap::default();
            map.insert("a".into(), 15.into());
            map.insert("b".into(), 3.into());
            map
        },
    };

    // send event via API
    let client = Client::new();
    let response = client
        .post(&format!("http://{}/api/v1/request", options.listen))
        .header("Authorization", &format!("Bearer {}", Uuid::new_v4()))
        .json(&request)
        .send()
        .await
        .unwrap();
    assert!(response.status().is_success());
    let response: Response = response.json().await.unwrap();
    assert_eq!(response.account, request.account);
    assert_eq!(response.device, request.device);
    assert_eq!(response.service, "hive-device");
    let sum = response.other.get("sum").unwrap().as_f64().unwrap() as i64;
    assert_eq!(sum, 18);

    // stop server
    server.abort();
    let _ = server.await;
    let _ = responder.await;
}

#[ignore]
#[tokio::test]
async fn send_request_timeout() {
    // create options with low timeout. this test will send a request but
    // we don't launch the task that responds to it. therefore, we will
    // hit this timeout.
    let mut options = service_options(Url::parse("http://localhost:1234").unwrap(), 18002).unwrap();
    options.timeout = Duration::from_secs(3);

    // connect to AMQP and create channel
    let connection = amqp_connect(&options).await.unwrap();
    let _channel = amqp_channel(&options, &connection).await.unwrap();

    // start service and wait for to be reachable
    let options_clone = options.clone();
    let server = tokio::spawn(async move {
        options_clone.run().await.unwrap();
    });
    service_await(&options).await;

    // request to send
    let request = Request {
        account: Uuid::new_v4(),
        device: Uuid::new_v4(),
        service: "hive-backend".to_string(),
        request: None,
        r#type: "test-request".to_string(),
        other: BTreeMap::new(),
    };

    // send event via API
    let client = Client::new();
    let response = client
        .post(&format!("http://{}/api/v1/request", options.listen))
        .header("Authorization", &format!("Bearer {}", Uuid::new_v4()))
        .json(&request)
        .send()
        .await
        .unwrap();
    assert!(!response.status().is_success());

    // stop server
    server.abort();
    let _ = server.await;
}

async fn callback_server(token: &str) -> Result<(Url, broadcast::Receiver<Event>)> {
    let expected_authorization = format!("Bearer {token}");
    let (sender, receiver) = broadcast::channel(10);
    let checker = warp::post()
        .and(warp::header("Authorization"))
        .and(warp::body::json())
        .map(move |authorization: String, event: Event| {
            if authorization == expected_authorization {
                sender.send(event).unwrap();
                Box::new(StatusCode::OK) as Box<dyn warp::Reply>
            } else {
                Box::new(StatusCode::UNAUTHORIZED) as Box<dyn warp::Reply>
            }
        });

    let (socket, future) = warp::serve(checker).bind_ephemeral(([127, 0, 0, 1], 0));
    tokio::spawn(future);
    let url = Url::parse(&format!("http://{}/", socket))?;
    Ok((url, receiver))
}

async fn callback_server_janky(token: &str) -> Result<(Url, broadcast::Receiver<Event>)> {
    let expected_authorization = format!("Bearer {token}");
    let (sender, receiver) = broadcast::channel(10);

    // should this request error out? set initially to true.
    let should_error = Arc::new(Mutex::new(true));

    let checker = warp::post()
        .and(warp::header("Authorization"))
        .and(warp::body::json())
        .map(move |authorization: String, event: Event| {
            if authorization == expected_authorization {
                // if should_error is true, we error out, and set it to false,
                // so that on the second request it completes successfully.
                let mut should_error_lock = should_error.lock().unwrap();
                if *should_error_lock {
                    *should_error_lock = false;
                    Box::new(StatusCode::INTERNAL_SERVER_ERROR) as Box<dyn warp::Reply>
                } else {
                    sender.send(event).unwrap();
                    Box::new(StatusCode::OK) as Box<dyn warp::Reply>
                }
            } else {
                Box::new(StatusCode::UNAUTHORIZED) as Box<dyn warp::Reply>
            }
        });

    let (socket, future) = warp::serve(checker).bind_ephemeral(([127, 0, 0, 1], 0));
    tokio::spawn(future);
    let url = Url::parse(&format!("http://{}/", socket))?;
    Ok((url, receiver))
}

#[tokio::test]
async fn test_callback_server() {
    let (url, mut receiver) = callback_server("token").await.unwrap();
    let client = Client::new();
    let sent_event = Event {
        account: Uuid::new_v4(),
        device: Uuid::new_v4(),
        service: "abc".to_string(),
        r#type: "test".to_string(),
        other: Default::default(),
    };
    let response = client
        .post(url)
        .header("Authorization", "Bearer token")
        .json(&sent_event)
        .send()
        .await
        .unwrap();
    assert!(response.status().is_success());
    let event = receiver.recv().await.unwrap();
    assert_eq!(event, sent_event);
}

#[tokio::test]
async fn test_callback_server_janky() {
    let (url, mut receiver) = callback_server_janky("token").await.unwrap();
    let client = Client::new();
    let sent_event = Event {
        account: Uuid::new_v4(),
        device: Uuid::new_v4(),
        service: "abc".to_string(),
        r#type: "test".to_string(),
        other: Default::default(),
    };

    // first request fails
    let response = client
        .post(url.clone())
        .header("Authorization", "Bearer token")
        .json(&sent_event)
        .send()
        .await
        .unwrap();
    assert_eq!(response.status(), StatusCode::INTERNAL_SERVER_ERROR);

    // second request works
    let response = client
        .post(url)
        .header("Authorization", "Bearer token")
        .json(&sent_event)
        .send()
        .await
        .unwrap();
    assert!(response.status().is_success());
    let event = receiver.recv().await.unwrap();
    assert_eq!(event, sent_event);
}

#[ignore]
#[tokio::test]
async fn receive_event() {
    // create options with low timeout. this test will send a request but
    // we don't launch the task that responds to it. therefore, we will
    // hit this timeout.
    let (callback_url, mut callbacks) = callback_server(JWT).await.unwrap();
    let options = service_options(callback_url, 18004).unwrap();

    // connect to AMQP and create channel
    let connection = amqp_connect(&options).await.unwrap();
    let channel = amqp_channel(&options, &connection).await.unwrap();

    // start service and wait for to be reachable
    let options_clone = options.clone();
    let server = tokio::spawn(async move {
        options_clone.run().await.unwrap();
    });
    service_await(&options).await;

    // event to send
    let event = Event {
        account: Uuid::new_v4(),
        device: Uuid::new_v4(),
        service: "hive-device".to_string(),
        r#type: "test-message".to_string(),
        other: Default::default(),
    };

    // send event to AMQP
    let event_value = serde_json::to_value(&event).unwrap();
    let event_value = match event_value.to_amqp_value().unwrap() {
        AMQPValue::FieldTable(table) => table,
        _ => unreachable!(),
    };
    let properties = BasicProperties::default().with_headers(event_value);
    let _result = channel
        .basic_publish(
            &options.amqp_exchange,
            "",
            Default::default(),
            &[],
            properties,
        )
        .await
        .unwrap();

    loop {
        let actual_event = callbacks.recv().await.unwrap();
        if actual_event == event {
            break;
        }
    }

    // stop server
    server.abort();
    let _ = server.await;
}

#[ignore]
#[tokio::test]
async fn receive_event_janky() {
    // here we explicitly use a janky callback server that will error out
    // on the first callback it receives. we do that to test the retry functionality.
    // this test will only succeed if the service will retry sending the event after
    // it hit a internal server error on the first attempt.
    let (callback_url, mut callbacks) = callback_server_janky(JWT).await.unwrap();
    let options = service_options(callback_url, 18008).unwrap();

    // connect to AMQP and create channel
    let connection = amqp_connect(&options).await.unwrap();
    let channel = amqp_channel(&options, &connection).await.unwrap();

    // start service and wait for to be reachable
    let options_clone = options.clone();
    let server = tokio::spawn(async move {
        options_clone.run().await.unwrap();
    });
    service_await(&options).await;

    // event to send
    let event = Event {
        account: Uuid::new_v4(),
        device: Uuid::new_v4(),
        service: "hive-device".to_string(),
        r#type: "test-message".to_string(),
        other: Default::default(),
    };

    // send event to AMQP
    let event_value = serde_json::to_value(&event).unwrap();
    let event_value = match event_value.to_amqp_value().unwrap() {
        AMQPValue::FieldTable(table) => table,
        _ => unreachable!(),
    };
    let properties = BasicProperties::default().with_headers(event_value);
    let _result = channel
        .basic_publish(
            &options.amqp_exchange,
            "",
            Default::default(),
            &[],
            properties,
        )
        .await
        .unwrap();

    loop {
        let actual_event = callbacks.recv().await.unwrap();
        if actual_event == event {
            break;
        }
    }

    // stop server
    server.abort();
    let _ = server.await;
}

#[ignore]
#[tokio::test]
async fn receive_websocket_event() {
    // create options with low timeout. this test will send a request but
    // we don't launch the task that responds to it. therefore, we will
    // hit this timeout.
    let (callback_url, mut callbacks) = callback_server(JWT).await.unwrap();
    let options = service_options(callback_url, 18005).unwrap();

    // connect to AMQP and create channel
    let connection = amqp_connect(&options).await.unwrap();
    let channel = amqp_channel(&options, &connection).await.unwrap();

    // start service and wait for to be reachable
    let options_clone = options.clone();
    let server = tokio::spawn(async move {
        options_clone.run().await.unwrap();
    });
    service_await(&options).await;

    // event to send
    let event = Event {
        account: Uuid::new_v4(),
        device: Uuid::new_v4(),
        service: "hive-websocket".to_string(),
        r#type: "device-alive".to_string(),
        other: Default::default(),
    };

    // send event to AMQP
    let event_value = serde_json::to_value(&event).unwrap();
    let event_value = match event_value.to_amqp_value().unwrap() {
        AMQPValue::FieldTable(table) => table,
        _ => unreachable!(),
    };
    let properties = BasicProperties::default().with_headers(event_value);
    let _result = channel
        .basic_publish(
            &options.amqp_exchange,
            "",
            Default::default(),
            &[],
            properties,
        )
        .await
        .unwrap();

    loop {
        let actual_event = callbacks.recv().await.unwrap();
        if actual_event == event {
            break;
        }
    }

    // stop server
    server.abort();
    let _ = server.await;
}
