# NextMR

# [Fixes callback crash when unsuccessful](https://gitlab.com/fractalnetworks/services/hive-device-service/-/merge_requests/1)

This adds a retry logic into the hive device service which will retry to
hit the callback URL for events if it fails. Currently, there is a small
timeout of 100ms between the retries in order to not spam the logs if
the callback URL is down momentarily. Also adds a unit test for this
behaviour.

