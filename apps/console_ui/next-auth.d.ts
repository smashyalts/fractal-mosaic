import NextAuth from 'next-auth'

declare module 'next-auth' {
	interface Session {
		uuid: string
		accessToken: string
	}
}

declare global {
	interface Window {
		CHATTERBOX_CONFIG_LOCATION: string
	}
}
