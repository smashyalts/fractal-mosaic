// Type
import {ActionsType} from 'types'

const appActions: ActionsType[] = [
	// {
	//   icn: ["fas", "globe"],
	//   title: "Customize Domain",
	//   func: () => {
	//     return;
	//   },
	// },
	// {
	//   icn: ["fas", "trash-can"],
	//   title: "Remove Domain",
	//   func: () => {
	//     return;
	//   },
	// },
	{
		icn: ['fas', 'trash-can'],
		title: 'Uninstall App',
		func: () => {
			return
		},
	},
	{
		icn: ['fas', 'desktop'],
		title: 'Reschedule App',
		func: () => {
			return
		},
	},
]

export default appActions
