// Type
import { ActionsType } from "types";

const memberActions: ActionsType[] = [
  {
    icn: ["fas", "trash-can"],
    title: "Remove Member",
    func: () => {
      return;
    },
  },
];

export default memberActions;
