import React from 'react'
import {RecoilRoot} from 'recoil'
import {SessionProvider} from 'next-auth/react'
import 'whatwg-fetch'

export const TestWrapper = ({children}: any) => {
	return (
		<SessionProvider>
			<RecoilRoot>{children}</RecoilRoot>
		</SessionProvider>
	)
}

export const SignedInWrapper = ({children}: any) => {
	const mockSession = {
		expires: '1',
		user: {email: 'a', name: 'Delta', image: 'c'},
		accessToken:
			'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c',
		uuid: 'deez',
	}
	return (
		<SessionProvider session={mockSession}>
			<RecoilRoot>{children}</RecoilRoot>
		</SessionProvider>
	)
}
