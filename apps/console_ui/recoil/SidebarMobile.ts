// Recoil
import { atom } from "recoil";

export const sidebarMobile = atom<boolean>({
  key: "sidebarMobile",
  default: false,
});