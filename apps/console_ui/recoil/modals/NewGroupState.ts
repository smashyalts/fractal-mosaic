// Recoil
import { atom } from "recoil";

export const newGroupState = atom<boolean>({
  key: "newGroupState",
  default: false,
});
