// Recoil
import { atom } from "recoil";
import { ModalProps } from 'types'

export const removeDomainState = atom<ModalProps>({
  key: "removeDomainState",
  default: {
    show: false,
    uuid: "",
  },
});
