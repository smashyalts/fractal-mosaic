// Recoil
import {atom} from 'recoil'
import {ModalProps} from 'types'

export const rescheduleApp = atom<ModalProps>({
	key: 'rescheduleApp',
	default: {
		show: false,
		uuid: '',
	},
})
