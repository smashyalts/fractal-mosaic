// Recoil
import { atom } from "recoil";

export const PersonalSettingsState = atom<boolean>({
  key: "personalSettings",
  default: false
});

export const GroupUUID = atom<string>({
	key: "groupUUID",
	default: ""
  });