// Recoil
import { atom } from "recoil";
import { ModalProps } from 'types'

export const editConfigState = atom<ModalProps>({
  key: "editConfigState",
  default: {
    show: false,
    uuid: "",
  },
});
