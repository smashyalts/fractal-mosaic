// React
import {useState} from 'react'

// Recoil
import {useRecoilState} from 'recoil'
import {StateGroupEdit} from '../../recoil/modals/StateGroupEdit'

// Bootstrap
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'

// Fractal Components
import LayoutModalHeader from 'components/Layout/Modal/LayoutModalHeader'
import LayoutModalFooter from 'components/Layout/Modal/LayoutModalFooter'
import Group from 'data/groups/Group'
import CardSm from 'components/Card/CardSm'
import TextInput from 'components/Input/TextInput'

// Fractal Hooks
import useModalToggle from 'hooks/useModalToggle'
import PictureChooser from 'components/PictureChooser/PictureChooser'

// Icons
import {IconName, IconPrefix} from '@fortawesome/free-solid-svg-icons'

const ModalGroupEdit = () => {
	// Group Name State
	const [groupName, setGroupName] = useState('')
	const handleGroupNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		setGroupName(e.target.value)
	}

	// Show / Hide Modal State
	const [state, setShow] = useRecoilState(StateGroupEdit)
	const {show, uuid} = state

	const toggleModal = useModalToggle(setShow)
	const [selectedIcon, setSelectedIcon] = useState<[IconPrefix, IconName]>()

	const resetState = () => {
		setGroupName('')
		setSelectedIcon(undefined)
	}

	return (
		<Modal show={show} onHide={toggleModal} centered enforceFocus={false} onExited={resetState}>
			<LayoutModalHeader>
				Edit &ensp;
				<Group uuid={`${uuid}`} children={<CardSm />} />
			</LayoutModalHeader>
			<Modal.Body className='rounded-0'>
				<Form>
					<Form.Group className='mb-3'>
						<Form.Label>Set a Group Name</Form.Label>
						<Group uuid={uuid} children={<TextInput onChange={(e: any) => handleGroupNameChange(e as any)} />} />
					</Form.Group>
				</Form>

				<Form.Label>Choose a Group Icon</Form.Label>

				<PictureChooser groupUuid={uuid} />
			</Modal.Body>
			<LayoutModalFooter>
				<Button onClick={toggleModal} variant='secondary'>
					Cancel
				</Button>
				<Button onClick={toggleModal} variant='primary'>
					Save
				</Button>
			</LayoutModalFooter>
		</Modal>
	)
}

export default ModalGroupEdit
