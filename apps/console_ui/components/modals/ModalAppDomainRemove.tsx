// React
import {useState} from 'react'

// Recoil
import {useRecoilState} from 'recoil'
import {removeDomainState} from '../../recoil/modals/RemoveDomainState'

// Bootstrap
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'

// Fractal Hooks
import useModalToggle from 'hooks/useModalToggle'

// Fractal Components
import LayoutModalHeader from 'components/Layout/Modal/LayoutModalHeader'
import LayoutModalFooter from 'components/Layout/Modal/LayoutModalFooter'
import App from 'data/apps/App'
import CardSm from 'components/Card/CardSm'

const ModalAppDomainRemove = () => {
	// Hides Modal
	const [state, setShow] = useRecoilState(removeDomainState)
	const {show, uuid} = state

	const toggleModal = useModalToggle(setShow)

	// Track value of input field
	const [input, setInput] = useState('')

	return (
		<Modal show={show} onHide={toggleModal} centered>
			<LayoutModalHeader>
				Remove &ensp;
				<span className='text-decoration-underline'>custom-domain.com</span>
				&ensp; from &ensp;
				<App uuid={uuid} children={<CardSm noCircle />} />
			</LayoutModalHeader>
			<LayoutModalFooter>
				<Button onClick={toggleModal} variant='secondary'>
					Cancel
				</Button>
				<Button onClick={toggleModal} variant='primary'>
					Save
				</Button>
			</LayoutModalFooter>
		</Modal>
	)
}

export default ModalAppDomainRemove
