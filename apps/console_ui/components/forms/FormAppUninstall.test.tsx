import React from 'react'
import {render, screen} from '@testing-library/react'
import '@testing-library/jest-dom'
import userEvent from '@testing-library/user-event'
import {SignedInWrapper} from '../../util/testing/testing-wrapper'
import FormAppUninstall from './FormAppUninstall'

// Mocking toggle modal function prop passed from parent modal
const mockToggleModal = jest.fn(() => console.log('toggle modal'))

// Mock client api
jest.mock('../../clientApi/clientApi', () => ({
	__esModule: true,
	default: () => ({
		uninstallApp: mockUninstallApp,
	}),
}))

const mockUninstallApp = jest.fn()

describe('Client side validation works for Uninstall App Form', () => {
	it('Error message states This input is required', async () => {
		const user = userEvent.setup()
		render(<FormAppUninstall title='Element' uuid='1234' toggleModal={mockToggleModal} />, {wrapper: SignedInWrapper})
		const button = screen.getByText('Uninstall App')

		await user.click(button)
		expect(screen.getByText('This input is required')).toBeInTheDocument()
		expect(mockToggleModal).not.toHaveBeenCalled()
	})
	it('Error message states Input did not match', async () => {
		const user = userEvent.setup()
		render(<FormAppUninstall title='Element' uuid='1234' toggleModal={mockToggleModal} />, {wrapper: SignedInWrapper})
		const input = screen.getByTestId('input field')
		const button = screen.getByText('Uninstall App')
		await user.type(input, '123')
		await user.click(button)
		expect(screen.getByText('Input did not match')).toBeInTheDocument()
		expect(mockToggleModal).not.toHaveBeenCalled()
	})
	it('Submits PATCH request if input matches pattern', async () => {
		const user = userEvent.setup()
		render(<FormAppUninstall title='Element' uuid='1234' toggleModal={mockToggleModal} />, {wrapper: SignedInWrapper})
		const input = screen.getByTestId('input field')
		const button = screen.getByText('Uninstall App')
		await user.type(input, 'Element')
		await user.click(button)
		expect(screen.queryByText('Input did not match')).not.toBeInTheDocument()
		expect(screen.queryByText('This input is required')).not.toBeInTheDocument()
		expect(mockToggleModal).toHaveBeenCalled()
		expect(mockUninstallApp).toHaveBeenCalled()
	})
})
