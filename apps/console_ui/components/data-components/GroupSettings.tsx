// React
import {useRouter} from 'next/router'

// Bootstrap
import Button from 'react-bootstrap/Button'

// Recoil
import {useSetRecoilState} from 'recoil'
import {StateGroupEdit} from '../../recoil/modals/StateGroupEdit'

// Actions
import useActions from 'hooks/useActions'
import {groupSettingsAction} from '../../util/actions/groupActions'

import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

const GroupSettings = () => {
	const router = useRouter()
	const uuid = router.query.uuid

	// Shows Modal
	const setGroupEditShow = useSetRecoilState(StateGroupEdit)

	//@ts-ignore
	const actions = useActions(uuid, groupSettingsAction, [setGroupEditShow])

	return (
		<>
			<Button
				className='d-none d-sm-flex flex-fill flex-grow-0 flex-shrink-0'
				variant='secondary'
				onClick={actions[0].func}>
				Group Settings
			</Button>

			<Button className='d-flex d-sm-none px-1' variant='link' onClick={actions[0].func}>
				<FontAwesomeIcon icon={['fas', 'gear']} fixedWidth />
			</Button>
		</>
	)
}

export default GroupSettings
