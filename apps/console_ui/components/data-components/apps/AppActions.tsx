// React
import React from 'react'

// Recoil
import {useSetRecoilState} from 'recoil'
import {customDomainState} from '../../../recoil/modals/CustomDomainState'
import {removeDomainState} from '../../../recoil/modals/RemoveDomainState'
import {uninstallApp} from '../../../recoil/modals/UninstallApp'
import {rescheduleApp} from '../../../recoil/modals/RescheduleAppState'

// Actions
import useActions from 'hooks/useActions'
import appActions from '../../../util/actions/appActions'

// Utils
import {DataProps} from 'types'

//takes child component and adds data to them as a prop
const AppActions = ({children, uuid, ...rest}: DataProps) => {
	// Actions we can take with Apps
	const setCustomizeDomainShow = useSetRecoilState(customDomainState)
	const setRemoveDomainShow = useSetRecoilState(removeDomainState)
	const setUninstallAppState = useSetRecoilState(uninstallApp)
	const setRescheduleAppState = useSetRecoilState(rescheduleApp)
	const actions = useActions(uuid, appActions, [setUninstallAppState, setRescheduleAppState])
	// const actions = useActions(uuid, appActions, [setCustomizeDomainShow, setRemoveDomainShow, setUninstallAppState])

	return React.cloneElement(children, {actions, uuid, ...rest})
}

export default AppActions
