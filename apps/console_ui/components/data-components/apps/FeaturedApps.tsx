import React, {useState, useEffect, useRef, Dispatch, SetStateAction} from 'react'

// SWR
import useSWR from 'swr'

import {FEATURED_APPS_ENDPOINT} from 'util/endpoints'

import CardFeatureApp from 'components/Card/CardFeatureApp'

// Icons
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faX} from '@fortawesome/free-solid-svg-icons'

import css from '../../../styles/featuredapps.module.css'

interface IAppData {
	uuid: string
	image: string
	github_url: string
	content: string
	subtitle: string
	description: string
	name: string
}

interface IFeaturedAppsProps {
	setShow: Dispatch<SetStateAction<boolean>>
}

const FeaturedApps = ({setShow}: IFeaturedAppsProps) => {
	const featuredAppsContainerRef = useRef<HTMLDivElement>(null)
	const lastAppRef = useRef<HTMLDivElement>(null)
	const fetcher = (arg: string) => fetch(arg).then((res) => res.json())

	const {data, error} = useSWR(FEATURED_APPS_ENDPOINT, fetcher)

	useEffect(() => {
		if (featuredAppsContainerRef) {
			setTimeout(() => {
				featuredAppsContainerRef.current?.scroll({top: undefined, left: 0, behavior: 'smooth'})
			}, 1500)
		}
	}, [])

	if (data && data.length > 0) {
		return (
			<div className='p-3'>
				<h1 className={`${css.mobile_title}`}>Featured Services</h1>
				<div className='d-flex justify-content-between align-items-center position-relative'>
					<FontAwesomeIcon onClick={() => setShow(false)} className={css.close} icon={faX} size={'lg'} />
				</div>
				<div>
					<div ref={featuredAppsContainerRef} className={`py-3 ${css.featuredServices_container}`}>
						<h1 className={`text-center m-0 ps-5 ${css.title}`}>Featured Services</h1>

						{data.map((item: IAppData, idx: number, array: any[]) => {
							if (idx == array.length - 1) {
								return (
									<CardFeatureApp
										ref={lastAppRef}
										uuid={item.uuid}
										image={item.image}
										name={item.name}
										github_url={item.github_url}
										subtitle={item.subtitle}
										description={item.description}
										content={item.content}
										key={item.uuid}
									/>
								)
							}
							return (
								<CardFeatureApp
									uuid={item.uuid}
									image={item.image}
									name={item.name}
									github_url={item.github_url}
									subtitle={item.subtitle}
									description={item.description}
									content={item.content}
									key={item.uuid}
								/>
							)
						})}
					</div>
				</div>
			</div>
		)
	} else {
		// TODO - ADD LOADING STATE
		return null
	}
}

export default FeaturedApps
