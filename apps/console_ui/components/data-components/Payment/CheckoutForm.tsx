import React, {useEffect, useState} from 'react'
import {PaymentElement, useStripe, useElements} from '@stripe/react-stripe-js'

// Bootstrap
import Button from 'react-bootstrap/Button'
import Toast from 'react-bootstrap/Toast'
import ToastContainer from 'react-bootstrap/ToastContainer'

const CheckoutForm = () => {
	const stripe = useStripe()
	const elements = useElements()

	const [message, setMessage] = useState<null | string>(null)
	const [isLoading, setIsLoading] = useState(false)
	const [payNowBtn, setPayNowBtn] = useState(false)

	useEffect(() => {
		if (!stripe) {
			return
		}

		const clientSecret = new URLSearchParams(window.location.search).get('payment_intent_client_secret')

		if (!clientSecret) {
			return
		}

		stripe.retrievePaymentIntent(clientSecret).then(({paymentIntent}) => {
			switch (paymentIntent?.status) {
				case 'succeeded':
					setMessage('Payment succeeded!')
					break
				case 'processing':
					setMessage('Your payment is processing.')
					break
				case 'requires_payment_method':
					setMessage('Your payment was not successful, please try again.')
					break
				default:
					setMessage('Something went wrong.')
					break
			}
		})
	}, [stripe])

	const handleSubmit = async (e: any) => {
		e.preventDefault()

		if (!stripe || !elements) {
			// Stripe.js has not yet loaded.
			// Make sure to disable form submission until Stripe.js has loaded.
			return
		}

		setIsLoading(true)

		const result = await stripe.confirmPayment({
			elements,
			confirmParams: {
				return_url: `${process.env.NEXT_PUBLIC_CONSOLE_BASE_URL}`,
			},
			redirect: 'if_required',
		})
		setPayNowBtn(true)

		toggleToast()

		// Handle Error state from stripe docs
		if (result.error) {
			if (result.error?.type === 'card_error' || result.error?.type === 'validation_error') {
				setMessage(result.error?.message as string)
			} else {
				setMessage('An unexpected error occurred.')
			}
			// Success state
		} else {
			setMessage('Payment succeeded!')
		}

		setIsLoading(false)
	}

	// Toast message state
	const [toast, setToast] = useState(false)
	const toggleToast = () => setToast(!toast)

	return (
		<>
			<form id='payment-form' onSubmit={handleSubmit}>
				<PaymentElement id='payment-element' />
				<Button className='mt-2' disabled={isLoading || !stripe || !elements || payNowBtn} id='submit' type='submit'>
					<span id='button-text'>Pay Now</span>
				</Button>
			</form>
			{/* Toast for response message to user */}
			<ToastContainer className='p-5' position='bottom-start'>
				<Toast show={toast} onClose={toggleToast} delay={6000} autohide>
					<Toast.Header>
						<strong className='me-auto'>Fractal Networks</strong>
					</Toast.Header>
					<Toast.Body className={message === 'Payment succeeded!' ? 'text-success' : 'text-danger'}>
						{message}
					</Toast.Body>
				</Toast>
			</ToastContainer>
		</>
	)
}

export default CheckoutForm
