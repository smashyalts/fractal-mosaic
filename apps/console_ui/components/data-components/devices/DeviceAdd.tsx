//Bootstrap
import Button from 'react-bootstrap/Button'

// Recoil
import {useSetRecoilState} from 'recoil'
import {deviceInstallState} from '../../../recoil/modals/DeviceInstallState'

const DeviceAdd = ({uuid}: {uuid?: string}) => {
	const setDeviceInstallShow = useSetRecoilState(deviceInstallState)

	const handleDeviceShow = (): void => {
		if (uuid) {
			setDeviceInstallShow((currState) => ({
				uuid,
				show: !currState.show,
			}))
		} else console.error('no UUID provided to', this)
	}

	return (
		<Button onClick={() => handleDeviceShow()} size='sm' className='w-100'>
			Add Device
		</Button>
	)
}

export default DeviceAdd
