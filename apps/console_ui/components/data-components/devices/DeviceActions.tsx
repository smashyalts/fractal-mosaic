import React from 'react'

// Recoil
import {useSetRecoilState} from 'recoil'
import {deleteDeviceState} from '../../../recoil/modals/DeleteDeviceState'
import {editConfigState} from '../../../recoil/modals/EditConfigState'

// Actions
import useActions from 'hooks/useActions'
import deviceActions from '../../../util/actions/deviceActions'

import {DataProps} from 'types'

const DeviceActions = ({children, uuid, ...rest}: DataProps) => {
	const setDeleteDeviceShow = useSetRecoilState(deleteDeviceState)
	const setEditConfigShow = useSetRecoilState(editConfigState)
	const actions = useActions(uuid, deviceActions, [setEditConfigShow, setDeleteDeviceShow])

	return React.cloneElement(children, {actions, ...rest})
}

export default DeviceActions
