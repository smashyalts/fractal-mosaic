// React
import React from 'react'

// Recoil
import {useRecoilState} from 'recoil'
import {newGroupState} from '../../../recoil/modals/NewGroupState'

import {DataProps, IcnType, TitleType} from 'types'

const GroupAdd = ({children}: DataProps) => {
	const title: TitleType = 'New Group'
	const icn: IcnType = ['fas', 'plus']

	// Show / Hide Modal State
	const [show, setShow] = useRecoilState(newGroupState)
	const showNewGroupModal = () => {
		setShow(true)
	}

	// passing function to children
	const action = () => {
		showNewGroupModal()
	}

	return React.cloneElement(children, {title, icn, action})
}

export default GroupAdd
