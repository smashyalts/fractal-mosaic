// React
import React from 'react'

// Recoil
import {useSetRecoilState} from 'recoil'
import {removeMember} from '../../../recoil/modals/RemoveMember'

// Actions
import useActions from 'hooks/useActions'
import memberActions from '../../../util/actions/memberActions'

import {DataProps} from 'types'

const MemberActions = ({children, uuid, ...rest}: DataProps) => {
	// Actions we can take with Members
	const setRemoveMemberShow = useSetRecoilState(removeMember)
	const actions = useActions(uuid, memberActions, [setRemoveMemberShow])

	return React.cloneElement(children, {actions, ...rest})
}

export default MemberActions
