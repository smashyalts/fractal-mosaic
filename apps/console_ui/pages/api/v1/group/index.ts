import type {NextApiRequest, NextApiResponse} from 'next'

const handler = (req: NextApiRequest, res: NextApiResponse) => {
	res.status(200).json([
		'f93c9d21-8a5f-44a5-9adc-217b0be3aae2',
		// to test errors, uncomment this (will show an invalid member)
		// "f93c9d21-8a5f-44a5-9adc-217b0be3aae4",
		'987ffb95-bbbe-4e92-acb4-7cb3223efdbd',
	])
}

export default handler
