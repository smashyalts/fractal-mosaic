import { Html, Head, Main, NextScript } from 'next/document'

export default function Document() {
	return (
		<Html>
			<Head>
				<title>Fractal Networks | Mosaic</title>
				<meta name="description" content="Self host services together" />
				<meta name="theme-color" content="#ef0005" />
				<link rel="manifest" href="/manifest.webmanifest" />
				<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png" />
				<link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png" />
				<link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png" />
				<link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#ef0005" />
				<link rel="shortcut icon" href="/favicon.ico" />
				<meta name="apple-mobile-web-app-status-bar" content="#ef0005" />
				<meta name="msapplication-TileColor" content="#ef0005" />
				<meta name="msapplication-TileImage" content="/favicon/mstile-144x144.png" />
				<meta name="msapplication-config" content="/favicon/browserconfig.xml" />
				<link rel="icon" href="/favicon.ico" />
			</Head>
			<body>
				<Main />
				<NextScript />
			</body>
		</Html>
	)
}