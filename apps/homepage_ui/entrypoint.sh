#!/bin/sh
npm install
cd apps/homepage_ui
if [ "$NODE_ENV" == "dev" ]; then
	npm run dev
else
 	npm run build
 	npm run start
fi
