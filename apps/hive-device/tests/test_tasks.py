from unittest.mock import patch
from src import tasks
import json

async def test_start_app_unencrypted(run_app_message_decrypted, test_key):
    '''
    Ensures that an app on successful start returns running message or
    None if the app doesn't exist.
    '''
    msg = run_app_message_decrypted
    key = test_key

    original_keys = msg['storage_apikeys']

    with patch('src.tasks.compose._run_command', return_value=(0, (b'Mock Response', b'Mock Response'))) as runcmd:
        response = await tasks.start_app(msg)
        runcmd.assert_awaited()

        instance = response['instance']
        state = response['state']
        msg_type = response['type']

        assert instance ==  msg['instance']
        assert state ==  "running"
        assert msg_type ==  "instance-state"

        # storage apikeys in response should be encrypted, therefore
        # should not match the original message
        encrypted_storage_keys = response['encrypted_storage_apikeys']
        assert encrypted_storage_keys != original_keys

        # decrypt storage apikeys and confirm that they are the same
        decrypted_keys = json.loads(key.decrypt(encrypted_storage_keys))
        assert original_keys == decrypted_keys

    msg['application']['name'] = 'test'
    response = await tasks.start_app(msg)
    assert response == None

async def test_start_app_encrypted(run_app_message_encrypted, test_key):
    '''
    Ensures that an app on successful start returns running message or
    None if the app doesn't exist.
    '''
    msg = run_app_message_encrypted
    key = test_key

    # using hardcoded keys from message to ensure that decryption is working correctly
    keys_from_message = msg['storage_apikeys']

    with patch('src.tasks.compose._run_command', return_value=(0, (b'Mock Response', b'Mock Response'))) as runcmd:
        response = await tasks.start_app(msg)
        runcmd.assert_awaited()

        instance = response['instance']
        state = response['state']
        msg_type = response['type']
        encrypted_keys = response['encrypted_storage_apikeys']

        assert instance ==  msg['instance']
        assert state ==  "running"
        assert msg_type ==  "instance-state"

        # start app decrypts then reencrypts the keys. They shouldn't be the exact same
        assert encrypted_keys != msg['encrypted_storage_apikeys']

        # decrypt storage apikeys from response and confirm they are the same
        keys_from_response = json.loads(key.decrypt(encrypted_keys))
        assert keys_from_message == keys_from_response

async def test_start_app_not_found(run_app_message_decrypted):
    '''
    Ensures that when trying to run an app that doesn't exist, None is returned
    '''
    msg = run_app_message_decrypted
    msg['application']['name'] = 'test'
    response = await tasks.start_app(msg)
    assert response == None

async def test_stop_app_not_found(generate_payload):
    '''
    Ensures that when trying to stop an app that doesn't exist,
    a stopped message is returned.
    '''
    msg = generate_payload('SchedulerAction.stop')
    msg['application']['name'] = 'test'

    response = await tasks.stop_app(msg)
    assert response['instance'] == msg['instance']
    assert response['state'] == "stopped"
    assert response['type'] == "instance-state"


async def test_stop_app(generate_payload):
    '''
    Ensures that an app on successful stop or when app doesn't exist
    returns a stopped message.
    '''
    msg = generate_payload('SchedulerAction.stop')

    with patch('src.tasks.compose._run_command', return_value=(0, (b'Mock Response', b'Mock Response'))) as runcmd:
        response = await tasks.stop_app(msg)
        runcmd.assert_awaited()

        instance = response['instance']
        state = response['state']
        msg_type = response['type']
        legacy_storage_keys = response['storage_apikeys']
        device_encrypted_keys = response['encrypted_storage_apikeys']

        assert instance ==  msg['instance']
        assert state ==  "stopped"
        assert msg_type ==  "instance-state"
        assert legacy_storage_keys == None
        assert device_encrypted_keys


async def test_run_app_instance_command(generate_payload):
    '''
    Ensures that spawn_background_thread is called twice for creating
    an admin account for the Element app.
    '''
    msg = {
        "container": "synapse",
        "action": "create_admin_user",
        "args": {
            "username": "test_user",
            "password": "test_password"
        }
    }
    payload = generate_payload('AppInstanceCommand', **msg)

    with patch('src.utils.spawn_background_thread', return_value=0) as mock_thread:
        await tasks.run_app_instance_command(payload)
        mock_thread.assert_awaited()
        assert mock_thread.call_count == 2
