from src.crypto import PassphraseEncryptionXSalsa20
from nacl import pwhash, secret, encoding
import os
import json


def test_encrypt(test_key, test_storage_keys):
    '''
    Ensure that keys can be encrypted.
    '''
    encryption_key = test_key
    encrypted_storage_keys = encryption_key.encrypt(
        json.dumps(test_storage_keys))
    assert test_storage_keys != encrypted_storage_keys


def test_decrypt(test_key, test_storage_keys):
    '''
    Ensure that keys can be decrypted.
    '''
    encryption_key = test_key
    encrypted_storage_keys = encryption_key.encrypt(
        json.dumps(test_storage_keys))
    assert test_storage_keys != encrypted_storage_keys

    decrypted_keys = json.loads(encryption_key.decrypt(encrypted_storage_keys))
    assert decrypted_keys == test_storage_keys


def test_derive_key(device_secret):
    '''
    Ensure that derive_key correctly derives password into key.
    '''
    passphrase = device_secret
    secret_message = 'attack at dawn!'

    # __init__ will call derive_key
    encryption_key = PassphraseEncryptionXSalsa20(passphrase)

    # encrypt message with passphrase
    encrypted_storage_keys = encryption_key.encrypt(secret_message)

    # derive key from passphrase
    kdf = pwhash.argon2i.kdf
    salt = b'fractalnetworks!'
    password = os.environ.get("DEVICE_SECRET").encode('utf-8')
    ops = pwhash.argon2i.OPSLIMIT_MODERATE
    mem = pwhash.argon2i.MEMLIMIT_MODERATE
    key = kdf(secret.SecretBox.KEY_SIZE, password,
              salt, opslimit=ops, memlimit=mem)
    passphrase_encryption_key = secret.SecretBox(key)

    # ensure that decrypting the message works with freshly derived key
    decrypted_keys = passphrase_encryption_key.decrypt(
        encrypted_storage_keys, encoder=encoding.Base64Encoder).decode()
    assert decrypted_keys == secret_message
