import asyncio
import copy
import json
import os
import logging
import sys
from shlex import quote

import docker
from aiopath import AsyncPath
from docker.errors import APIError

from src import utils, compose, crypto, __version__
from src.commands.device import DeviceCommands

APPS_PATH = utils.APPS_PATH
DEVICE_SECRET = os.environ.get("DEVICE_SECRET")
DEVICE_IMAGE = os.environ.get("DEVICE_IMAGE", "fractalnetworks/hive-device:alpha")
AUTO_UPDATES = utils.str_to_bool(os.environ.get("AUTO_UPDATES", True))
UPDATE_INTERVAL = int(os.environ.get("UPDATE_INTERVAL", 3600))

cache = {}
encryption_key = crypto.PassphraseEncryptionXSalsa20(DEVICE_SECRET)

async def send_websocket_message(websocket, message_to_send: str):
    '''
    Sends a message into the provided websocket
    '''
    message_to_send = json.dumps(message_to_send, default=str)
    await websocket.send(message_to_send)
    logging.info(f"Sent message: {message_to_send}")

async def run_loop(client: docker.DockerClient, websocket):
    '''
    Handles various events that are received from the backend

    Params:
    - websocket: Websocket connection to use
    '''
    while True:
        recv_msg = await websocket.recv()
        recv_msg = json.loads(recv_msg)

        # ignore any messages sent by the device
        service = recv_msg["service"]
        if service == "hive-device":
            continue

        # immediately ack back message if it's a device-request message type
        # and command is not a stop command
        msg_type = recv_msg["type"]
        msg_command = recv_msg.get("command")
        if msg_type == "device-request" and msg_command != "SchedulerAction.stop":
            # copy message to avoid overwriting recv_msg
            ack_msg = recv_msg
            await send_websocket_message(websocket, ack_msg)

        # handle received message in async task
        asyncio.create_task(handle_message(recv_msg, client, websocket))

async def handle_message(message: dict, client, websocket):
    '''
    Handles messages that are received from the websocket
    '''
    msg_type = message.get("type")
    service = message.get("service")
    command = message.get("command")

    if service == "hive-backend" and msg_type == "device-request":
        # determine what action the backend wants us to take
        command = message.get("command")
        reply = None

        if command == "UpgradeDevice":
            # TODO: Handle manual updates
            if AUTO_UPDATES:
                if 'update' not in cache.keys():
                    cache['update'] = True
                    await upgrade_device(client)
                    del cache['update']
                else:
                    logging.info("Received an UpgradeDevice command but already upgrading. Ignoring.")
                return
            logging.warning("WARNING: Auto Updates are disabled!")

        elif command == "DeviceCommand":
            if "device_command" not in cache.keys():
                cache["device_command"] = True
                reply = await run_device_command(message["payload"])
                del cache["device_command"]

                if reply:
                    await send_websocket_message(websocket, reply)
            else:
                logging.info("DEBUG: Received a device command but already running one.")
            return

        # only run command if app instance uuid is not in cache
        app_instance_uuid = message["payload"]["instance"]
        if app_instance_uuid in cache:
            logging.debug(f"{app_instance_uuid} is already in the cache.")
            return

        if command == "SchedulerAction.start":
            # Add app instance to cache with current command
            cache[app_instance_uuid] = command
            reply = await start_app(message["payload"])
            del cache[app_instance_uuid]

        elif command == "SchedulerAction.stop":
            cache[app_instance_uuid] = command
            reply = await stop_app(message["payload"])
            del cache[app_instance_uuid]

        elif command == "AppInstanceCommand":
            container = message["payload"]["container"]
            # run photoprism command in another thread
            if(container is not None and container == "photoprism"):
               await run_photoprism_instance_command(message["payload"]) 
            else:
                # run element command in another thread
                await run_app_instance_command(message["payload"])

        else:
            logging.error(f"Unsupported command: {command}")
        # TODO: Handle standby readonly volumes.

        if reply == None:
            return

        # if app successfully running, send instances message.
        # Doing so allows the hive backend to update the health of the app faster
        if reply.get("state") == "running":
            logging.info(f"App ({app_instance_uuid}) successfully running.")
            await send_instances(client, websocket, interval=0)
            await send_websocket_message(websocket, reply)

        # ack back stop message if stop was successful
        elif reply.get("state") == "stopped":
            logging.info(f"App ({app_instance_uuid}) successfully stopped.")
            await send_websocket_message(websocket, message)
            await send_websocket_message(websocket, reply)

        # send reply message to the backend
        # await send_websocket_message(websocket, reply)

async def run_app_instance_command(message: dict):
    '''
    Runs a given message's command on an app instance. Runs the given command
    in a separate thread.

    Params:
    - message(dict): payload that contains a command to run in a instance's container.
    '''
    commands = {
        "create_admin_user": "bash -c 'register_new_matrix_user -c /data/homeserver.yaml -u %s -p %s -a http://localhost:8008'",
        "create_user": "bash -c 'register_new_matrix_user -c /data/homeserver.yaml -u %s -p %s --no-admin http://localhost:8008'",
        "configure_element_web": """ash -c 'sed -i s/matrix-client.matrix.org/%s/g /app/config.json; sed -i s/\\"server_name\\":\ \\"matrix.org\\"/\\"server_name\\":\ \\"%s\\"/g /app/config.json'"""
    }
    app_instance = message.get("instance")
    container_name = message.get("container")
    container = f'{app_instance}-{container_name}-1'
    element_container = f'{app_instance}-element-1'
    action = message.get("action")
    # escape user provided data
    args = tuple(quote(arg) for arg in message.get("args"))
    command = commands[action] % tuple(args)
    link_domain = quote(message.get('link_domain'))
    config_element_command = commands['configure_element_web'] % (link_domain, link_domain)

    await utils.spawn_background_thread(compose.exec_command, container, command)
    await utils.spawn_background_thread(compose.exec_command, element_container, config_element_command)

async def run_photoprism_instance_command(message: dict):
    '''
    Runs photoprisms command on an app instance. Runs the given command
    in a separate thread.

    Params:
    - message(dict): payload that contains a command to run in a instance's container.
    '''
    password = message['args'][0]
    username = message["args"][1]
    commands = {
        "create_admin_password": "bash -c 'photoprism users add -p {0} -s -w {1}'".format(password, username),  
    }
    app_instance = message.get("instance")
    container_name = message.get("container")
    container = f'{app_instance}-{container_name}-1'
    action = message.get("action")
    command = commands[action]

    await utils.spawn_background_thread(compose.exec_command, container, command)

async def run_device_command(message: dict):
    '''
    Runs a given message's command on the device. Runs the given command
    in a separate thread.

    Params:
    - message(dict): Payload of device command.

    NOTE: currently does not support passing args.
    '''
    action = message.get("action")

    # ensure the given action is a supported device command
    try:
        command = DeviceCommands[action].value
    except KeyError:
        logging.warning(f"Got unsupported device command: {action}")
        return

    # TODO: Figure out if command needs args & handle giving args to command

    # run command and return its response
    response = await command.run()
    return response


async def send_instances(client: docker.DockerClient, websocket, interval=15):
    '''
    Task that queries docker for all currently running apps,
    collects them into a list, then sends an event to the backend
    that contains this list.

    Params:
    - client: Docker client to use
    - websocket: Websocket connection to use
    - interval: If 0 passed, only runs once. Otherwise will run on interval (seconds).
    '''
    while True:
        instances = await utils.get_instances(client)
        instances_msg = {
            "type": "instances",
            "instances": instances,
        }
        await send_websocket_message(websocket, instances_msg)

        if interval == 0:
            break
        else:
            await asyncio.sleep(interval)

async def docker_events(client: docker.DockerClient, websocket):
    '''
    Task that forwards all docker events to the backend.

    Params:
    - client: Docker client to use
    - websocket: Websocket connection to use

     NOTE: this is blocking and needs to be in its own thread
    '''
    docker_events = client.events()
    for event in docker_events:
        event = json.loads(event)
        # get the instance uuid the container is associated with if one exists
        instance = event.get("Actor").get("Attributes").get("name")
        instance = instance[:36] if instance else None

        # ignore certain health events as these messages are the execution and finishing
        # of health checks for a container. These bloat log messages.
        # For health checks, we only care about "health_status".
        event_status = event.get("status")
        ignored_health_events = set({'exec_start', 'exec_die', 'exec_create'})
        if event_status and any(ignored in event_status for ignored in ignored_health_events):
            continue

        # send an unhealthy instance-state message if the
        # container's status is an unhealthy docker status
        elif event_status == "health_status: unhealthy":
            unhealthy_docker_msg = {
                "instance": instance,
                "state": "unhealthy",
                "type": "instance-state"
            }
            logging.error(f"Got unhealthy docker status:\n{unhealthy_docker_msg}")
            await send_websocket_message(websocket, unhealthy_docker_msg)
            continue

        # NOTE: For now not sending docker events to Hive Backend
        # send the event into the websocket
        # docker_event_msg = {
        #     "instance": instance,
        #     "event": event,
        #     "type": "docker"
        # }
        # await send_websocket_message(websocket, docker_event_msg)

async def start_app(message: dict):
    '''
    Task that handles starting an app using the docker-compose CLI.

    Params:
    - app (dict): Dictionary of information about the app to start

    TODO: Handle being given an app store.
    '''
    # capture configuration we need for starting an app
    app = copy.deepcopy(message)
    app_instance_uuid = app["instance"]
    app_name = app["application"]["name"]
    app_version = app["application"]["version"]
    env_file_path = f'{APPS_PATH}/{app_instance_uuid}.env'
    app_file_path = f'{APPS_PATH}/{app_name}_{app_version}.yaml'
    legacy_storage_apikeys = app.get("storage_apikeys")
    encrypted_storage_apikeys = app.get("encrypted_storage_apikeys")
    public_key = app.get("storage_pubkey")

    logging.info(f"Starting app: {app_name} ({app_instance_uuid})")

    # check if app exists
    path = AsyncPath(app_file_path)
    if not await path.exists():
        logging.warning(f"App does not exist: {app_file_path}")
        return

    # get storage keys and update the app's storage keys property
    storage_apikeys, public_key = utils.get_storage_keys(encryption_key, legacy_storage_apikeys, encrypted_storage_apikeys)
    app['storage_apikeys'] = storage_apikeys

    # make sure environment file is generated for app
    await utils.generate_env(app)

    # create writeable volumes
    for name, private_key in storage_apikeys.items():
        storage_volume_name = f'{app_instance_uuid}_{name.upper()}'
        vol_create_response = await compose.volume_create(
            volume_name=storage_volume_name,
            volume_mode="write",
            privkey=private_key)
        vol_return_code = vol_create_response[0]
        _, stderr = vol_create_response[1]
        logging.debug(f"Compose volume_create return code: {vol_return_code}")

        if vol_return_code != 0:
            logging.error(f"Error creating writeable volume. Return code: {vol_return_code}, Error: {stderr}")
            sys.exit(1)

    # use subprocess to call docker-compose CLI
    compose_up_response = await compose.up(
        compose_file_path=app_file_path,
        env_file_path=env_file_path,
        project_name=app_instance_uuid)
    stdout, stderr = compose_up_response[1]
    return_code = compose_up_response[0]
    logging.debug(f"Compose up return code: {return_code}")

    # send running event into websocket
    if return_code == 0:
        # encrypt storage privatekeys with password
        encrypted_keys = encryption_key.encrypt(json.dumps(storage_apikeys))

        docker_event_msg = {
            "instance": app_instance_uuid,
            "state": "running",
            "type": "instance-state",
            "storage_apikeys": {},
            "encrypted_storage_apikeys": encrypted_keys,
            "storage_pubkey": public_key,
        }
        return docker_event_msg
    else:
        docker_event_msg = {
            "instance": app_instance_uuid,
            "type": "error",
            "return_code": return_code,
            "stderr": stderr
        }
        logging.error(f"Compose up call returned exit code: {return_code}. Error: {docker_event_msg['stderr']}")
        return docker_event_msg

async def stop_app(message: dict):
    '''
    Task that handles stopping an app using the docker-compose CLI.

    Params:
    - client: Docker client to use
    - websocket: Websocket connection to use
    - app (dict): Dictionary of information about the app to stop

    TODO: Handle being given an app store.
    '''

    # capture configuration we need for stopping an app
    app = copy.deepcopy(message)
    app_instance_uuid = app["instance"]
    app_name = app["application"]["name"]
    app_version = app["application"]["version"]
    env_file_path = f'{APPS_PATH}/{app_instance_uuid}.env'
    app_file_path = f'{APPS_PATH}/{app_name}_{app_version}.yaml'
    legacy_storage_apikeys = app.get("storage_apikeys")
    encrypted_storage_apikeys = app.get("encrypted_storage_apikeys")
    public_key = app.get("storage_pubkey")

    logging.info(f"Stopping app: {app_name} ({app_instance_uuid})")

    # check if app exists
    path = AsyncPath(app_file_path)
    if not await path.exists():
        logging.warning(f"App does not exist: {app_file_path}")
        # ack back stopped event even if app doesn't exist
        docker_event_msg = {
            "instance": app_instance_uuid,
            "state": "stopped",
            "type": "instance-state",
        }
        return docker_event_msg

    storage_apikeys, public_key = utils.get_storage_keys(encryption_key, legacy_storage_apikeys, encrypted_storage_apikeys)
    app['storage_apikeys'] = storage_apikeys

    # make sure environment file is generated for app
    await utils.generate_env(app)

    # use subprocess to call docker-compose CLI
    compose_down_response = await compose.down(
        compose_file_path=app_file_path,
        env_file_path=env_file_path,
        project_name=app_instance_uuid)
    return_code = compose_down_response[0]
    logging.debug(f"Compose down return code: {return_code}")

    # delete writeable volume
    for name in storage_apikeys.keys():
        storage_volume_name = f'{app_instance_uuid}_{name.upper()}'
        logging.debug(f'removing volume: {storage_volume_name}')
        vol_delete_response = await compose.volume_delete(storage_volume_name)
        vol_return_code = vol_delete_response[0]
        logging.debug(f"Compose volume_delete return code: {vol_return_code}")
        _, vol_delete_stderr = vol_delete_response[1]

        if vol_return_code != 0:
            logging.error(f"Error {vol_return_code} while deleting writeable volume: {vol_delete_stderr}")

    # send stopped event into websocket
    if return_code == 0:
        encrypted_keys = encryption_key.encrypt(json.dumps(storage_apikeys))
        docker_event_msg = {
            "instance": app_instance_uuid,
            "state": "stopped",
            "type": "instance-state",
            "storage_apikeys": None, # make sure legacy storage keys is unset
            "encrypted_storage_apikeys": encrypted_keys,
            "storage_pubkey": public_key,
        }
        return docker_event_msg
    else:
        logging.error(f"Compose down call returned exit code: {return_code}")
        docker_event_msg = {
            "instance": app_instance_uuid,
            "type": "error",
            "return_code": return_code,
            "stderr": vol_delete_stderr
        }
        return docker_event_msg

async def send_version(websocket, interval=UPDATE_INTERVAL):
    '''
    Sends the current version of the device

    Params:
    - websocket: Websocket connection to send message with.
    - interval: If 0 passed, only runs once. Otherwise will run on interval (seconds). Defaults to 1 hour.
    '''
    version_msg = {
        "type": "device-version",
        "version": __version__,
        "auto_updates": AUTO_UPDATES
    }

    if interval == 0:
        await send_websocket_message(websocket, version_msg)
        return

    while True:
        await send_websocket_message(websocket, version_msg)
        await asyncio.sleep(interval)

async def upgrade_device(client: docker.DockerClient):
    '''
    Runs Watchtower container to apply the latest update for the
    currently running Device.

    Params:
    - client: Which Docker client to launch Watchtower with.
    '''
    try:
        device_containers = client.containers.list(filters={"label": "co.fractalnetworks.mosaic=device"})
    except APIError as error:
        logging.error(f"Encountered Docker error while trying to get running devices: {error}")
        return False

    # add all device containers into a single space separated string
    container_names = ' '.join([container.name for container in device_containers])

    if container_names:
        return_code, (stdout, stderr) = await compose.run_updater(container_names)
    else:
        logging.error("Error while attempting to update: Couldn't find running Device containers.")
        return False

    if return_code != 0:
        logging.info(f"Error while attempting to update: Hive Device Updater returned a non-zero error code: {return_code} when attempting to update device: {stderr}")
        return False

    logging.debug(f"Hive Device Updater returned 0. Hive Device Updater stdout:\n{stdout}")

async def publish_volume_statuses(websocket):
    '''
    Periodic background task that uses the Fractal CLI to send latest
    timestamp for each local Fractal volume.
    '''
    from fractal.controllers.volume.util import get_local_volumes

    while True:
        volumes = get_local_volumes()

        volume_timestamps = []
        for volume in volumes:
            info = {
                "name": volume["name"],
                "pubkey": volume["pubkey"],
                "last_updated": volume["last_updated"]
            }
            volume_timestamps.append(info)

        volumes_msg = {
            "type": "volume-info",
            "volumes": volume_timestamps,
        }
        await send_websocket_message(websocket, volumes_msg)

        await asyncio.sleep(30)
