import docker
from docker import DockerClient
from docker.errors import APIError, NotFound, ImageNotFound, ContainerError
from uuid import UUID
from cryptography.hazmat.primitives.asymmetric.ed25519 import Ed25519PrivateKey
from cryptography.hazmat.primitives import serialization
from base64 import b64encode, urlsafe_b64encode, urlsafe_b64decode
from src.crypto import PassphraseEncryptionXSalsa20
import os
import sys
import logging
import asyncio
import threading
import aiofiles
import platform
import json
import yaml

APPS_PATH = os.environ.get("APPS_PATH", "/apps")

def ensure_storage_plugin_ready(client: DockerClient):
    '''
    Ensures that the Docker Storage Plugin is installed & enabled.
    '''
    # Check if a storage plugin was passed in
    STORAGE_PLUGIN = os.environ.get("STORAGE_PLUGIN")
    STORAGE_DEFAULT_TARGET = os.environ.get("STORAGE_DEFAULT_TARGET", "https://storage.fractalnetworks.co")
    STORAGE_IPFS = os.environ.get("STORAGE_IPFS", "http://magma-uninsured.fractalnetworks.co")
    PLUGIN_FORCE_MOUNT_BTRFS = os.environ.get("PLUGIN_FORCE_MOUNT_BTRFS", "true")

    # if not passed a STORAGE_PLUGIN, determine what architecture the device running on
    if not STORAGE_PLUGIN:
        architecture = platform.machine()
        if architecture == "x86_64":
            STORAGE_PLUGIN = "fractalnetworks/storage:main"
        elif architecture in {'arm64', 'aarch64'}:
            STORAGE_PLUGIN = "fractalnetworks/storage:main-arm64"
        else:
            logging.error(f"Got unsupported architecture when installing plugin: {architecture}")
            STORAGE_PLUGIN = "fractalnetworks/storage:main"
    os.environ['STORAGE_PLUGIN'] = STORAGE_PLUGIN
    try:
        logging.info(f"Fetching storage plugin: {STORAGE_PLUGIN}")
        plugin = client.plugins.get(STORAGE_PLUGIN)
    except NotFound:
        logging.info("Fractal Storage plugin not found. Installing...")
        plugin = client.plugins.install(STORAGE_PLUGIN)
    except APIError as e:
        logging.error(f"Error fetching plugin: {e}")
        sys.exit(1)

    # make sure docker plugin is enabled
    if not plugin.enabled:
        logging.info(f"Enabling {STORAGE_PLUGIN}")

        # configure plugin with provided configuration
        plugin.configure({
            "STORAGE_DEFAULT_TARGET": STORAGE_DEFAULT_TARGET,
            "STORAGE_IPFS": STORAGE_IPFS,
            "PLUGIN_FORCE_MOUNT_BTRFS": PLUGIN_FORCE_MOUNT_BTRFS
        })

        plugin.enable()

    logging.info(f"{STORAGE_PLUGIN} enabled")

def create_fractal_dir(client: DockerClient):
    '''
    Ensures that /var/lib/fractal directory is created on the Docker Host.
    '''
    logging.info("Ensuring /var/lib/fractal directory created on host.")
    try:
        client.containers.run(
            image="fractalnetworks/hive-device:alpha",
            remove=True,
            volumes=["/var/lib:/var/lib"],
            privileged=True,
            pid_mode='host',
            entrypoint="nsenter -t 1 -m -u -n -i mkdir -p /var/lib/fractal")
    except (ImageNotFound, ContainerError, APIError) as docker_err:
        logging.error(docker_err)
        sys.exit(1)

    logging.info("/var/lib/fractal directory successfully created on host.")

def is_valid_uuid(value) -> bool:
    '''
    Checks if the passed in value is a valid UUID
    '''
    try:
        UUID(str(value), version=4)
        return True
    except ValueError:
        return False

async def _read_file(file, return_lines=False) -> list|str:
    '''
    Reads file into either a list of lines or as a string.

    Returns:
    - list[lines] when return_lines = True
    - str when return_lines = False
    '''
    try:
        async with aiofiles.open(file, mode='r') as f:
            if return_lines:
                contents = await f.readlines()
            else:
                contents = await f.read()
    except FileNotFoundError:
        logging.warning(f"File not found: {file}")
        return []
    return contents

async def get_app_path_from_env(env_file) -> str:
    '''
    Parses app environment file and returns app file path for app
    in environment file

    Returns:
    - app_path(str): if env file can successfully be parsed and file contains
                and contains HIVE_APP_NAME & HIVE_APP_VERSION.
    - Empty string if the environment file doesn't exist or HIVE_APP_NAME
               or HIVE_APP_VERSION cannot be found.
    '''
    # find app name and version from env_file
    name = ''
    version = ''

    contents = await _read_file(env_file, return_lines=True)
    for line in contents:
        if "HIVE_APP_NAME" in line:
            name = line.rstrip().strip("HIVE_APP_NAME=")
        elif "HIVE_APP_VERSION" in line:
            version = line.rstrip().strip("HIVE_APP_VERSION=")

    # handles the case where the env file doesn't exist.
    if name == '' or version == '':
        return False

    app_path = f'{APPS_PATH}/{name}_{version}.yaml'
    return app_path

async def determine_app_running(client: DockerClient, app_uuid) -> bool:
    '''
    Parses app docker-compose file and determines if all containers
    for an app are correctly running.

    NOTE: Currently blocks when parsing file stream into YAML.
    NOTE: One-off "helper" containers are not supported currently. In that case,
          the device will not report the app in its instances list.

    Returns:
    - True when all containers are running
    - False when all containers are not running
    '''
    # parse env file for app name. Need to determine which app `app_uuid` refers to
    app_env_file = f'{APPS_PATH}/{app_uuid}.env'
    compose_file_path = await get_app_path_from_env(app_env_file)
    if not compose_file_path:
        logging.error(f"Error finding Compose File for {app_uuid}")
        return False

    # parse yaml
    try:
        compose_file_contents: str = await _read_file(compose_file_path, return_lines=False)
        compose_file = yaml.safe_load(compose_file_contents)
    except yaml.YAMLError as error:
        logging.error(f"Error parsing YAML: {error}")
        return False

    services = compose_file.get("services")
    num_containers_in_file = len(services.keys())

    # get the number of currently running containers whose name contains app_uuid
    running_containers = client.containers.list(filters={"name": app_uuid})
    num_running_containers = len(running_containers)

    if num_running_containers != num_containers_in_file:
        return False
    else:
        # check to make sure that all running containers are healthy
        for container in running_containers:
            try:
                health = container.attrs.get("State").get("Health").get("Status")
            except (NameError, AttributeError) as e:
                logging.warning(f"{container.name} is missing a health check. Health checks are mandatory!")
            if health != "healthy":
                return False
        return True

async def get_instances(client: DockerClient) -> list:
    '''
    Returns a list of all docker containers that are valid UUID v4.

    NOTE: client.containers.list is blocking. Find a way to make async
    '''
    # instances known to be currently running that should be sent back to the backend
    instances = set()
    # keep track of which uuids we've seen so we only run determine_app_running once per app
    uuids_handled = set()

    running_containers = client.containers.list()
    for container in running_containers:
        app_uuid = container.name[:36]
        if is_valid_uuid(app_uuid):
            if app_uuid not in uuids_handled:
                uuids_handled.add(app_uuid)
                # only add app to instances list if all containers for app are running
                app_is_running: bool = await determine_app_running(client, app_uuid)
                if app_is_running:
                    instances.add(app_uuid)
            else:
                continue

    return list(instances)

async def spawn_background_thread(coro, *args):
    '''
    Spawns a background thread for a given coroutine.

    Params:
    - coro: The coroutine to be run.
    - *args: args to be passed to coroutine

    TODO: Make sure that if the coroutine crashes it restarts
    '''
    background_loop = asyncio.new_event_loop()
    threading.Thread(target=background_loop.run_forever, daemon=True).start()
    background_loop.call_soon_threadsafe(
        asyncio.create_task, coro(*args)
    )
    logging.debug(f"Spawned background thread for {coro.__name__}")

def get_device_name() -> str:
    DEVICE_NAME = os.environ.get("DEVICE_NAME")

    # if no DEVICE_NAME provided, use system's hostname
    if not DEVICE_NAME:
        import socket
        DEVICE_NAME = socket.gethostname()

    return DEVICE_NAME

def register_with_backend(api_token: str) -> str:
    '''
    Makes request to the Hive Backend to register this device.
    '''
    from requests.adapters import HTTPAdapter, Retry
    import requests

    FRACTAL_HIVE_API = os.environ.get("FRACTAL_HIVE_API", "https://hive-api.fractalnetworks.co")

    DEVICE_NAME = get_device_name()
    logging.debug(f"Device Name: {DEVICE_NAME}")
    headers = {"Authorization": f"Token {api_token}"}
    data = {"name": DEVICE_NAME}

    session = requests.Session()
    retries = Retry(total=15, connect=15, backoff_factor=1, status_forcelist=[502, 503, 504])
    session.mount("http://", HTTPAdapter(max_retries=retries))
    response = session.post(f'{FRACTAL_HIVE_API}/api/v1/device/', headers=headers, json=data)

    if not response.ok:
        logging.error(f"Error while registering with Hive Backend: {response.status_code}")
        sys.exit(1)

    DEVICE_TOKEN = response.json().get("token")
    return DEVICE_TOKEN

async def generate_env(app: dict):
    '''
    Creates an application's environment file if it doesn't exist.
    '''
    app_instance_uuid = app["instance"]
    app_name = app["application"]["name"]
    app_version = app["application"]["version"]
    link_config = app["links"]
    storage_apikeys = app["storage_apikeys"]
    env_file_path = f'{APPS_PATH}/{app_instance_uuid}.env'

    env_vars = {
        "HIVE_APP_NAME": app_name,
        "HIVE_APP_VERSION": app_version,
        "FRACTAL_STORAGE_PLUGIN": os.environ.get("STORAGE_PLUGIN"),
    }

    # iterate through all items in link_config object and add each token/domain
    for name, config in link_config.items():
        env_vars[f"LINK_TOKEN_{name.upper()}"] = os.environ.get("DEVICE_TOKEN")
        env_vars[f"LINK_DOMAIN_{name.upper()}"] = config.get("domain")

    # set link container gateway endpoint
    env_vars[f"LINK_GATEWAY_API"] = os.environ.get("LINK_GATEWAY_API", "https://gateway.fractalnetworks.co")

    # write volume names and private keys
    for name, private_key in storage_apikeys.items():
        name = name.upper()
        env_vars[f"{name}_VOLUME_NAME"] = f'{app_instance_uuid}_{name}'
        env_vars[f"{name}_VOLUME_PRIVKEY"] = private_key

    # concatenate all env_vars into a new line separated string
    env_file_data = ""
    for name, value in env_vars.items():
        env_file_data += f"{name}={value}\n"

    # create and write concatenated env vars to env file
    async with aiofiles.open(env_file_path, 'w') as env_file:
        await env_file.write(env_file_data)
    logging.debug(f'Wrote data to envfile: {env_file_data}')

def generate_storage_keys(*names) -> dict:
    '''
    Generates base64 encoded ED25519 private keys for given number of names.

    Returns:
    - storage_keys(dict)
    '''
    storage_keys = {}

    for name in names:
        private_key = Ed25519PrivateKey.generate()
        private_key = private_key.private_bytes(
            encoding=serialization.Encoding.Raw,
            format=serialization.PrivateFormat.Raw,
            encryption_algorithm=serialization.NoEncryption())
        private_key = b64encode(private_key).decode("ascii")
        storage_keys[name] = private_key

    return storage_keys

def get_pubkey(private_key: str) -> str:
    '''
    Gets a public key for a given private key
    '''
    private_key = urlsafe_b64decode(private_key)
    private_key = Ed25519PrivateKey.from_private_bytes(private_key)
    public_key = private_key.public_key()
    public_key = public_key.public_bytes(
        encoding=serialization.Encoding.Raw,
        format=serialization.PublicFormat.Raw
    )
    public_key = urlsafe_b64encode(public_key).decode('utf-8')
    return public_key

def get_storage_keys(key: PassphraseEncryptionXSalsa20, legacy_storage_apikeys: dict, encrypted_storage_apikeys: str) -> tuple:
    '''
    Either generates storage keys or returns decrypted storage keys.

    Params:
    - key(PassphraseEncryptionXSalsa20): Encryption key to encrypt with.
    - legacy_storage_apikeys(dict): Unencrypted storage keys from an earlier version
    of the Hive Backend.
    - encrypted_storage_apikeys(str): Blob of storage keys that were encrypted
    with DEVICE_SECRET.

    Returns:
    - tuple(storage_apikeys(dict), pubkey(str))
    '''
    # got legacy storage keys, simply return them
    if legacy_storage_apikeys and not encrypted_storage_apikeys:
        storage_apikeys: dict = legacy_storage_apikeys
        # get pubkey for the app private key
        pubkey = get_pubkey(storage_apikeys.get('app'))
        return storage_apikeys, pubkey

    # decrypt storage keys
    elif encrypted_storage_apikeys:
        storage_apikeys: dict = json.loads(key.decrypt(encrypted_storage_apikeys))
        # get pubkey for the app private key
        pubkey = get_pubkey(storage_apikeys.get('app'))

    # no storage keys were given. generate them
    else:
        # FIXME: figure out how to dynamically create volumes as needed per app
        storage_apikeys: dict = generate_storage_keys('app', 'link')
        # get pubkey for the app private key
        pubkey = get_pubkey(storage_apikeys.get('app'))

    return storage_apikeys, pubkey

def str_to_bool(input) -> bool:
    '''
    Converts a string Boolean to a Python Boolean.
    '''
    if type(input) == str:
        if input in {'True', 'true'}:
            return True
        else:
            return False
    return bool(input)
