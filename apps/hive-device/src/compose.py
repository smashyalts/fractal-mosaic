from shlex import quote
import asyncio
import logging
import os
import time
from src.utils import APPS_PATH

DEVICE_IMAGE = os.environ.get("DEVICE_IMAGE", "fractalnetworks/hive-device:alpha")

async def pull(compose_file_path: str, env_file_path: str, project_name: str) -> tuple:
    '''
    Pull latest containers for a docker compose project.
    '''
    cmd = f"docker-compose -f {APPS_PATH}/link.yaml -f {compose_file_path} --env-file {env_file_path} -p {project_name} pull"
    return await _run_command(cmd)

async def up(compose_file_path: str, env_file_path: str, project_name: str) -> tuple:
    '''
    Runs docker-compose up in a subprocess.

    Params:
    - compose_file_path(str): The path to the docker-compose file.
    - env_file_path(str): The path to the environment file.
    - project_name(str): The project name to use.

    Returns:
    - Exit Status of command (int)
    '''
    # always pull before launching TODO user managed upgrades
    await pull(compose_file_path, env_file_path, project_name)

    cmd = f"docker-compose -f {APPS_PATH}/link.yaml -f {compose_file_path} --env-file {env_file_path} -p {project_name} up --wait"
    return await _run_command(cmd)

async def volume_create(volume_name: str, volume_mode: str, privkey: str) -> tuple:
    '''
    Runs docker volume create in a subprocess. This allows for the device
    to begin pulling in storage volume snapshots.

    Params:
    - volume_name(str): The name of the volume
    - volume_mode(str): The mode of the volume (read or write)
    - privkey(str): The private key to use for the volume

    Returns:
    - Exit Status of command (int)

    TODO: Maybe use Python Docker API? Problem with using Docker API right now
          is that it's blocking. Would need to spawn a thread instead.

          Downside to current approach is that we have to install the
          Docker CLI in the container.
    '''
    STORAGE_PLUGIN = os.environ.get("STORAGE_PLUGIN")
    volume_exists = await check_volume_exists(volume_name)
    if volume_exists:
        logging.debug(f"Volume ({volume_name}) exists. Not recreating")
        return 0, (b'', b'')

    # determine which mode to set the docker volume to
    if volume_mode == "read":
        cmd = f"docker volume create --driver {STORAGE_PLUGIN} --opt mode=read --opt privkey={privkey} {volume_name}"
        # cmd = f"docker volume create {volume_name}"
        logging.debug(f"create read-only volume cmd: {cmd}")

    elif volume_mode == "write":
        cmd = f"docker volume create --driver {STORAGE_PLUGIN} --opt mode=write --opt create --opt privkey={privkey} {volume_name}"
        # cmd = f"docker volume create {volume_name}" # FIXME: this is just for testing
        logging.debug(f"create write volume cmd: {cmd}")

    else:
        logging.error(f"volume_create got unsupported volume_mode: {volume_mode}")
        return 1, (b'', b'')

    # execute command in subprocess and await result
    logging.debug(f"Volume ({volume_name}) didn't exist. Creating")

    return await _run_command(cmd)

async def volume_delete(volume_name: str) -> tuple:
    '''
    Runs docker volume rm in a subprocess. This makes sure that there isn't a
    writeable volume when told to stop.

    Params:
    - volume_name(str): The name of the volume

    Returns:
    - Exit Status of command (int)
    '''
    logging.debug(f"Removing volume ({volume_name})")
    cmd = f"docker volume rm {volume_name}"
    return await _run_command(cmd)


async def check_volume_exists(volume_name: str) -> bool:
    '''
    Runs docker volume inspect <volume_name> in a subprocess.

    Params:
    - volume_name(str): The name of the volume

    Returns:
    - True - if subprocess return code is 0
    - False - if subprocess is not 0
    '''
    cmd = f"docker volume inspect {volume_name}"
    return_code, _ = await _run_command(cmd)

    if return_code == 0:
        return True
    else:
        return False


async def down(compose_file_path: str, env_file_path: str, project_name: str) -> tuple:
    '''
    Runs docker-compose down in a subprocess.

    Params:
    - compose_file_path(str): The path to the docker-compose file.
    - env_file_path(str): The path to the environment file.
    - project_name(str): The project name to use.

    Returns:
    - Exit Status of command (int)
    '''

    cmd = f"docker-compose -f {APPS_PATH}/link.yaml -f {compose_file_path} --env-file {env_file_path} -p {project_name} down -v"
    return await _run_command(cmd)

async def exec_command(container: str, command: str):
    '''
    Runs a given command on a given container.

    NOTE: Intended to be run in a separate thread for now.

    Params:
    - container(str): Container to run command in.
    - command(str): Command to run in container
    '''
    logging.debug(f"Execing command: {command} in container: {container}")

    # TODO make sure command is part of the set of "trusted" commands
    # sanitize container string
    container = quote(container)

    # attempt to run command for a set number of tries
    return_code = 1
    max_retries = 5
    while return_code != 0 and max_retries:
        cmd = f"docker exec -it {container} {command}"
        return_code, (stdout, stderr) = await _run_command(cmd)
        logging.debug(f'Exec Command stdout: {stdout}, Exec Command stderr: {stderr}')
        if return_code == 0:
            # TODO send ui-event command success
            return
        max_retries = max_retries-1
        time.sleep(2)
    # TODO send ui-event command failed

async def run_updater(device_names: str):
    '''
    Runs Watchtower to update the given device_name container.
    '''
    logging.info(f"Attempting to update {device_names}")

    # Dont try to pull for local image
    if DEVICE_IMAGE == 'hive-device:local':
        cmd = f"docker run -it --rm -v /var/run/docker.sock:/var/run/docker.sock --name Hive-Device-Updater containrrr/watchtower:1.4.0 {device_names} --run-once --stop-timeout 30s --no-pull"
    else:
        cmd = f"docker run -it --rm -v /var/run/docker.sock:/var/run/docker.sock --name Hive-Device-Updater containrrr/watchtower:1.4.0 {device_names} --run-once --stop-timeout 30s"

    # check if DOCKER_HOST variable is set. If it is, configure Watchtower to use it
    DOCKER_HOST = os.environ.get("DOCKER_HOST")
    if DOCKER_HOST:
        cmd += f' --host {DOCKER_HOST}'

    return await _run_command(cmd)

async def run_device_command(cmd: str):
    '''
    Runs given device command
    '''
    return await _run_command(cmd)

async def _run_command(cmd: str) -> tuple:
    logging.info(f"Running command: {cmd}")
    try:
        proc = await asyncio.create_subprocess_shell(
            cmd,
            stdout = asyncio.subprocess.PIPE, # piping in case we want to use output. Currently not used
            stderr = asyncio.subprocess.PIPE)
        stdout, stderr = await proc.communicate()
        return proc.returncode, (stdout.decode('utf-8'), stderr.decode('utf-8'))

    except asyncio.CancelledError:
        proc.terminate()
        logging.error(f"Cancelled: {cmd}")
        return 1, (b'', b'')
