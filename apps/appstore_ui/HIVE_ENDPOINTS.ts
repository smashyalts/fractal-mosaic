export const APP_ENDPOINT = `${process.env.NEXT_PUBLIC_MOSAIC_API}app/`;
export const APP_LIST_ENDPOINT = `${process.env.NEXT_PUBLIC_MOSAIC_API}app/?installed=True`;
export const CATALOG_APP_ENDPOINT = `${process.env.NEXT_PUBLIC_MOSAIC_API}app/`;
export const CATALOG_APP_LIST_ENDPOINT = `${process.env.NEXT_PUBLIC_MOSAIC_API}app/?installed=False`;
export const GENERATE_TOKEN_ENDPOINT = `${process.env.NEXT_PUBLIC_MOSAIC_API}token/`;
