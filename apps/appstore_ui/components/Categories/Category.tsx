// React
import {useEffect, useState} from 'react'

// Icons
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

// Bootstrap
import {Button} from 'react-bootstrap'

// Util
import {DisplayComponentProps} from 'types'

// Recoil
import {categoriesState} from '../../recoil/categories'
import {useRecoilState} from 'recoil'

const Category = ({icn, title}: DisplayComponentProps) => {
	const [categoryState, setCategoryState] = useRecoilState(categoriesState)
	// Active
	const [active, setActive] = useState(false)
	const toggleActive = () => {
		const currentActiveState = !active
		setActive(!active)
		if (currentActiveState) {
			setCategoryState([...categoryState, title as string])
		} else {
			const filteredArr = categoryState.filter((item) => item !== title)
			setCategoryState(filteredArr)
		}
	}

	useEffect(() => {
		if (categoryState.includes(title as string)) {
			setActive(true)
		}
	}, [categoryState])

	return (
		<Button
			variant='link'
			onClick={toggleActive}
			className={`
				${active && 'text-black fw-bold'}
				w-100 py-1 px-0 d-flex flex-nowrap align-items-center gap-2 text-decoration-none 
			`}>
			{icn && <FontAwesomeIcon icon={icn} fixedWidth />}
			<span className='ms-1 me-auto'>{title}</span>
			{active ? (
				<FontAwesomeIcon icon={['fas', 'times']} fixedWidth />
			) : (
				<div style={{width: '20px', height: '20px'}}></div>
			)}
		</Button>
	)
}
export default Category
