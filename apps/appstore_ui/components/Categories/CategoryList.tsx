import React from 'react'
import {LayoutProps} from 'types'
import Category from '../../components/Categories/Category'

const CategoryList = () => {
	return (
		<div className='w-100 px-3 d-flex flex-column'>
			<Category title='Collaboration' icn={['fas', 'people-group']} />
			<Category title='Media' icn={['fas', 'photo-film']} />
			<Category title='Security' icn={['fas', 'lock']} />
		</div>
	)
}

export default CategoryList
