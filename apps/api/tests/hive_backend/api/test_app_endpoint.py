from hive_backend.api.models import Device
import pytest
import uuid

APP_ENDPOINT='/api/v1/app/'

pytestmark = pytest.mark.django_db

def test_app_uuid_list(authenticated_client, test_app_instance):
    '''
    Ensure that user can retrieve a list of their app instance uuids.
    '''
    client = authenticated_client
    resp = client.get(APP_ENDPOINT)
    data = resp.json()
    assert resp.status_code == 200
    assert type(data) == list
    assert data[0]["uuid"] == str(test_app_instance.uuid)

def test_query_params(authenticated_client, test_app_instance):
    '''
    Ensure that query params are correctly filtering the app endpoint.
    '''
    client = authenticated_client

    # expect to only get installed app instances
    resp = client.get(f'{APP_ENDPOINT}?installed=true')
    data = resp.json()
    assert resp.status_code == 200
    assert len(data) > 0
    assert data[0]["uuid"] == str(test_app_instance.uuid)

    # expect to not get any app instances back.
    # test user's only app instance is installed
    resp = client.get(f'{APP_ENDPOINT}?installed=false')
    data = resp.json()
    assert resp.status_code == 200
    assert len(data) == 0

    # Uninstall the app instance
    test_app_instance.state = "not-installed"
    test_app_instance.save()

    # expect app instance to show up in the uninstalled filter
    resp = client.get(f'{APP_ENDPOINT}?installed=false')
    data = resp.json()
    assert resp.status_code == 200
    assert len(data) > 0
    assert data[0]["uuid"] == str(test_app_instance.uuid)

    # expect to not get any app instances back.
    # test user's only app instance is not-installed
    resp = client.get(f'{APP_ENDPOINT}?installed=true')
    data = resp.json()
    assert resp.status_code == 200
    assert len(data) == 0

def test_app_detail(authenticated_client, test_app_instance):
    '''
    Ensure that the user can retrieve details about an app instance
    '''
    client = authenticated_client
    resp = client.get(f'{APP_ENDPOINT}{test_app_instance.uuid}/')
    data = resp.json()
    assert resp.status_code == 200
    assert data["uuid"] == str(test_app_instance.uuid)
    assert data["name"] == test_app_instance.name
    assert data["app"] == str(test_app_instance.app.uuid)
    assert data["state"] == "installed"
    assert data["device"] == str(test_app_instance.device.uuid)
    assert data["links"]["default"]["domain"] == "test-link.fractal.pub"
    # make sure storage keys are not returned by serializer
    assert data.get("storage_apikeys") == None
    assert data["target_state"] == "running"

def test_app_name_check(authenticated_client, test_app_instance):
    '''
    Using an app uuid correctly returns app instance if exists.
    '''
    client = authenticated_client
    resp = client.get(f'{APP_ENDPOINT}{test_app_instance.app.uuid}/app-name/')
    assert resp.status_code == 200

    data = resp.json()
    assert type(data) == dict
    assert data["name"] == "WikiJS"
    assert data["app"] == str(test_app_instance.app.uuid)
    assert data["state"] == "installed"
    assert data["device"] == str(test_app_instance.device.uuid)
    assert data["links"]["default"]["domain"] == "test-link.fractal.pub"
    # make sure storage keys are not returned by serializer
    assert data.get("storage_apikeys") == None
    assert data["target_state"] == "running"

    # expect a random app uuid to return None
    random_uuid = uuid.uuid4()
    resp = client.get(f'{APP_ENDPOINT}{random_uuid}/app-name/')
    assert resp.status_code == 200
    assert resp.data == None

def test_can_create_app_instance(authenticated_client, test_user_uuid, fractal_cloud_device_uuid, test_app_uuid, mock_link_generation, mock_device_request_success):
    '''
    User can create an app instance using their Fractal Cloud device and not specifying a name.
    '''
    client = authenticated_client

    # expect fetching the Fractal Cloud Device to raise DoesNotExist exception
    # since the Device hasn't been created yet
    with pytest.raises(Device.DoesNotExist):
        Device.objects.get(owner__uuid=test_user_uuid, name="Fractal Cloud Device")

    app_data = {
        "app": test_app_uuid,
        "state": "installed",
        "device": fractal_cloud_device_uuid,
        "target_state": "running"
    }
    resp = client.post(APP_ENDPOINT, data=app_data)
    assert resp.status_code == 201

    device = Device.objects.get(owner__uuid=test_user_uuid, name="Fractal Cloud Device")

    data = resp.json()
    assert data["owner"] == test_user_uuid
    assert data["name"] == "WikiJS"
    assert data["app"] == test_app_uuid
    assert data["state"] == "installed"
    assert data["device"] == str(device.uuid)
    assert data["links"]["default"]["domain"] == "test-link.fractal.pub"
    # make sure storage keys are not returned by serializer
    assert data.get("storage_apikeys") == None
    assert data["target_state"] == "running"

def test_can_patch_app_instance(authenticated_client, fractal_cloud_device_uuid,  test_app_instance, mock_device_request_success):
    '''
    PATCHing an app instance's device to the Fractal Cloud Device UUID
    correctly creates the Device
    '''
    client = authenticated_client

    # expect fetching the Fractal Cloud Device to raise DoesNotExist exception
    # since the Device hasn't been created yet
    with pytest.raises(Device.DoesNotExist):
        device = Device.objects.get(owner=test_app_instance.owner, name="Fractal Cloud Device")

    app_data = {
        "target_state": "stopped",
        "device": fractal_cloud_device_uuid
    }
    resp = client.patch(f'{APP_ENDPOINT}{test_app_instance.uuid}/', data=app_data)
    assert resp.status_code == 200
    data = resp.json()

    # ensure Fractal Cloud Device is created
    device = Device.objects.get(owner=test_app_instance.owner, name="Fractal Cloud Device")

    assert data["reschedule_to_device"] == str(device.uuid)
    assert data["target_state"] == "stopped"
