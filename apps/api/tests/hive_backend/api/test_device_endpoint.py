from hive_backend.api.models import Device
import pytest
import uuid

DEVICE_ENDPOINT = '/api/v1/device/'

pytestmark = pytest.mark.django_db


'''
Tests TODO:

1. Ensure that when device-alive message sent to api/v1/device/event/
   the Device uuid is added to the cache.
'''

def test_can_get_device_list(authenticated_client, test_user_uuid, test_device_uuid):
    '''
    Ensure authenticated user gets back a list of devices.
    '''
    client = authenticated_client
    resp = client.get(DEVICE_ENDPOINT)
    data = resp.json()

    assert resp.status_code == 200
    assert type(data) == list
    assert len(data) == 1
    assert test_device_uuid == data[0]["uuid"]
    assert test_user_uuid == data[0]["owner"]

def test_can_get_device_detail(authenticated_client, test_user_uuid, test_device_uuid):
    '''
    Ensure authenticated user gets json with info related to device requested.
    '''
    client = authenticated_client
    resp = client.get(f'{DEVICE_ENDPOINT}{test_device_uuid}/')
    data = resp.json()

    assert resp.status_code == 200
    assert type(data) == dict
    assert data["uuid"] == test_device_uuid
    assert data["owner"] == test_user_uuid

def test_created_device_in_device_list(authenticated_client):
    '''
    Ensure authenticated user's created device is returned in device list.
    '''
    client = authenticated_client
    client.post(DEVICE_ENDPOINT, data={"name": "test-device"})
    resp = client.get(DEVICE_ENDPOINT)
    data = resp.json()

    assert resp.status_code == 200
    assert len(data) == 2
    assert data[0]["name"] == 'test-device'

def test_create_device_returns_token(authenticated_client):
    '''
    Ensure authenticated user receives a device token when creating a device.
    '''
    client = authenticated_client
    resp = client.post(DEVICE_ENDPOINT, data={"name": "test-device"})

    assert resp.status_code == 201
    assert resp.json()['token']

def test_can_update_device(authenticated_client, test_device_uuid):
    '''
    Ensure authenticated user can update their device's name.
    '''
    client = authenticated_client
    resp = client.patch(f'{DEVICE_ENDPOINT}{test_device_uuid}/', data={"name": "test-device-changed"})
    data = resp.json()

    assert resp.status_code == 200
    assert data["name"] == "test-device-changed"

def test_can_delete_device(authenticated_client, test_device_uuid):
    '''
    Ensure authenticated user can delete a device.
    '''
    client = authenticated_client
    resp = client.delete(f'{DEVICE_ENDPOINT}{test_device_uuid}/')
    assert resp.status_code == 204

    # expect user's device list to be empty
    resp = client.get(DEVICE_ENDPOINT)
    data = resp.json()
    assert len(data) == 0

def test_device_event_requires_system_auth(authenticated_client, admin_authenticated_client):
    '''
    Ensure that only system_scoped JWTs can authenticate
    with the device event endpoint
    '''
    # expect request to be unauthorized when making a request with a user token
    unauthorized_client = authenticated_client
    resp = unauthorized_client.post(f'{DEVICE_ENDPOINT}event/')
    assert resp.status_code == 401

    authorized_client = admin_authenticated_client
    resp = authorized_client.post(f'{DEVICE_ENDPOINT}event/')
    assert resp.status_code == 200

def test_device_event_device_not_exists(admin_authenticated_client):
    client = admin_authenticated_client

    # generate an event with random device & owner uuids
    event = {
        "device": uuid.uuid4(),
        "owner": uuid.uuid4()
    }
    resp = client.post(f'{DEVICE_ENDPOINT}event/', data=event)

    # expect the device event endpoint to gracefully return a 200
    assert resp.status_code == 200

def test_delete_device_on_app_instance(test_app_instance):
    '''
    Ensure that handle_device_cascade pre_delete signal called.
    '''
    Device.objects.get(uuid=test_app_instance.device.uuid).delete()
    test_app_instance.refresh_from_db()
    assert test_app_instance.device == None
