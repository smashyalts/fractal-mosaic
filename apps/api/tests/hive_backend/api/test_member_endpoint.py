import pytest

MEMBER_ENDPOINT = '/api/v1/member/'

pytestmark = pytest.mark.django_db

def test_member_list(authenticated_client, test_user_uuid):
    '''
    Ensure that user can get member list.
    '''
    client = authenticated_client
    resp = client.get(MEMBER_ENDPOINT)
    assert resp.status_code == 200

    data = resp.json()
    assert type(data) == list
    assert data[0]["uuid"] == test_user_uuid

def test_member_detail(authenticated_client, test_user_uuid):
    '''
    Ensure that user can get member details.
    '''
    client = authenticated_client
    resp = client.get(f'{MEMBER_ENDPOINT}{test_user_uuid}/')
    assert resp.status_code == 200

    data = resp.json()
    assert type(data) == dict
    assert data["uuid"] == test_user_uuid
