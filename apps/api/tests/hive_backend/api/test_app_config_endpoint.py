from unittest.mock import patch
import pytest

pytestmark = pytest.mark.django_db

APP_CONFIG_ENDPOINT = '/api/v1/appinstance_config/'
def test_app_config_create(authenticated_client, element_app_uuid, create_app_instance):
    '''
    Test that app config can be created for an app. Make sure that
    the inputted user password is not stored in data.
    '''
    client = authenticated_client
    element_instance = create_app_instance(element_app_uuid, "installed", "running")
    username = 'test_username'
    password = 'test_password'
    device = element_instance.device
    app_instance_uuid = str(element_instance.uuid)

    config_data = {
        "appinstance": app_instance_uuid,
        "data": {
            "username": username,
            "password": password
        }
    }

    # make sure that when a POST to app config is made, that create_element_admin is called
    with patch('hive_backend.api.views.create_element_user', return_value=True) as patched:
        resp = client.post(APP_CONFIG_ENDPOINT, data=config_data, format='json')
        patched.assert_called_once_with(username, password, device, element_instance, admin=True)

    # ensure that config is created and that password is not returned in response
    resp_data = resp.json()
    assert resp_data['appinstance'] == app_instance_uuid
    assert resp_data['data']['username'] == username
    assert 'password' not in resp_data['data']

