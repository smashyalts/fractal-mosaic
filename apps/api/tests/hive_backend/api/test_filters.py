import pytest

from hive_backend.api.models import User

from hive_backend.api.filters  import UserFilterBackend, IsOwnerFilterBackend

# mark entire module as needing django db functionality
# pytest will create an empty hive_test database then remove it
# when tests are completed

# by default, tests are not isolated so be careful
# use transactions if test isolation is needed
pytestmark = pytest.mark.django_db
def _all_objects_are_blong_to_me(response, myUUID, key='owner'):
    data = response.json()
    for item in data:
        assert myUUID == item[key]
    return True

def tests_filter_backends(authenticated_client, test_user_uuid):
    '''
    Ensure protected endpoint only return objects belogning to the authenticated user.
    '''
    client = authenticated_client
    resp = client.get('/api/v1/device/')
    assert _all_objects_are_blong_to_me(resp, test_user_uuid)
    resp = client.get('/api/v1/app/')
    assert _all_objects_are_blong_to_me(resp, test_user_uuid)
    resp = client.get('/api/v1/member/')
    assert _all_objects_are_blong_to_me(resp, test_user_uuid, key='uuid')
    resp = client.get('/api/v1/token/')
    assert _all_objects_are_blong_to_me(resp, test_user_uuid)