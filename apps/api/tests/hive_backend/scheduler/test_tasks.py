from unittest.mock import patch
from hive_backend.tasks.executor import command_executor, mark_devices_unhealthy
import pytest

DEVICE_EVENT_ENDPOINT = '/api/v1/device/event/'
pytestmark = pytest.mark.django_db

def test_executor_single_command(generate_device_commands):
    '''
    Test that the command executor sends a device request
    for a single command.
    '''
    command = generate_device_commands(sequence=False)
    with patch('hive_backend.tasks.executor.send_device_request.delay', return_value=True) as patched:
        return_value = command_executor(command)
        patched.assert_called_once_with(current_command=command[0])

    assert return_value == True

def test_executor_sequence_commands(generate_device_commands):
    '''
    Test that the command executor sends a device request
    for a sequence of commands.
    '''
    # generated a sequence of commands. The first element is the sequence of commands
    commands = generate_device_commands(sequence=True)
    with patch('hive_backend.tasks.executor.send_device_request.delay', return_value=True) as patched:
        return_value = command_executor(commands)
        # TODO: figure out how to see how many times patched was called.
        patched.assert_called()

    assert return_value == True

def test_healthy_device_in_cache(admin_authenticated_client, test_device_uuid, test_user_uuid, test_app_instance, test_cache):
    '''
    Ensure that if device service sends "device-alive" for a device
    that the device is added into the cache with a 60 second TTL.
    '''
    cache = test_cache
    client = admin_authenticated_client

    # ensure that the device is not in the cache currently
    device_in_cache = cache.has_key(f'seen-{test_device_uuid}')
    assert device_in_cache == False

    device_alive_data = {
        "type": "device-alive",
        "device": test_device_uuid,
        "account": test_user_uuid
    }
    resp = client.post(DEVICE_EVENT_ENDPOINT, data=device_alive_data)
    assert resp.status_code == 200

    cache_device_uuid = cache.get(f'seen-{test_device_uuid}')
    cache_ttl = cache.ttl(f'seen-{test_device_uuid}')
    assert cache_device_uuid == test_device_uuid
    assert cache_ttl <= 60

def test_mark_devices_unhealthy(test_device_uuid, test_app_instance, test_cache):
    '''
    Tests that the mark_devices_unhealthy function correctly marks all devices
    not currently in the cache unhealthy.
    '''
    cache = test_cache
    test_device = test_app_instance.device
    assert test_app_instance.health == "green"
    assert test_device.health == "green"

    # ensure that the device is not in the cache currently
    device_in_cache = cache.has_key(f'seen-{test_device_uuid}')
    assert device_in_cache == False

    result = mark_devices_unhealthy()
    assert result == True

    # refresh instance / device
    test_device.refresh_from_db()
    test_app_instance.refresh_from_db()

    assert test_device.health == "red"
    assert test_app_instance.health == "red"

    # trigger a ping which updates health to green & adds device to cache
    test_device.handle_ping_event()

    # ensure that device is not marked unhealthy since it will be in the cache
    result = mark_devices_unhealthy()
    assert result == True

    device_in_cache = cache.has_key(f'seen-{test_device_uuid}')
    cache_ttl = cache.ttl(f'seen-{test_device_uuid}')

    assert device_in_cache == True
    assert test_device.health == "green"
    assert cache_ttl <= 60



# TODO: Test the send_device_request function!
