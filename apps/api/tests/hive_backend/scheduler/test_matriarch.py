from hive_backend.scheduler import AppInstanceCurrentState, AppInstanceDesiredState, AppInstanceState
from hive_backend.scheduler.matriarch import schedule
import pytest
import uuid

pytestmark = pytest.mark.django_db

def test_schedule_start(test_app_instance_context, test_device_uuid):
    '''
    Ensure that matriarch schedules an app to start that isn't running.
    '''
    context = test_app_instance_context
    test_app_instance_uuid = context['instance']

    current_state = AppInstanceCurrentState(
        AppInstanceState.stopped,
        test_app_instance_uuid,
        test_device_uuid)
    desired_state = AppInstanceDesiredState(
        AppInstanceState.running,
        test_app_instance_uuid,
        test_device_uuid)
    device_commands = schedule(current_state, desired_state, context)

    assert type(device_commands) == list
    assert len(device_commands) == 1
    assert device_commands[0]['command'] == "SchedulerAction.start"
    assert device_commands[0]['device'] == test_device_uuid
    assert device_commands[0]['payload']['instance'] == test_app_instance_uuid

def test_schedule_stop(test_app_instance_context, test_device_uuid):
    '''
    Ensure that matriarch schedules an app to stop that is running.
    '''
    context = test_app_instance_context
    test_app_instance_uuid = context['instance']

    current_state = AppInstanceCurrentState(
        AppInstanceState.running,
        test_app_instance_uuid,
        test_device_uuid)
    desired_state = AppInstanceDesiredState(
        AppInstanceState.stopped,
        test_app_instance_uuid,
        test_device_uuid)
    device_commands = schedule(current_state, desired_state, context)

    assert type(device_commands) == list
    assert len(device_commands) == 1
    assert device_commands[0]['command'] == "SchedulerAction.stop"
    assert device_commands[0]['device'] == test_device_uuid
    assert device_commands[0]['payload']['instance'] == test_app_instance_uuid

def test_reschedule_to_new_device(test_app_instance, test_app_instance_context, test_device_uuid):
    '''
    Ensure that matriarch reschedules an app to another device that is running.
    '''
    device2_uuid = str(uuid.uuid4())
    test_app_instance_uuid = str(test_app_instance.uuid)
    context = test_app_instance_context

    current_state = AppInstanceCurrentState(
        AppInstanceState.running,
        test_app_instance_uuid,
        test_device_uuid)
    desired_state = AppInstanceDesiredState(
        AppInstanceState.running,
        test_app_instance_uuid,
        device2_uuid)
    device_commands = schedule(current_state, desired_state, context)
    assert type(device_commands) == list

    # cast Django Rest Framework Serializer ReturnList into a list,
    # then check that schedule returned a list of commands to run sequentially
    seq_commands = list(device_commands[0])
    assert len(seq_commands) == 2

    # expect the first command to be a stop on the current device
    assert seq_commands[0]['command'] == "SchedulerAction.stop"
    assert seq_commands[0]['device'] == test_device_uuid
    assert seq_commands[0]['payload']['instance'] == test_app_instance_uuid

    # expect the second command to be a start on the new device
    assert seq_commands[1]['command'] == "SchedulerAction.start"
    assert seq_commands[1]['device'] == device2_uuid
    assert seq_commands[1]['payload']['instance'] == test_app_instance_uuid
