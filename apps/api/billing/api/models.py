from django.db import models


class BaseModel(models.Model):
    created_date = models.DateTimeField(auto_now_add=True)
    deleted = models.BooleanField(default=False)
    modified_date = models.DateTimeField(auto_now=True)

    class Meta:
        abstract=True


class Donation(BaseModel):
    owner = models.ForeignKey('api.User', on_delete=models.DO_NOTHING)
    amount = models.IntegerField()

    def __str__(self) -> str:
        return f'${self.amount / 100} - {self.owner}'
