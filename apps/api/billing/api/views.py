from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from django.core.cache import caches
from django.core.exceptions import ObjectDoesNotExist
from .models import Donation
import stripe
import os
import json

stripe.api_key = os.environ.get("STRIPE_API_KEY")
endpoint_secret = os.environ.get("STRIPE_WEBHOOK_SECRET")
cache = caches['redis']

def handle_payment_success(payment_intent_id: str):
    try:
        data = cache.get(f'donation-{payment_intent_id}')
        intent_data = json.loads(data)
    except ObjectDoesNotExist as e:
        raise e

    try:
        Donation.objects.create(owner_id=intent_data['owner'], amount=intent_data['amount'])
        cache.delete(f'donation-{payment_intent_id}')

    except Exception as e:
        raise e

def calculate_order_amount(item):
    # Calculate the order total on the server to prevent
    # people from directly manipulating the amount on the client 
    return item * 100


class PaymentIntent(APIView):
    authentication_classes = []
    permission_classes = []

    def post(self, request, format=None):
        try:
            data = request.data
            # Create a PaymentIntent with the order amount and currency
            intent = stripe.PaymentIntent.create(
                amount=calculate_order_amount(data['item']),
                currency='usd',
                payment_method_types=["card"],
            )
            # print(f'Intent: {intent}')
            intent_data = {'owner': data.get('uuid'), 'amount': intent.amount}
            cache.set(f'donation-{intent.id}', json.dumps(intent_data), timeout=3600)
            return Response({'clientSecret': intent['client_secret']})
        except Exception as e:
            return Response(error=str(e), status=403)


class WebhookView(APIView):
    authentication_classes = []
    permission_classes = []

    def post(self, request, format=None):
        event = None
        payload = request.body
        sig_header = request.headers['STRIPE_SIGNATURE']

        try:
            event = stripe.Webhook.construct_event(
                payload, sig_header, endpoint_secret
            )
        except ValueError as e:
            # Invalid payload
            raise e
        except stripe.error.SignatureVerificationError as e:
            # Invalid signature
            raise e

        event_type = event['type']
        event_object = event['data']['object']

        if event and event_type == 'payment_intent.succeeded':
            payment_intent_id = event_object['id']
            handle_payment_success(payment_intent_id)
            return Response({'success': 'True'}, status=status.HTTP_200_OK)
        
        return Response({'success': 'True'}, status=status.HTTP_200_OK)
