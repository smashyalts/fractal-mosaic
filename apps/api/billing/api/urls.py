from django.urls import re_path
from billing.api import views as api_views

urlpatterns = [
    re_path(r'^payment_intent/$', api_views.PaymentIntent.as_view(), name='payment_intent'),
    re_path(r'^webhook/$', api_views.WebhookView.as_view(), name='webhook'),
]
