from setuptools import find_packages, setup

setup(
    name='fractal_billing_api',
    packages=find_packages(),
    version='0.1.0',
    description='Billing Api for Fractal Networks',
    author='Fractal Networks',
    license='MIT',
    install_requires=['stripe'], 
)
