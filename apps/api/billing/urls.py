"""billing URL Configuration
"""
from django.urls import re_path
from django.conf.urls import include

urlpatterns = [
    re_path(r"^", include("billing.api.urls")), 
]
