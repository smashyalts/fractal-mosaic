from django.conf import settings
import logging 

class RequireProductionTrue(logging.Filter):
    def filter(self, record):
        if settings.DJANGO_ENV == 'PROD': 
            return True
        return False
