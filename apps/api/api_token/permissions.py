from rest_framework import permissions


class APITokenPermissions(permissions.IsAuthenticated):

    def has_permission(self, request, view):
        """
        TODO: Should have some sort of set of capabilties per view?
        TODO: Inherit from IsAuthenticated

        Example: In order to make POST request to DeviceViewSet, token must
                 have 'add_device'.
        """
        authenticated = super().has_permission(self, request, view)

        if authenticated:
            try:
                if 'add_device' in request.auth.capabilities:
                    return True
                return False

            # Received a JWT
            # TODO FIXME
            # this causes an issue in tests when doing client.login -- probably a good idea to understand why
            except AttributeError:
                return True
        return False
