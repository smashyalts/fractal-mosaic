from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAdminUser
from hive_backend.api.views import UserOwnedBaseViewSet
from api_token.models import DeviceToken, FractalAPIToken
from api_token.serializers import FractalAPITokenSerializer
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist


class FractalAPITokenViewSet(UserOwnedBaseViewSet):
    serializer_class = FractalAPITokenSerializer

    def get_queryset(self):
        return FractalAPIToken.objects.filter(owner=self.request.user)

    def list(self, request, *args, **kwargs):
        users_tokens = FractalAPIToken.objects.filter(owner=self.request.user)
        users_tokens = self.get_serializer(users_tokens, many=True).data

        response = []

        # only show portion of token
        for token_object in users_tokens:
            object = {
                'token': f'{token_object.pop("key")[:8]}...',
                **token_object
            }

            response.append(object)

        return Response(response, status=status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)

        response = {
            'token': serializer.instance.key,
        }

        return Response(response, status.HTTP_201_CREATED)

class CheckTokenView(APIView):
    '''
    system scope / admin required for this view

    Checks if provided token is either a API or Device Token.
    '''
    permission_classes = [IsAdminUser]

    def try_get_user_from_token(self, token):
        '''
        Attempts to fetch user for provided token. Checks
        FractalAPIToken and DeviceToken tables.
        '''
        try:
            # check if provided token is a FractalAPIToken
            token = FractalAPIToken.objects.get(key=token)
            token = token.owner.uuid
        except ObjectDoesNotExist:
            # not a FractalAPIToken so check if it's a DeviceToken
            try:
                token = DeviceToken.objects.get(key=token)
                token = token.device.owner.uuid

            except ObjectDoesNotExist:
                # token was neither, so return None
                return None
        return token


    def post(self, request, *args, **kwargs):
        token = request.data.get('token')
        user_uuid = self.try_get_user_from_token(token)

        if not user_uuid:
            return Response({}, status=status.HTTP_404_NOT_FOUND)

        response = {
            'uuid': user_uuid
        }
        return Response(response, status=status.HTTP_200_OK)


class CheckFractalAPITokenView(APIView):
    '''
    system scope / admin required for this view
    '''
    permission_classes = [IsAdminUser]

    def post(self, request, *args, **kwargs):
        token = request.data.get('token')
        token = FractalAPIToken.objects.get(key=token)

        response = {
            'uuid': token.owner.uuid
        }
        return Response(response, status=status.HTTP_200_OK)


class CheckDeviceTokenView(APIView):
    '''
    system scope / admin required for this view
    '''
    # disables auth for checking token.
    if settings.WARNING_INSECURE_AUTH:
        authentication_classes = []
        permission_classes = []
    else:
        permission_classes = [IsAdminUser]


    def post(self, request, *args, **kwargs):
        token = request.data.get('token')
        token = DeviceToken.objects.get(key=token)

        response = {
            'user': token.device.owner.uuid,
            'device': token.device.uuid
        }
        return Response(response, status=status.HTTP_200_OK)
