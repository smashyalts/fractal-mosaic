from django.apps import AppConfig


class APITokenConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'api_token'
    verbose_name = "API Tokens"