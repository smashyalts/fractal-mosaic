from django.utils.translation import gettext_lazy as _
from django.db import models
from hive_backend.api.models import BaseModel
import binascii
import os


class FractalAPIToken(BaseModel):
    key = models.CharField(_("Key"), max_length=40)
    owner = models.ForeignKey("api.User", on_delete=models.CASCADE)
    capabilities = models.CharField(max_length=255, default='add_device')


    class Meta:
        verbose_name = _("API Token")
        verbose_name_plural = _("API Tokens")

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key()
        return super(FractalAPIToken, self).save(*args, **kwargs)

    def generate_key(self):
        return binascii.hexlify(os.urandom(20)).decode()

    def __str__(self):
        return f'{self.key} (FractalAPIToken) - {self.owner}'

class DeviceToken(BaseModel):
    key = models.CharField(_("Key"), max_length=40)
    device = models.ForeignKey('api.Device', on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Device Token")
        verbose_name_plural = _("Device Tokens")

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key()
        return super(DeviceToken, self).save(*args, **kwargs)

    def generate_key(self):
        return binascii.hexlify(os.urandom(20)).decode()

    def __str__(self):
        return self.key
