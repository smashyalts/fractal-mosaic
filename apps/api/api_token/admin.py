from django.contrib import admin
from api_token.models import FractalAPIToken, DeviceToken

admin.site.register(FractalAPIToken)
admin.site.register(DeviceToken)
