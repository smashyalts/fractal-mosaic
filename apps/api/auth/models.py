from rest_framework_simplejwt.models import TokenUser
from django.utils.functional import cached_property


class FractalTokenUser(TokenUser):

    @cached_property
    def is_active(self):
        return self.token.get('active', '')

    @cached_property
    def get_email(self):
        '''
        Gets email from token
        '''
        return self.token.get('email', '')

    @cached_property
    def is_staff(self):
        '''
        Checks if the token has the system scope.

        Returns:
        - True if the token has the system scope
        - False otherwise
        '''
        token_scopes = self.token.get('scope')
        if 'system' in token_scopes:
            return True
        return False

    @cached_property
    def get_attributes(self):
        attributes = {
            "attributes": {
                "idp": [self.token.get("idp", "")],
                "active": [self.token.get("active", "")],
                "picture": [self.token.get("picture", "")],
                "plan": [self.token.get("plan", "")],
            }
        }

        return attributes
