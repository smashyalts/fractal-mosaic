from django.contrib.auth.models import BaseUserManager


class FractalUserManager(BaseUserManager):
    """
    Simple mananger that allows creation of users with an email instead of a 
    username. We currently do not have usernames so the default manager has 
    to be overridden. Once users are allowed usernames, we can likely use 
    the default Django manager.
    """
    def create_user(self, email, password=None, **extra_fields):

        if email is None:
            raise TypeError('Users must have an email address.')

        user = self.model(email=self.normalize_email(email))
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, email, password, **extra_fields):

        if password is None:
            raise TypeError('Superusers must have a password.')

        user = self.create_user(email, password, **extra_fields)
        user.is_admin = True
        user.save()

        return user
