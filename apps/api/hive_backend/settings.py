from pathlib import Path
from datetime import timedelta
import os

BASE_DIR = Path(__file__).resolve().parent.parent

SECRET_KEY = os.environ['DJANGO_SECRET_KEY']
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

# environment variables
SITE_URL = os.environ.get('HIVE_API_DOMAIN', 'http://localhost:8000')
# disables authentication. Helpful when debugging the hive websocket
WARNING_INSECURE_AUTH = os.environ.get('WARNING_INSECURE_AUTH', False)
RABBITMQ_URL = os.environ.get('RABBITMQ_URL', 'amqp://rabbitmq')
EVENT_SERVICE_URL = os.environ['EVENT_SERVICE_URL']
CONNECTIVITY_URL = os.environ['CONNECTIVITY_URL']
STORAGE_URL = os.environ.get('STORAGE_URL', 'https://storage.fractalnetworks.co')
SYSTEM_JWT = os.environ['SYSTEM_JWT']
DJANGO_ENV = os.environ.get('DJANGO_ENV', 'DEV')
REDIS_HOST = os.environ['REDIS_HOST']
REDIS_PORT = os.environ['REDIS_PORT']
REDIS_USER = os.environ['REDIS_USER']
REDIS_PASSWORD = os.environ['REDIS_PASSWORD']
REDIS_LOCATION = f'redis://{REDIS_USER}:{REDIS_PASSWORD}@{REDIS_HOST}:{REDIS_PORT}/0'

# email related configuration
EMAIL_HOST = os.environ.get('EMAIL_HOST', '')
EMAIL_HOST_USER = os.environ.get('EMAIL_HOST_USER', '')
EMAIL_HOST_PASSWORD = os.environ.get('EMAIL_HOST_PASSWORD', '')
EMAIL_PORT = 587
EMAIL_USE_TLS = True
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
SERVER_EMAIL = 'alerts@fractalnetworks.co'
ADMINS = [('fractal-team', 'alerts@fractalnetworks.co'),]

# Determine which environment the API should run in
# Run the API in debug mode with no external requests
if DJANGO_ENV == 'DEV':
    DEBUG = True
    EXTERNAL_REQUESTS = False
# Run the API in debug mode with external requests
elif DJANGO_ENV == 'TEST':
    DEBUG = True
    EXTERNAL_REQUESTS = True
# Run the API in production mode
elif DJANGO_ENV == 'PROD':
    DEBUG = False
    EXTERNAL_REQUESTS = True

INSTALLED_APPS = [
    'hive_backend.api.apps.ApiConfig',
    'hive_backend.app_catalog.apps.AppCatalogConfig',
    'auth.apps.AuthConfig',
    'api_token.apps.APITokenConfig',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'rest_framework_simplejwt',
    'corsheaders',
    'health_check',
    'health_check.db',
    'health_check.cache',
    'health_check.storage',
    'health_check.contrib.migrations',
    'django_cleanup.apps.CleanupConfig',
    'billing.api',
]

MIDDLEWARE = [
    'django_prometheus.middleware.PrometheusBeforeMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django_prometheus.middleware.PrometheusAfterMiddleware',
]

ROOT_URLCONF = 'hive_backend.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates', 'email_templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

REST_FRAMEWORK = {
    'DEFAULT_PARSER_CLASSES': [
        'rest_framework.parsers.JSONParser',
        'rest_framework.parsers.FormParser',
        'rest_framework.parsers.MultiPartParser'
    ],
}

# enable authentication only if WARNING_INSECURE_AUTH is False
if not WARNING_INSECURE_AUTH:
    REST_FRAMEWORK['DEFAULT_AUTHENTICATION_CLASSES'] = [
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        'auth.authentication.FractalJWTAuthentication',
    ]
    REST_FRAMEWORK['DEFAULT_PERMISSION_CLASSES'] = [
        'rest_framework.permissions.IsAuthenticated'
    ]

WSGI_APPLICATION = 'hive_backend.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django_prometheus.db.backends.postgresql',
        'NAME': 'hive',
        'USER': os.environ.get("HIVE_POSTGRES_USER", "postgres"),
        'PASSWORD': os.environ.get("HIVE_POSTGRES_PASSWORD", "postgres"),
        'HOST': os.environ.get("HIVE_POSTGRES_HOSTNAME", "hive-db"),
        'PORT': 5432,
    }
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.db.DatabaseCache',
        'LOCATION': 'github_stars',
        # 24 hours
        'TIMEOUT': 86400
    },
    'redis': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': REDIS_LOCATION,
    }
}

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '{levelname} {asctime} {module} {process:d} {thread:d} {message}\n',
            'style': '{',
        },
        'simple': {
            'format': '{levelname}::{module}::{asctime} {message}\n',
            'style': '{',
        },
    },
    'filters': {
        'require_production_true': {
            '()': 'hive_backend.logging.RequireProductionTrue',
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        # only send exception emails when in production mode
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_production_true'],
            'class': 'hive_backend.logging.AsyncAdminEmailHandler',
            'include_html': True,
        },
        'celery_default_file': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(BASE_DIR, 'log/celery.log'),
            'backupCount': 10, # keep at most 10 log files
            'maxBytes': 209715200, # 200 MB
            'formatter': 'simple'
        },
        'scheduler_log_file': {
            'class': 'logging.FileHandler',
            'filename': os.path.join(BASE_DIR, 'log/scheduler_log_file.log'),
            'formatter': 'simple'
        },

    },
    'root': {
        'handlers': ['console', 'mail_admins'],
        'level': 'INFO',
    },
    'loggers': {
        'django': {
            'handlers': ['console', 'mail_admins'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'INFO'),
            'propagate': False,
        },
        # default celery logger. This is the output of celery beat, etc.
        'celery': {
            'handlers': ['celery_default_file', 'mail_admins'],
            'level': os.getenv('CELERY_LOG_LEVEL', 'INFO'),
            'propagate': False,
        },
        # all logging related to scheduling
        'scheduler': {
            'handlers': ['scheduler_log_file', 'mail_admins'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'INFO'),
            'propagate': False,
        },
    },
}

CSRF_TRUSTED_ORIGINS = ['http://localhost:8000', 'https://*.fractalnetworks.co']

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_URL = '/static/'

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

# JWT Authentication settings
AUTH_JWKS = os.environ.get('KEYCLOAK_JWKS', None)
SIMPLE_JWT = {
    'ACCESS_TOKEN_LIFETIME': timedelta(minutes=5),
    'REFRESH_TOKEN_LIFETIME': timedelta(days=1),
    'ROTATE_REFRESH_TOKENS': False,
    'BLACKLIST_AFTER_ROTATION': False,
    'UPDATE_LAST_LOGIN': False,

    'ALGORITHM': 'RS256',
    'SIGNING_KEY': SECRET_KEY,
    'VERIFYING_KEY': None,
    'AUDIENCE': None,
    'ISSUER': None,
    'JWK_URL': AUTH_JWKS,
    'LEEWAY': 0,

    'AUTH_HEADER_TYPES': ('Bearer',),
    'AUTH_HEADER_NAME': 'HTTP_AUTHORIZATION',
    'USER_ID_FIELD': 'uuid',
    'USER_ID_CLAIM': 'sub',
    'USER_AUTHENTICATION_RULE': 'rest_framework_simplejwt.authentication.default_user_authentication_rule',

    'AUTH_TOKEN_CLASSES': ('auth.token.KeycloakToken',),
    'TOKEN_USER_CLASS': 'hive_backend.api.models.User',
    'TOKEN_TYPE_CLAIM': 'typ',

    'JTI_CLAIM': 'jti',

    'SLIDING_TOKEN_REFRESH_EXP_CLAIM': 'refresh_exp',
    'SLIDING_TOKEN_LIFETIME': timedelta(minutes=5),
    'SLIDING_TOKEN_REFRESH_LIFETIME': timedelta(days=1),
}

# Custom stuff
ALLOWED_HOSTS = ['*']
CORS_ORIGIN_ALLOW_ALL = True
AUTH_USER_MODEL='api.User'

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
CELERY_BROKER_URL = RABBITMQ_URL
CELERY_RESULT_BACKEND = 'rpc://'
CELERY_IMPORTS = ['hive_backend.tasks']
