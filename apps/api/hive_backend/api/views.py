import logging
from base64 import urlsafe_b64decode
from binascii import Error as binascii_error
from datetime import datetime

import requests
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import get_object_or_404
from django.http import Http404
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.permissions import IsAdminUser
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework import status

from api_token.authentication import FractalTokenAuthentication
from auth.authentication import FractalJWTAuthentication
from hive_backend.api.models import AppInstance, User, Device, AppInstanceConfig, LatestDeviceVersion
from hive_backend.api.device_event_handlers.instances_event import handle_instance_event
from hive_backend.api.device_event_handlers.instance_state_event import update_instance_storage_keys
from hive_backend.api.device_event_handlers.version_event import handle_version_event
from hive_backend.api.device_event_handlers.device_state import handle_device_state_event
from hive_backend.api.filters import IsOwnerFilterBackend, UserFilterBackend
from hive_backend.api.utils import send_device_request
from hive_backend.api.serializers import AppInstanceSerializer, AppInstanceUuidSerializer, UserSerializer, DeviceSerializer, AppInstanceConfigSerializer
from hive_backend.app_catalog.models import App

logger = logging.getLogger('django')
scheduler_logger = logging.getLogger("scheduler")

class UserOwnedBaseViewSet(ModelViewSet):
    filter_backends = [IsOwnerFilterBackend]
    authentication_classes = [FractalJWTAuthentication, FractalTokenAuthentication]

    def perform_create(self, serializer):
        serializer.save(
            owner=self.request.user,
        )


class UserViewSet(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    filter_backends = [UserFilterBackend]
    authentication_classes = [FractalJWTAuthentication, FractalTokenAuthentication]

    def create(self, request, *args, **kwargs):
        '''
        Not allowing user creation through this member endpoint.
        User creating is only allowed by the JWT auth backend at /api/v1/member/join/
        See handle_join action below
        '''
        return Response({}, status=status.HTTP_405_METHOD_NOT_ALLOWED)

    @action(
        detail=False,
        methods=['post'],
        url_path='join',
        url_name='join',
        permission_classes= [],
        authentication_classes= [FractalJWTAuthentication])
    def handle_join(self, request):
        '''
        User Registration
        /api/v1/member/join/
        '''
        serializer= self.get_serializer(data=self.request.user)
        serializer.is_valid()
        if request.user_was_created:
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(
        detail=True,
        methods=['get', 'post'],
        url_path='passphrase',
        url_name='passphrase',)
    def handle_passphrase(self, request, pk=None):
        '''
        Passphrase
        /api/v1/member/<user_uuid>/passphrase/
        '''
        if request.method == 'GET':
            instance = self.get_object()
            serializer = self.get_serializer(instance)
            return Response(serializer.data)

        if request.method == 'POST':
            salted_passphrase = request.data['salted_passphrase']
            salt = request.data['salt']

            instance = self.get_object()
            serializer = self.get_serializer(instance, data={'salt': salt, 'salted_passphrase': salted_passphrase}, partial=True)
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)

            if getattr(instance, '_prefetched_objects_cache', None):
                # If 'prefetch_related' has been applied to a queryset, we need to
                # forcibly invalidate the prefetch cache on the instance.
                instance._prefetched_objects_cache = {}

            return Response(serializer.data)

    @action(
        detail=False,
        methods=['get'],
        url_path='unsubscribe/(?P<encoded_user_info>[^/.]+)',
        url_name='unsubscribe',
        permission_classes= [],
        renderer_classes = [TemplateHTMLRenderer],
        authentication_classes= [])
    def unsubscribe_from_notifications(self, request, encoded_user_info):
        '''
        Unsubscribes a user from notifications.
        /api/v1/member/unsubscribe/<encoded_user_info>/
        '''
        # attempt to decode the encoded user info
        try:
            email, uuid = urlsafe_b64decode(encoded_user_info.encode('utf-8')).decode('utf-8').split(',')
        except (binascii_error, UnicodeDecodeError, UnicodeEncodeError):
            # some error with the encoded_user_info so return 404
            raise Http404

        user = get_object_or_404(User, uuid=uuid, email=email)
        user.notifications_muted = True
        user.save()

        logger.info(f'{email} has unsubscribed from notifications.')

        return Response(template_name='unsubscribed.html', status=status.HTTP_200_OK)


class AppInstanceViewSet(UserOwnedBaseViewSet):
    queryset = AppInstance.objects.all()

    def get_volume_info(self, pubkey: str, app_target_state) -> dict:
        '''
        Fetches volume information using provided pubkey
        '''
        FIVE_MINUTES = 5.0
        HOUR = 60.0

        vol_info = {
            "state": "pending",
            "message": "Initial snapshot pending.",
            "health": "red",
        }

        if not pubkey:
            return vol_info

        # make request to the Storage API to determine app's volume health
        # FIXME: Use one of user's api tokens once storage auth enabled
        headers = {
            "Authorization": "Bearer fb8f31b0-e5ac-4eb9-abbe-194ef82b6f52"
        }
        try:
            resp = requests.get(f'{settings.STORAGE_URL}/api/v1/volume/{pubkey}', headers=headers)
        except requests.RequestException:
            vol_info["state"] = "error"
            vol_info["message"] = f"Error contacting {settings.STORAGE_URL}."
            return vol_info

        if not resp.ok:
            vol_info["state"] = "error"
            vol_info["message"] = str(resp.status_code)
            return vol_info

        volume = resp.json()

        # no replication information available yet
        if not volume.get("updated"):
            return vol_info
        last_replicated_time = datetime.fromtimestamp(volume.get("updated"))
        current_time = datetime.now()
        time_since_last_snapshot = (current_time - last_replicated_time).seconds / 60

        vol_info["message"] = str(last_replicated_time)

        # if app is not supposed to be running, then assume the volume is correctly replicated
        if app_target_state != "running":
            vol_info["state"] = "replicated"
            vol_info["health"] = "green"
            return vol_info

        # app is supposed to be running. Determine volume replication health
        if time_since_last_snapshot <= FIVE_MINUTES:
            vol_info["state"] = "replicated"
            vol_info["health"] = "green"
        elif time_since_last_snapshot > FIVE_MINUTES and time_since_last_snapshot <= HOUR:
            vol_info["state"] = "recent-unreplicated"
            vol_info["health"] = "yellow"
        else:
            # TODO: Send notification to user about unreplicated volume
            vol_info["state"] = "old-unreplicated"
            vol_info["health"] = "red"

        return vol_info

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        vol_info = self.get_volume_info(instance.storage_pubkey, instance.target_state)
        data = serializer.data
        data["volume_info"] = vol_info
        return Response(data)

    def create(self, request, *args, **kwargs):
        request.POST._mutable = True
        if request.data.get('device') == "41e35656-349f-4d69-9e5d-6bbc735cf85e":
            d, _ = Device.objects.get_or_create(owner=self.request.user, name='Fractal Cloud Device')
            request.data["device"] = d.uuid

        if not request.data.get('name', None):
            # default name to App's name
            app = App.objects.get(uuid=request.data['app'])
            logger.info("app name: %s", app.name)
            request.data['name'] =  app.name

        request.POST._mutable = False
        return super().create(request, *args, **kwargs)

    def partial_update(self, request, *args, **kwargs):
        device_uuid = request.data.get('device')
        app_instance = self.get_object()
        request.POST._mutable = True
        # special case for fractal cloud device
        if device_uuid == "41e35656-349f-4d69-9e5d-6bbc735cf85e":
            d, _ = Device.objects.get_or_create(owner=self.request.user, name='Fractal Cloud Device')
            request.data["device"] = d.uuid
            device_uuid = d.uuid

        if device_uuid:
            # user wants to reschedule to a different device
            reschedule_to_device = get_object_or_404(Device, uuid=device_uuid)

            # if there wasn't a device previously attached, then we do not need
            # to do any special rescheduling logic.
            if app_instance.device == None:
                pass

            elif str(app_instance.device.uuid) != device_uuid:
                # we dont update device until successful reschedule so instance_event doesn't leapfrog synchronus reschedule
                del request.data['device']
                request.data['reschedule_to_device'] = device_uuid

        request.POST._mutable = False
        return super().partial_update(request, *args, **kwargs)

    def get_serializer_class(self, *args, **kwargs):
        if self.action == 'retrieve' or self.action == 'partial_update' or self.action == 'create':
            return AppInstanceSerializer
        return AppInstanceUuidSerializer

    def get_queryset(self):
        '''
        Only gets objects that belong to the current user.
        If the request has an installed query param,
        filters the queryset by whether app is installed
        or not.
        '''
        queryset = super().get_queryset()
        installed_detail = self.request.query_params.get('installed', '').lower()

        if installed_detail == 'true':
            return queryset.filter(state = 'installed')
        elif installed_detail == 'false':
            return queryset.filter(state = 'not-installed')

        return queryset

    @action(detail=True, methods=['get'], url_path='app-name', url_name='app-name-check')
    def app_name_check(self, request, pk=None):
        try:
            app_instance = AppInstance.objects.get(owner=self.request.user, state='installed', app__uuid=pk)
            serializer = AppInstanceSerializer(app_instance)
            return Response(serializer.data)
        except AppInstance.DoesNotExist:
            return Response(data=None)

    @action(
        detail=True,
        methods=['post'],
        url_path='element',
        url_name='create_element_user',)
    def create_element_user(self, request, pk=None):
        username = self.request.data.get('username')
        password = self.request.data.get('password')
        app_uuid = self.request.data.get('appinstance')
        admin = self.request.data.get('admin', False)
        # Convert typescript string "true" to boolean
        if admin == 'true':
            admin = True

        app_instance = AppInstance.objects.get(owner=self.request.user, uuid=app_uuid)
        create_element_user(username, password, app_instance.device, app_instance, admin)
        return Response({}, status=status.HTTP_200_OK)


class DeviceViewSet(UserOwnedBaseViewSet):
    '''
    Only API Tokens with the device_add capability are allowed to
    authenticate with this viewset. JWTs are accepted as well.
    '''
    # UI hardcoes the Device name of Fractal Cloud Device so we filter that out in this queryset
    queryset = Device.objects.exclude(name='Fractal Cloud Device')
    serializer_class = DeviceSerializer
    authentication_classes = [FractalJWTAuthentication, FractalTokenAuthentication]

    def create(self, request, *args, **kwargs):
        '''
        Adds a Device to a user. Returns only DeviceToken
        '''
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)

        # create a DeviceToken for the Device
        token = serializer.instance.devicetoken_set.create()

        res = {
            "token": token.key,
        }

        return Response(res, status=status.HTTP_201_CREATED)

    @action(
        detail=False,
        methods=['post'],
        url_path='event',
        url_name='device-event',
        permission_classes= [] if settings.WARNING_INSECURE_AUTH else [IsAdminUser],
        authentication_classes= [] if settings.WARNING_INSECURE_AUTH else [FractalJWTAuthentication]
    )
    def device_event(self, request, *args, **kwargs):
        '''
        system scope / admin required

        Handles various events received from the Hive Device Service.
        '''
        try:
            device = Device.objects.get(uuid=request.data.get('device'))
            owner = User.objects.get(uuid=request.data.get('account'))
        except ObjectDoesNotExist:
            return Response({}, status=status.HTTP_200_OK)

        event_type = request.data.get('type', None)

        logger.debug(f'Request Data: {request.data}\n')

        if event_type == 'device-alive':
            device.handle_ping_event()
            device.save()

        # ensures currently running apps are synchronized
        elif event_type == 'instances':
            handle_instance_event(owner, device, request.data)

        # handles state change for an app
        elif event_type == 'instance-state':
            app_instance_uuid = request.data.get("instance")
            update_instance_storage_keys(owner, device, app_instance_uuid, request.data)

        elif event_type == 'device-version':
            handle_version_event(device, request.data)

        elif event_type == 'device-state':
            handle_device_state_event(owner, device, request.data)

        return Response({}, status=status.HTTP_200_OK)

    @action(
        detail=False,
        methods=['post'],
        url_path='version',
        url_name='update-device-version',
        permission_classes= [] if settings.WARNING_INSECURE_AUTH else [IsAdminUser],
        authentication_classes= [] if settings.WARNING_INSECURE_AUTH else [FractalJWTAuthentication])
    def update_device_version(self, request, *args, **kwargs):
        '''
        system scope / admin required

        Handles updating the device version.
        '''
        new_version = request.data.get("version")
        latest_device_version = LatestDeviceVersion.objects.get()
        latest_device_version.version = new_version
        latest_device_version.save()

        return Response({"version": latest_device_version.version}, status=status.HTTP_200_OK)


class AppInstanceConfigViewSet(UserOwnedBaseViewSet):
    serializer_class = AppInstanceConfigSerializer

    def create(self, request, *args, **kwargs):
        app_instance = AppInstance.objects.get(uuid=self.request.data.get('appinstance'))
        request.POST._mutable = True

        if app_instance is not None:
            if app_instance.app.name.lower() == 'element':
                device = app_instance.device
                username = request.data['data']['username']
                password = request.data['data']['password']
                del request.data['data']['password']
                create_element_user(username, password, device, app_instance, admin = True)
            if app_instance.app.name.lower() == 'photoprism':
                device = app_instance.device
                password = request.data['data']['password']
                username = request.data['data']['username']
                del request.data['data']['password']
                del request.data['data']['username']
                create_photoprism_password(password, username, device, app_instance, admin = True)

        request.POST._mutable = False
        response = super().create(request, *args, **kwargs)
        app_config = AppInstanceConfig.objects.get(uuid=str(response.data['uuid']))
        app_instance.config=app_config
        app_instance.save()
        return response


def create_element_user(username: str, password: str, device: Device, app_instance: AppInstance, admin=False):

    create_user_request = {
        "args": [username, password],
        "action": "create_admin_user" if admin else 'create_user',
        "instance": str(app_instance.uuid),
        "container": "synapse",
        "link_domain": app_instance.links['default']['domain'],
    }

    return send_device_request(device, "AppInstanceCommand", create_user_request)

def create_photoprism_password(password: str, username: str, device: Device, app_instance: AppInstance, admin=False):

    create_user_request = {
        "args": [password, username],
        "action": "create_admin_password",
        "instance": str(app_instance.uuid),
        "container": "photoprism",
        "link_domain": app_instance.links['default']['domain'],
    }

    return send_device_request(device, "AppInstanceCommand", create_user_request)
