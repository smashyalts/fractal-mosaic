from django.core.exceptions import ObjectDoesNotExist
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver

from hive_backend.api.utils import get_notifier
from hive_backend.api.models import Device, User, AppInstance

'''
All notification handlers live here.
'''


@receiver(pre_save, sender=Device)
def notify_device_health_change(sender, instance, *args, **kwargs):
    '''
    Handles sending a notification to the user when their Device's
    health changes.
    '''
    device_owner = instance.owner
    device_name = instance.name

    # attempt to get notifier for user
    notifier = get_notifier(device_owner)

    # if no notifier returned, then dont send a notification
    if not notifier:
        return

    # determine if a new Device was created
    try:
        previous = sender.objects.get(uuid=instance.uuid)
    except ObjectDoesNotExist:
        notifier.new_device_added(device_name)
        return

    # send notification to user if device's health has changed.
    if previous.health != instance.health:
        notifier.device_health_change(device_name, instance.health)


@receiver(post_save, sender=User)
def send_welcome_message(sender, instance, created, raw, *args, **kwrags):
    '''
    Sends a welcome message to a new user using their display name if set,
    or email otherwise.
    '''
    if not created or raw:
        return

    # use user's display_name if one is set
    name = instance.display_name if instance.display_name else instance.email

    # attempt to get notifier for user
    notifier = get_notifier(instance)

    # if no notifier returned, then dont send a notification
    if not notifier:
        return

    # send welcome notification
    notifier.welcome(name)

@receiver(pre_save, sender=AppInstance)
def send_app_state_change(sender, instance, *args, **kwargs):
    '''
    Sends a notification to the user when their App Instance's state changes.
    '''
    app_owner = instance.owner
    app_name = instance.name

    # dont try to send an email if the instance does not have a link
    uri = instance.links.get("default").get("domain")
    if not uri:
        return

    link_domain = f'https://{uri}'
    notifier = get_notifier(app_owner)

    # if no notifier returned, then dont send a notification
    if not notifier:
        return

    try:
        previous = sender.objects.get(uuid=instance.uuid)
    except ObjectDoesNotExist:
        notifier.app_installation_change(app_name, instance.state, link_domain)
        return

    # notify the user if their app's target state has changed.
    if previous.target_state != instance.target_state and previous.state == instance.state:
        notifier.app_target_state_change(app_name, instance.target_state, link_domain)

    # notify the user if their app's installation state has changed
    if previous.state != instance.state:
        notifier.app_installation_change(app_name, instance.state, link_domain)
