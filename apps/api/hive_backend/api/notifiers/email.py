from base64 import urlsafe_b64encode

from django.conf import settings
from django.template.loader import render_to_string

from hive_backend.api.models import User
from hive_backend.api.notifiers import BaseNotifier
from hive_backend.tasks.notifications import send_email


class EmailNotifier(BaseNotifier):
    '''
    Notifier for emails
    '''

    def __init__(self, recipient: str):
        '''
        Set up sender / recipient for email sending.
        '''
        self.sender = "notifications@fractalnetworks.co" # FIXME
        self.recipient = [recipient]

    def _build_unique_unsubscribe_url(self):
        '''
        Builds a unique unsubscribe url using the current user's email and uuid.

        email and uuid are encoded in a string thats formatted like so:
        "email,uuid"
        '''
        email = self.recipient[0]
        uuid = User.objects.get(email=email).uuid
        unique = f'{email},{uuid}'.encode('utf-8')
        unique = urlsafe_b64encode(unique).decode('utf-8')
        unique_url = f'{settings.SITE_URL}/api/v1/member/unsubscribe/{unique}/'
        return unique_url

    def _notify(self, template_prefix: str, context: dict):
        '''
        Sends an email with the provided message contents
        '''
        context["unsubscribe_url"] = self._build_unique_unsubscribe_url()

        # extract subject & body from message contents passed in
        subject_txt = render_to_string(f'{template_prefix}_subject.txt', context)
        body_txt = render_to_string(f'{template_prefix}_body.txt', context)
        body_html = render_to_string(f'{template_prefix}_body.html', context)

        # only send email if Django in PROD / TEST
        if settings.DJANGO_ENV == "PROD" or settings.DJANGO_ENV == "TEST":
            send_email.apply_async(args=(self.recipient, self.sender, subject_txt, body_txt, body_html), ignore_result=True)
        else:
            print(f"Django running in {settings.DJANGO_ENV}. Not sending email. Contents:")
            print(f"Recipient: {self.recipient}")
            print(f"Sender: {self.sender}")
            print(f"Subject: {subject_txt}")
            print(f"Body: {body_txt}\n")
