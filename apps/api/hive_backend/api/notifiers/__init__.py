

class BaseNotifier():
    '''
    A generic notifier that is intended to be subclassed to extend any
    necessary functionality for a given notifier.
    '''

    def _notify(self, template_prefix: str, message_context: dict):
        '''
        Sends a message using the configured notifier. Functionality
        should be added by the inheritor.
        '''
        raise NotImplementedError()

    def welcome(self, name: str):
        '''
        Welcome the User to Mosaic.
        '''
        template_prefix = 'user/welcome'
        context = {
            "name": name,
        }

        # fire off notification
        self._notify(template_prefix, context)

    def launch_device_reminder(self, name: str):
        '''
        Remind the User to launch a Device.
        '''
        template_prefix = 'user/launch_device'
        context = {
            "name": name,
        }

        # fire off notification
        self._notify(template_prefix, context)

    def device_health_change(self, device_name: str, health: str):
        '''
        Notify the User that their Device's health has changed.
        '''
        template_prefix = 'device/health_change'
        context = {
            "device_name": device_name,
            "health": health,
        }

        # fire off notification
        self._notify(template_prefix, context)

    def new_device_added(self, device_name: str):
        '''
        Notify the User that they have added a new Device.
        '''
        template_prefix = 'device/newly_added'
        context = {
            "device_name": device_name,
        }

        # fire off notification
        self._notify(template_prefix, context)

    def app_target_state_change(self, app_name: str, target_state: str, link_domain: str):
        '''
        Notify the User that their App Instance's target state has changed.
        '''
        template_prefix = 'app/state_change'
        context = {
            "app_name": app_name,
            "target_state": target_state,
            "link_domain": link_domain,
        }

        # fire off notification
        self._notify(template_prefix, context)

    def app_installation_change(self, app_name: str, installation_state: str, link_domain: str):
        '''
        Notify the User that their App Instance's installation state has changed.
        '''
        template_prefix = 'app/installation_change'
        context = {
            "app_name": app_name,
            "installation_state": installation_state,
            "link_domain": link_domain,
        }

        # fire off notification
        self._notify(template_prefix, context)

    def low_disk_space_warning(self, device_name: str):
        '''
        Notify the User that their App Instance's installation state has changed.
        '''
        template_prefix = 'device/low_disk_space'
        context = {
            "device_name": device_name,
        }

        # fire off notification
        self._notify(template_prefix, context)
