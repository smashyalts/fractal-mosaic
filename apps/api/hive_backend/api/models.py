from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.core.cache import caches
from django.utils import timezone
from auth.manager import FractalUserManager
from hive_backend.celery_config import MAX_PING_DURATION
from django_prometheus.models import ExportModelOperationsMixin
import uuid
import logging

logger = logging.getLogger('django')
scheduler_logger = logging.getLogger('scheduler')
cache = caches['redis']

def upload_path(instance, filename):
    return '/'.join(['profile_images', str(instance.uuid), filename])


class BaseModel(models.Model):
    uuid = models.UUIDField(
         primary_key = True,
         default = uuid.uuid4,
         editable = False)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    deleted = models.BooleanField(default=False)


    class Meta:
        abstract = True


class OwnedBaseModel(BaseModel):
    owner = models.ForeignKey("User", on_delete=models.CASCADE)


    class Meta:
        abstract = True


class User(ExportModelOperationsMixin('user'), BaseModel, AbstractBaseUser):
    app_catalogs = models.ManyToManyField("app_catalog.AppCatalog", blank=True)
    email = models.EmailField(unique=True)
    is_admin = models.BooleanField(default=False)
    profile_image = models.FileField(blank=True, null=True, upload_to=upload_path)
    display_name = models.CharField(max_length=255, blank=True, null=True)
    salt = models.CharField(max_length=255, blank=True, null=True)
    salted_passphrase = models.CharField(max_length=255, blank=True, null=True)
    notifications_muted = models.BooleanField(default=False)

    USERNAME_FIELD = 'email'

    objects = FractalUserManager()

    def __str__(self):
        return f'{self.email} - {self.uuid} (User)'

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        return self.is_admin


class AppInstance(ExportModelOperationsMixin('appinstance'),OwnedBaseModel):
    STATES = (
        ('stopped', 'stopped'),
        ('paused', 'paused'),
        ('running', 'running'),
    )

    name = models.CharField(max_length=255, blank=True, null=True)
    app = models.ForeignKey('app_catalog.App', on_delete=models.CASCADE, null=True, blank=True)
    health = models.CharField(max_length=255, default="red", null=True)
    image = models.FileField(blank=True, null=True, upload_to='./app_instance')
    icon = models.TextField(max_length=255, null=True, blank=True)
    state = models.CharField(
        choices=(
            ('installed', 'installed'),
            ('not-installed', 'not-installed')
        ),
        default='not-installed',
        max_length=255
    )
    target_state = models.CharField(
        choices=STATES,
        max_length=255,
        blank=True,
        null=True
    )
    current_state = models.CharField(
        choices=STATES + (('error', 'error'), ('starting', 'starting'),),
        editable=True,
        max_length=255,
        blank=True,
        null=True
    )
    config = models.OneToOneField('AppInstanceConfig', blank=True, null=True, on_delete=models.SET_NULL)
    device = models.ForeignKey('Device',blank=True, null=True, on_delete=models.DO_NOTHING)
    reschedule_to_device = models.ForeignKey('Device', related_name='reschedule_set', blank=True, null=True, on_delete=models.DO_NOTHING)
    last_state_change_timestamp = models.DateTimeField(blank=True, null=True)
    links = models.JSONField(default=dict, blank=True, null=True)
    storage_apikeys = models.JSONField(default=dict, blank=True, null=True)
    encrypted_storage_apikeys = models.TextField(blank=True, null=True)
    storage_pubkey = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        # display (app_name - link domain) if one exists otherwise just (app_name - uuid)
        if self.links and self.links.get('default'):
            return f'{self.app.name}({self.uuid}) - {self.links["default"]["domain"]} (AppInstance) - {self.owner.email}'
        return f'{self.app.name} - {self.uuid} (AppInstance) - {self.owner}'

    def save_related(self):
        '''
        Saves the related device and itself
        '''
        self.device.save()
        self.save()

    def set_current_state(self, state: str):
        '''
        Sets the current state to passed in state and updates
        last_state_change_timestamp
        '''
        # received state is not running, therefore app health is currently red
        if state != "running":
            self.health = "red"
        self.current_state = state
        self.last_state_change_timestamp = timezone.now()

    def handle_up_event(self, *args, **kwargs):
        '''
        Updates the AppInstance's current state to "running"
        '''
        self.set_current_state("running")
        self.health = "green"


class AppInstanceConfig(OwnedBaseModel):
    cpu_cores = models.IntegerField(blank=True, null=True)
    storage_limit = models.CharField(max_length=255, blank=True, null=True)
    data=models.JSONField(default=dict)


class DNSConfig(OwnedBaseModel):
    fqdn = models.CharField(max_length=255)
    data = models.CharField(max_length=255)
    record_type = models.CharField(max_length=255)
    app_instance = models.ForeignKey(AppInstance, on_delete=models.CASCADE)


class AppInstanceVolume(OwnedBaseModel):
    pub_key = models.CharField(max_length=255)
    instance = models.ForeignKey(AppInstance, on_delete=models.CASCADE)


class Device(ExportModelOperationsMixin('device'),OwnedBaseModel):
    name = models.CharField(max_length=255, blank=True)
    health = models.CharField(max_length=255, default="red", null=True)
    last_seen_timestamp = models.DateTimeField(null=True)
    version = models.CharField(max_length=255, default="1.0.0", null=True)

    def __str__(self):
        return f'{self.name} - {self.uuid} (Device) - {self.owner}'

    def handle_ping_event(self, *args, **kwargs):
        '''
        Updates the device's last_seen_timestamp

        TODO:
            Keep track of how long it's been since last_seen_timestamp

            last_seen 0 - 20s = green
            last_seen between 20 - 50s = yellow
            else red
        '''
        self.health = "green"
        # store this device's uuid in the cache.
        cache.set(f"seen-{self.uuid}", str(self.uuid), timeout=MAX_PING_DURATION)


class LatestDeviceVersion(BaseModel):
    '''
    Stores the latest version of the Hive Device
    '''
    version = models.CharField(max_length=255, default="1.0.0", null=True)

    def __str__(self):
        return f'{self.version} (LatestDeviceVersion)'
