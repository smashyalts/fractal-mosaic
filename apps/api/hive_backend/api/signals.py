from django.db.models.signals import pre_save, pre_delete
from django.dispatch import receiver
from django.core.exceptions import ObjectDoesNotExist
from hive_backend.api.serializers import AppInstanceContextSerializer
from hive_backend.api.models import AppInstance, Device
from hive_backend.api.utils import new_app_installation
from hive_backend.tasks.executor import command_executor
from hive_backend.scheduler import AppInstanceCurrentState, AppInstanceDesiredState, AppInstanceState
from hive_backend.scheduler.matriarch import schedule
import logging

logger = logging.getLogger('django')
scheduler_logger = logging.getLogger("scheduler")

@receiver(pre_save, sender=AppInstance)
def handle_app_state_change(sender, instance, *args, **kwargs):
    '''
    TODO: Cache current state from instances event into Redis.
          use pre_save signal to check if device has changed
          by querying Redis for current state
    '''
    # dont run for fixtures
    if kwargs.get("raw", False):
        return False

    created = False
    try:
        previous = sender.objects.get(uuid=instance.uuid)
    except ObjectDoesNotExist:
        created = True

    # determine if new instance created
    if created:
        logger.info("Created Instance: %s", instance)

        links = new_app_installation(instance)

        # failed to get links, dont start the app
        if not links:
            print(f"Failed to generate a link for App Instance: {instance}")
            instance.target_state = "stopped"
            instance.links = {
                "default": {
                    "domain": None,
                    "token": None,
                }
            }
            return

        # got links back so try to start the app
        instance.links = links

        # send start event to device
        context = AppInstanceContextSerializer(instance).data
        current_state = AppInstanceCurrentState(
            AppInstanceState.stopped,
            instance.uuid,
            instance.device.uuid)
        desired_state = AppInstanceDesiredState(
            AppInstanceState.running,
            instance.uuid,
            instance.device.uuid)
        device_commands = schedule(current_state, desired_state, context)
        command_executor.apply_async(args=(device_commands,), ignore_result=True)

    # existing app instance modified
    else:
        # user reinstalled the app
        if previous.state == "not-installed" and instance.state == "installed":
            # ensure storage keys cleared for freshly installed app
            instance.storage_apikeys = {}
            instance.encrypted_storage_apikeys = None
            instance.storage_pubkey = None
            instance.target_state = "running"
            instance.health = "yellow"

            # generate a new Fractal Link
            links = new_app_installation(instance)

            # failed to get links, dont start the app
            if not links:
                print(f"Failed to generate a link for App Instance: {instance}")
                instance.target_state = "stopped"
                instance.links = {
                    "default": {
                        "domain": None,
                        "token": None,
                    }
                }
                return

            # got links back so try to start the app
            instance.links = links

        # user uninstalled the app
        elif previous.state == "installed" and instance.state == "not-installed":
            instance.target_state = "stopped"
            instance.health = "yellow"

        # user chose to stop app. schedule stop command to device
        if previous.target_state != "stopped" and instance.target_state == "stopped":
            context = AppInstanceContextSerializer(instance).data
            instance.health = "yellow"

            # determine the current state of the app.
            # the user has switched devices, so the app is running on the previous device.
            if previous.device != instance.device or instance.device == None:
                current_state = AppInstanceCurrentState(
                    AppInstanceState.running,
                    instance.uuid,
                    previous.device.uuid)

            # the user hasn't switched devices, so app is running on the current device
            else:
                current_state = AppInstanceCurrentState(
                    AppInstanceState.running,
                    instance.uuid,
                    instance.device.uuid)

            # determine if the current device was removed. If it was, then the previous device is the device
            # that we must send a stop message to.
            if instance.device == None:
                desired_state = AppInstanceDesiredState(
                    AppInstanceState.stopped,
                    instance.uuid,
                    previous.device.uuid)

            # The current device wasn't removed, so send a stop message to the current device.
            else:
                desired_state = AppInstanceDesiredState(
                    AppInstanceState.stopped,
                    instance.uuid,
                    instance.device.uuid)

            device_commands = schedule(current_state, desired_state, context)
            command_executor.apply_async(args=(device_commands,), ignore_result=True)

        # user chose to run app, schedule start command to device
        elif previous.target_state != "running" and instance.target_state == "running":
            context = AppInstanceContextSerializer(instance).data
            instance.health = "yellow"

            # determine if device has changed
            if previous.device and previous.device != instance.device:
                current_state = AppInstanceCurrentState(
                    AppInstanceState.stopped,
                    instance.uuid,
                    previous.device.uuid)
            else:
                current_state = AppInstanceCurrentState(
                    AppInstanceState.stopped,
                    instance.uuid,
                    instance.device.uuid)

            desired_state = AppInstanceDesiredState(
                AppInstanceState.running,
                instance.uuid,
                instance.device.uuid)
            device_commands = schedule(current_state, desired_state, context)
            command_executor.apply_async(args=(device_commands,), ignore_result=True)

@receiver(pre_delete, sender=Device)
def handle_device_cascade(sender, instance, *args, **kwargs):
    '''
    Unset all AppInstance's device property where the device is the one being deleted
    '''
    instances = AppInstance.objects.filter(device=instance)
    for instance in instances:
        instance.device = None
        instance.target_state = 'stopped'
        instance.save()
