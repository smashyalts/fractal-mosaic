import logging
from hive_backend.api.models import AppInstance

logger = logging.getLogger(__name__)

def update_instance_storage_keys(owner, device, app_instance_uuid, event):
    '''
    Looks up an App Instance and updates its storage keys.
    '''
    logger.debug("update_instance_storage_keys called")

    # attempt to fetch AppInstance. Don't do anything if it doesnt exist
    try:
        app_instance = AppInstance.objects.get(
            owner=owner,
            device=device,
            uuid=app_instance_uuid)
    except AppInstance.DoesNotExist:
        logger.warning("App Instance (%s) does not exist for user (%s)", app_instance_uuid, owner.uuid)
        return

    encrypted_storage_apikeys = event.get("encrypted_storage_apikeys")
    storage_pubkey = event.get("storage_pubkey")
    # was bug here where device had been rescheduled to new device but instance events from old device
    # still being processed because the query above was doing a filter instead of a get (wasn't raising AppInstance.DoesNotExist)
    if not app_instance.encrypted_storage_apikeys:
        app_instance.encrypted_storage_apikeys=encrypted_storage_apikeys
        app_instance.save()
    if not app_instance.storage_pubkey:
        app_instance.storage_pubkey=storage_pubkey
        app_instance.save()
