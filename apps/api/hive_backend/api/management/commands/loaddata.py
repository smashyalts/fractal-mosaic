import json
import os
from django.core.management.commands import loaddata
from hive_backend.api.models import LatestDeviceVersion

# https://stackoverflow.com/a/68894033
def should_add_record(record):
    if record['model'] != 'api.latestdeviceversion':
        return True

    # dont add LatestDeviceVersion data if an entry already exists
    return not LatestDeviceVersion.objects.filter(
        uuid=record['pk'],
    ).exists()


class Command(loaddata.Command):
    def handle(self, *args, **options):
        '''
        Dont load data that already exists in the DB
        '''
        args = list(args)

        for file_name in args:
            file_dir_and_name, file_ext = os.path.splitext(file_name)

            # run regular loaddata if yaml file
            if file_ext in {'.yaml', '.yml'}:
                super().handle(*args, **options)

            # assuming file is JSON.
            else:
                # read the original JSON file
                with open(file_name) as json_file:
                    json_list = json.load(json_file)

                # filter out records that already exists
                json_list_filtered = list(filter(should_add_record, json_list))
                if not json_list_filtered:
                    return

                # write the updated JSON file
                file_name_temp = f"{file_dir_and_name}_temp{file_ext}"
                with open(file_name_temp, 'w') as json_file_temp:
                    json.dump(json_list_filtered, json_file_temp)

                # pass the request to the actual loaddata (parent functionality)
                super().handle(file_name_temp, **options)

                # remove tmp fixture file
                os.remove(file_name_temp)
