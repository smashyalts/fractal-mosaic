from django.apps import AppConfig


class ApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'hive_backend.api'
    verbose_name = "Hive Backend API"

    def ready(self):
        import hive_backend.api.signals
        import hive_backend.api.notifiers.signals
