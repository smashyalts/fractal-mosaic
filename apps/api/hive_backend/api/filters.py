from rest_framework.filters import BaseFilterBackend


class UserFilterBackend(BaseFilterBackend):
    """
    Filter that only allows user to see their own user object.

    This filter is specifically for the UserViewSet
    """

    def filter_queryset(self, request, queryset, view):
        return queryset.filter(uuid=request.user.uuid)


class IsOwnerFilterBackend(BaseFilterBackend):
    """
    Filter that only allows users to see their own objects.

    Only allows admins to view entire queryset.
    """

    def filter_queryset(self, request, queryset, view):
        return queryset.filter(owner=request.user)
