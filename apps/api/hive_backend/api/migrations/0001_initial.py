# Generated by Django 4.0.4 on 2022-08-19 21:58

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import hive_backend.api.models
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('app_catalog', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('deleted', models.BooleanField(default=False)),
                ('email', models.EmailField(max_length=254, unique=True)),
                ('is_admin', models.BooleanField(default=False)),
                ('profile_image', models.FileField(blank=True, null=True, upload_to=hive_backend.api.models.upload_path)),
                ('display_name', models.CharField(blank=True, max_length=255, null=True)),
                ('app_catalogs', models.ManyToManyField(blank=True, to='app_catalog.appcatalog')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='AppInstance',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('deleted', models.BooleanField(default=False)),
                ('name', models.CharField(blank=True, max_length=255, null=True)),
                ('health', models.CharField(default='red', max_length=255, null=True)),
                ('image', models.FileField(blank=True, null=True, upload_to='./app_instance')),
                ('icon', models.TextField(blank=True, max_length=255, null=True)),
                ('state', models.CharField(choices=[('installed', 'installed'), ('not-installed', 'not-installed')], default='not-installed', max_length=255)),
                ('target_state', models.CharField(blank=True, choices=[('stopped', 'stopped'), ('paused', 'paused'), ('running', 'running'), ('stopping', 'stopping')], max_length=255, null=True)),
                ('current_state', models.CharField(blank=True, choices=[('stopped', 'stopped'), ('paused', 'paused'), ('running', 'running'), ('error', 'error'), ('starting', 'starting')], max_length=255, null=True)),
                ('last_state_change_timestamp', models.DateTimeField(blank=True, null=True)),
                ('links', models.JSONField(blank=True, default=dict, null=True)),
                ('storage_apikeys', models.JSONField(blank=True, default=dict, null=True)),
                ('app', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='app_catalog.app')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='DNSConfig',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('deleted', models.BooleanField(default=False)),
                ('fqdn', models.CharField(max_length=255)),
                ('data', models.CharField(max_length=255)),
                ('record_type', models.CharField(max_length=255)),
                ('app_instance', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.appinstance')),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Device',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('deleted', models.BooleanField(default=False)),
                ('name', models.CharField(blank=True, max_length=255)),
                ('health', models.CharField(default='red', max_length=255, null=True)),
                ('last_seen_timestamp', models.DateTimeField(null=True)),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='AppInstanceVolume',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('deleted', models.BooleanField(default=False)),
                ('pub_key', models.CharField(max_length=255)),
                ('instance', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.appinstance')),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='AppInstanceConfig',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('deleted', models.BooleanField(default=False)),
                ('cpu_cores', models.IntegerField()),
                ('storage_limit', models.CharField(max_length=255)),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='appinstance',
            name='config',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='api.appinstanceconfig'),
        ),
        migrations.AddField(
            model_name='appinstance',
            name='device',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='api.device'),
        ),
        migrations.AddField(
            model_name='appinstance',
            name='owner',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
