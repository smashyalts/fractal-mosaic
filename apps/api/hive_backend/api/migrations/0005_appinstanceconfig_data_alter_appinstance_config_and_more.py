# Generated by Django 4.0.4 on 2022-09-12 20:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0004_alter_appinstance_target_state'),
    ]

    operations = [
        migrations.AddField(
            model_name='appinstanceconfig',
            name='data',
            field=models.JSONField(default=dict),
        ),
        migrations.AlterField(
            model_name='appinstance',
            name='config',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='api.appinstanceconfig'),
        ),
        migrations.AlterField(
            model_name='appinstanceconfig',
            name='cpu_cores',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='appinstanceconfig',
            name='storage_limit',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
