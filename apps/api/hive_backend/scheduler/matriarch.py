from hive_backend.scheduler import AppInstanceCurrentState, AppInstanceDesiredState, SchedulerAction, DeviceCommand
from hive_backend.scheduler.serializers import DeviceCommandSerializer
import logging

logger = logging.getLogger('django')

def schedule(current_state: AppInstanceCurrentState, desired_state: AppInstanceDesiredState, context: dict) -> list[dict]:
    '''
    Reconciles current_state with desired_state.
    '''
    logger.debug("Got current state: %s", current_state)
    logger.debug("Got desired state: %s", desired_state)

    # list of serialized DeviceCommands
    commands = []

    # Determine if device has been changed
    if current_state.device_id != desired_state.device_id:
        sequential_commands = []
        # build stop device command for current state device
        command = DeviceCommand(
            command=SchedulerAction.stop,
            state=AppInstanceDesiredState(*current_state),
            payload=context,
            device=current_state.device_id,
            account=context["account"])
        sequential_commands.append(command)

        # build start device command for desired state device
        command = DeviceCommand(
            command=SchedulerAction.start,
            state=desired_state,
            payload=context,
            device=desired_state.device_id,
            account=context["account"])
        sequential_commands.append(command)

        # append sequential commands to the commands list
        commands.append(DeviceCommandSerializer(sequential_commands, many=True).data)

    # determine if state has changed
    if current_state.state != desired_state.state:
        # build start device command
        if desired_state.state.value == "running":
            command = DeviceCommand(
                command=SchedulerAction.start ,
                state=desired_state,
                payload=context,
                device=desired_state.device_id,
                account=context["account"])
            commands.append(DeviceCommandSerializer(command).data)

        elif desired_state.state.value == "stopped":
            # build stop device command
            command = DeviceCommand(
                command=SchedulerAction.stop,
                state=desired_state,
                payload=context,
                device=desired_state.device_id,
                account=context["account"])
            commands.append(DeviceCommandSerializer(command).data)

    return commands