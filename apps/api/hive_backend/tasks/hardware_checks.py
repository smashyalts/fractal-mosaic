import hive_backend.tasks.executor as executor
from hive_backend.api.models import Device
from hive_backend.celery_config import app

'''
Tasks related to sending Device Hardware Checks.
'''

@app.task
def disk_space_check():
    '''
    Weekly task that sends a check_disk_space command to healthy devices to check if
    device is low on disk space.
    '''
    # get all healthy devices
    healthy_devices = Device.objects.filter(health="green")

    for device in healthy_devices:
        cmd = [{
            "account": device.owner.uuid,
            "device": device.uuid,
            "command": "DeviceCommand",
            "payload": {
                "action": 'check_disk_space'
            }
        }]
        executor.command_executor.apply_async(args=([cmd],), ignore_result=True)
