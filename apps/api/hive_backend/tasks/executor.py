import logging
import os
import time

import requests
from django.core.cache import caches

from hive_backend.api.models import AppInstance, Device, User
from hive_backend.celery_config import app
from django.core.exceptions import ObjectDoesNotExist

django_logger = logging.getLogger('django')
scheduler_logger = logging.getLogger("scheduler")
cache = caches['redis']

@app.task
def mark_devices_unhealthy():
    '''
    Monitors and marks all devices that have not pinged
    in the last MAX_PING_DURATION seconds.

    TODO: handle the yellow health case.
    '''
    from hive_backend.api.models import Device

    # query cache for all devices that are currently in the cache
    keys = cache.keys('seen-*')
    healthy_devices = cache.get_many(keys)

    # get all unhealthy devices that aren't already unhealthy
    unhealthy_devices = Device.objects.exclude(health="red").exclude(
        uuid__in=healthy_devices.values()
    )

    #unhealthy_devices.update(health="red")
    # call save to update device instead of using the above update
    # so we can send events in the post_save signal
    for device in unhealthy_devices:
        device.health = 'red'
        device.save()

        # update all app instances health on this device to red
        AppInstance.objects.filter(device=device).update(health="red")

    return True

@app.task
def send_device_request(current_command, remaining_commands=None):
    '''
    Sends commands to a device
    '''
    # ensure that the given device / user exist
    try:
        _ = Device.objects.get(uuid=current_command.get('device'))
        _ = User.objects.get(uuid=current_command.get('account'))

    # if either dont exist then dont make a request
    except ObjectDoesNotExist:
        return True

    jwt = os.environ.get("SYSTEM_JWT")
    event_service_url = os.environ.get("EVENT_SERVICE_URL")

    request_headers = {
        "Authorization": f"Bearer {jwt}"
    }

    req_body = {
        "service": "hive-backend",
        "type": "device-request",
        "account": current_command["account"],
        **current_command
    }

    response = requests.post(
        f'{event_service_url}/api/v1/request/',
        json=req_body,
        headers=request_headers
    )
    if not response.ok:
        scheduler_logger.error("HTTP Status (%d) sending event to Device (%s). Event: %s",
            response.status_code,
            current_command.get("device"),
            req_body)
        return False

    scheduler_logger.info("Device ACK: Request (%s) for App Instance (%s) completed by Device (%s): %s",
        current_command.get("command"),
        current_command.get("payload").get("instance"),
        current_command.get("device"),
        str(req_body))

    if remaining_commands:
        # handle a pending reschedule, we assume the stop command was acknowledged so we update device and unset reschedule_to_device on AppInstnace
        app_instance = AppInstance.objects.filter(uuid=current_command['payload']['instance'], reschedule_to_device__isnull=False).first()
        if app_instance:
            app_instance.device = app_instance.reschedule_to_device
            app_instance.reschedule_to_device = None
            app_instance.save()
        # waiting for device to ack stop event is not enough, storage isn't in a clean state
        # TODO fix me in storage plugin
        time.sleep(10)
        send_device_request.apply_async(
            kwargs={
                'current_command': remaining_commands[0],
                'remaining_commands': remaining_commands[1:]
            },
            ignore_result=True
        )

    return True

@app.task
def command_executor(commands):
    '''
    Handles sending commands to devices.

    NOTE: This task will call itself if given a sublist. This allows for commands
          to depend sequentially on each other.
          For example:
            commands = [[command2, command3_depends_on_command2_success, ...], ...]
    '''
    scheduler_logger.info("Command Executor Given Commands: %s", commands)

    for command in commands:
        # determine if commands should be run sequentially (commands are in a list)
        # each command must pass in order for next command to run
        if isinstance(command, list):
            scheduler_logger.debug("Calling sequential commands: %s", command)
            send_device_request.apply_async(kwargs={'current_command': command[0], 'remaining_commands': command[1:]}, ignore_result=True)

        # command is a single command that has no dependencies so "fire and forget"
        # (we dont need to await the result of the command)
        else:
            # send request to device
            send_device_request.apply_async(kwargs={'current_command': command}, ignore_result=True)

    return True
