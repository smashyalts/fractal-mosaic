from django.core.mail import send_mail, mail_admins

import hive_backend.api.utils as utils
from hive_backend.celery_config import app
from hive_backend.api.models import User

@app.task
def send_email(recipient, sender, subject, body, body_html=''):
    '''
    Handles sending an email to a user.
    '''
    send_mail(
        from_email=sender,
        subject=subject,
        message=body,
        html_message=body_html if body_html else None,
        recipient_list=recipient,
    )

@app.task
def send_admin_email(subject, message, connection, *args, **kwargs):
    '''
    Sends an email to alerts@fractalnetworks.co.
    '''
    mail_admins(subject, message, *args, connection=connection, **kwargs)

@app.task
def send_launch_device_reminder():
    '''
    Periodic task that sends a notifcation to users who have not yet
    launched a Device.
    '''
    # get all users that do not currently have a device
    users = User.objects.filter(device__isnull=True)

    # send an email to each of these users
    for user in users:
        name = user.display_name if user.display_name else user.email
        notifier = utils.get_notifier(user)

        # if no notifier returned, then dont send a notifcation
        if not notifier:
            return

        notifier.launch_device_reminder(name)
