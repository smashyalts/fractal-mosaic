from django.db import models
from hive_backend.api.models import BaseModel
from django.core.cache import cache
import logging
import requests

logger = logging.getLogger("django")

class AppCatalog(BaseModel):
    name = models.CharField(max_length=255)
    repo_url = models.URLField()

    def __str__(self,):
        return f'{self.name, self.uuid} (App Catalog)'


class App(BaseModel):
    name = models.CharField(max_length=255)
    image = models.FileField(null=True, upload_to='./app_catalog')
    catalog = models.ForeignKey(AppCatalog, on_delete=models.CASCADE)
    # version = models.ForeignKey('AppVersion', on_delete=models.CASCADE)

    # PROPOSED
    subtitle = models.CharField(max_length=255, blank=True)
    description = models.TextField(blank=True)
    hero_image = models.FileField(null=True, blank=True, upload_to='./app_catalog_hero_img')
    content = models.TextField(blank=True)
    github_url = models.URLField()
    categories = models.ManyToManyField('AppCategory', blank=True)


    def cached_github_stars(self):

        cache_prefix_name = f'{self.name}_github_stars'
        format_github_api_url = self.github_url.replace('https://github.com/', 'https://api.github.com/repos/')

        def safe_num(num):
            if isinstance(num, str):
                num = float(num)
            return float('{:.3g}'.format(abs(num)))

        def format_number(num):
            num = safe_num(num)
            sign = ''

            metric = {'T': 1000000000000, 'B': 1000000000, 'M': 1000000, 'K': 1000, '': 1}

            for index in metric:
                num_check = num / metric[index]

                if(num_check >= 1):
                    num = num_check
                    sign = index
                    break

            return f"{str(num).rstrip('0').rstrip('.')}{sign}"

        cached_github_stars = cache.get(cache_prefix_name)

        if cached_github_stars:
            logger.debug('***********GITHUB STAR CACHE REQUEST NOT SENT***********')
            return cached_github_stars
        else:
            logger.debug('***********GITHUB STAR CACHE REQUEST SENT***********')
            response = requests.get(format_github_api_url)
            cache.set(cache_prefix_name, format_number(response.json()["stargazers_count"]))
            return format_number(response.json()["stargazers_count"])

    def __str__(self):
        return f'{self.name}'


# class AppVersion(BaseModel):
#     version = models.CharField(max_length=255)


class AppCategory(BaseModel):
    name = models.CharField(max_length=255)

    def __str__(self):
        return f'{self.name}'


class AppResource(BaseModel):
    name = models.CharField(max_length=255)
    url = models.URLField()
    app = models.ForeignKey(App, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.name}'


class AppGalleryImage(BaseModel):
    url = models.FileField(null=False, upload_to='./app_catalog_gallery')
    caption = models.CharField(max_length=100)
    app = models.ForeignKey(App, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.url}'

