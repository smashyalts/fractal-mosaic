from rest_framework import serializers
from .models import App, AppCatalog, AppCategory, AppGalleryImage, AppResource

class CategorySerializer(serializers.ModelSerializer):
    
    
    class Meta:
        model = AppCategory
        fields = ['name']

    def to_representation(self, instance):
        return super().to_representation(instance)['name']


class ResourceSerializer(serializers.ModelSerializer):
    

    class Meta:
        model = AppResource
        fields = ('name', 'url')


class AppCatalogSerializer(serializers.ModelSerializer):
    

    class Meta:
        model = AppCatalog
        fields = ('name', 'repo_url')


class AppGalleryImageSerializer(serializers.ModelSerializer):


    class Meta:
        model = AppGalleryImage
        fields = ('url', 'caption')


class AppSerializer(serializers.ModelSerializer):
    categories = CategorySerializer(many=True, read_only=True)
    resources = ResourceSerializer(many=True, read_only=True, source = 'appresource_set')
    catalog = AppCatalogSerializer()
    gallery = AppGalleryImageSerializer(many=True, source = 'appgalleryimage_set')


    class Meta:
        model = App
        fields = ('uuid', 'date_created', 'date_modified', 'deleted', 'name', 'image', 'subtitle', 'description', 'hero_image', 'content', 'github_url', 'cached_github_stars', 'catalog', 'categories', 'resources', 'gallery')
        # fields = '__all__'
