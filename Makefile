.PHONY: reset-plugin
COMPOSE_FILES = -p fractal-mosaic -f docker-compose.yml -f docker-compose.dev.yml -f apps/hive-device/docker-compose.device.yml -f apps/hive-device/docker-compose.device2.yml -f docker-compose.flower.yml

dev:
	docker compose $(COMPOSE_FILES) --env-file .env up --remove-orphans --build --force-recreate -V -d;

up:
	docker compose $(COMPOSE_FILES) --env-file .env up --remove-orphans --force-recreate -V -d;

down:
	docker compose $(COMPOSE_FILES) --env-file .env down --remove-orphans ;

destroy:
	docker compose $(COMPOSE_FILES) --env-file .env down --remove-orphans -v;

clean:
	docker compose $(COMPOSE_FILES) --env-file .env down --remove-orphans -v;
	docker system prune -af --volumes ;
	find * -type d -name "node_modules" -prune -exec rm -rf '{}' + ;

api-shell:
	docker compose exec hive-backend bash ;

reset-migrations:
	find ./apps/api -path "*/migrations/*.py" -not -name "__init__.py" -not -path "*/MosaicAPIEnv/*" -delete ;

api-reset:
	docker compose $(COMPOSE_FILES) stop hive-backend hive-db hive-backend-worker cache;
	docker compose $(COMPOSE_FILES) rm -fv hive-backend hive-db hive-backend-worker cache;
	# remove postgres db and redis volumes
	docker volume rm fractal-mosaic_hive-db || true
	docker volume rm fractal-mosic_cache || true
	docker compose $(COMPOSE_FILES) up -d hive-backend hive-db hive-backend-worker cache;

api-logs:
	docker compose logs -f hive-backend ;

pyenv:
	python3 -m venv apps/api/MosaicAPIEnv ;
	. apps/api/MosaicAPIEnv/bin/activate && pip install -r apps/api/requirements.txt ;

dump_data:
	docker compose exec hive-backend bash -c "python3 manage.py dumpdata --natural-foreign --exclude=auth.permission --exclude=contenttypes --indent=4 > fixtures/data.json" ;

data:
	docker compose exec hive-backend python3 manage.py loaddata fixtures/data.json ;

migrate:
	docker compose exec hive-backend bash -c "python3 manage.py makemigrations && python3 manage.py migrate" ;

console-shell:
	docker compose exec console-ui bash ;

hosts-setup:
	grep keycloak /etc/hosts || echo "127.0.0.1 keycloak" | sudo tee -a /etc/hosts ;
	grep api /etc/hosts || echo "127.0.0.1 api" | sudo tee -a /etc/hosts ;

directory-ownership:
	sudo chown -R $$USER: * ;

device-logs:
	docker compose $(COMPOSE_FILES) logs -f device

device2-logs:
	docker compose $(COMPOSE_FILES) logs -f device2

npm-clean:
	find * -type d -name "node_modules" -prune -exec rm -rf '{}' + ;

device-reset:
	docker compose $(COMPOSE_FILES) up -d --force-recreate device device2

apitest:
	docker compose $(COMPOSE_FILES) exec hive-backend pytest -s

devicetest:
	docker run --rm -it -e DEVICE_SECRET=password -e API_TOKEN=apitoken --entrypoint pytest -v `pwd`/apps/hive-device:/code hive-device:local --asyncio-mode=auto -s tests/

reset-plugin:
	docker plugin rm -f fractalnetworks/storage:main
	sudo rm -rf /var/lib/fractal
	docker container run --rm -it --privileged   --pid=host alpine:edge nsenter -t 1 -m -u -n -i bash -c "rm -rf /var/lib/fractal"

db-shell:
	docker compose exec hive-db psql -d hive -U postgres

cache-shell:
	docker compose exec -it cache redis-cli -a fractalpassword
