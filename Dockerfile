FROM node:16.15.1-alpine

RUN mkdir -p /code/apps

WORKDIR /code

COPY ./apps/console_ui /code/apps/console_ui
COPY ./apps/appstore_ui /code/apps/appstore_ui
COPY ./libraries /code/libraries
COPY package.json /code
COPY package-lock.json /code

RUN npm install
RUN cd /code/apps/console_ui; npm run build; cd /code/apps/appstore_ui; npm run build

ENTRYPOINT ["tail -f /dev/null"]
